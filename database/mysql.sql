CREATE DATABASE IF NOT EXISTS Social_Network;
CREATE TABLE IF NOT EXISTS Accounts (
  ID                     SERIAL,
  First_Name             CHAR(20)                           NOT NULL,
  Last_Name              CHAR(20)                           NOT NULL,
  Middle_Name            CHAR(20),
  Date_of_Birth          DATE,
  Email                  VARCHAR(150) UNIQUE                NOT NULL,
  Password               VARCHAR(255)                       NOT NULL,
  ICQ                    VARCHAR(15),
  Skype                  VARCHAR(100),
  Additional_Information VARCHAR(255),
  Role                   ENUM ('USER', 'ADMIN')             NOT NULL DEFAULT 'USER',
  Photo                  MEDIUMBLOB,
  Registration           DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
  Deleted                BOOLEAN DEFAULT FALSE              NOT NULL,
  PRIMARY KEY (ID)
);
CREATE TABLE IF NOT EXISTS Phones (
  ID         SERIAL,
  Account_ID BIGINT UNSIGNED              NOT NULL,
  Type       ENUM ('PERSONAL', 'WORKING') NOT NULL,
  Number     CHAR(11)                     NOT NULL,
  PRIMARY KEY (ID),
  FOREIGN KEY (Account_ID) REFERENCES Accounts (ID)
);
CREATE TABLE IF NOT EXISTS Addresses (
  ID         SERIAL,
  Account_ID BIGINT UNSIGNED          NOT NULL,
  Type       ENUM ('HOME', 'WORKING') NOT NULL,
  Country    CHAR(20),
  Region     CHAR(20),
  City       CHAR(20),
  Street     CHAR(20),
  House      CHAR(20),
  PRIMARY KEY (ID),
  FOREIGN KEY (Account_ID) REFERENCES Accounts (ID)
);
CREATE TABLE IF NOT EXISTS Groups (
  ID               SERIAL,
  Name             CHAR(20) UNIQUE                    NOT NULL,
  Description      VARCHAR(255),
  Creator_ID       BIGINT UNSIGNED                    NOT NULL,
  Photo            MEDIUMBLOB,
  Date_of_Creation DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
  Deleted          BOOLEAN DEFAULT FALSE              NOT NULL,
  PRIMARY KEY (ID),
  FOREIGN KEY (Creator_ID) REFERENCES Accounts (ID)
);
CREATE TABLE IF NOT EXISTS Friends (
  ID_min         BIGINT UNSIGNED                                     NOT NULL,
  ID_max         BIGINT UNSIGNED                                     NOT NULL,
  Status         ENUM ('PENDING', 'ACCEPTED', 'DECLINED', 'BLOCKED') NOT NULL,
  Action_User_ID BIGINT UNSIGNED                                     NOT NULL,
  PRIMARY KEY (ID_min, ID_max),
  FOREIGN KEY (ID_min) REFERENCES Accounts (ID),
  FOREIGN KEY (ID_max) REFERENCES Accounts (ID)
);
CREATE TABLE IF NOT EXISTS Group_Members (
  ID_User  BIGINT UNSIGNED                              NOT NULL,
  ID_Group BIGINT UNSIGNED                              NOT NULL,
  Status   ENUM ('PENDING', 'USER', 'MODER', 'BLOCKED') NOT NULL,
  PRIMARY KEY (ID_User, ID_Group),
  FOREIGN KEY (ID_User) REFERENCES Accounts (ID),
  FOREIGN KEY (ID_Group) REFERENCES Groups (ID)
);
CREATE TABLE IF NOT EXISTS Wall_Messages (
  ID               SERIAL,
  ID_Author        BIGINT UNSIGNED                    NOT NULL,
  ID_Target        BIGINT UNSIGNED                    NOT NULL,
  Text             VARCHAR(255),
  Photo            MEDIUMBLOB,
  Date_of_Creation DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
  Edited           BOOLEAN DEFAULT FALSE              NOT NULL,
  Removed          BOOLEAN DEFAULT FALSE              NOT NULL,
  FOREIGN KEY (ID_Author) REFERENCES Accounts (ID),
  FOREIGN KEY (ID_Target) REFERENCES Accounts (ID)
);
CREATE TABLE IF NOT EXISTS Group_Messages (
  ID               SERIAL,
  ID_Author        BIGINT UNSIGNED                    NOT NULL,
  ID_Target        BIGINT UNSIGNED                    NOT NULL,
  Text             VARCHAR(255),
  Photo            MEDIUMBLOB,
  Date_of_Creation DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
  Edited           BOOLEAN DEFAULT FALSE              NOT NULL,
  Removed          BOOLEAN DEFAULT FALSE              NOT NULL,
  FOREIGN KEY (ID_Author) REFERENCES Accounts (ID),
  FOREIGN KEY (ID_Target) REFERENCES Groups (ID)
);
CREATE TABLE IF NOT EXISTS Private_Messages (
  ID                SERIAL,
  ID_Author         BIGINT UNSIGNED                    NOT NULL,
  ID_Target         BIGINT UNSIGNED                    NOT NULL,
  Text              VARCHAR(255),
  Photo             MEDIUMBLOB,
  Date_of_Creation  DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
  Viewed            BOOLEAN DEFAULT FALSE              NOT NULL,
  Edited            BOOLEAN DEFAULT FALSE              NOT NULL,
  Removed_by_Author BOOLEAN DEFAULT FALSE              NOT NULL,
  Removed_by_Target BOOLEAN DEFAULT FALSE              NOT NULL,
  FOREIGN KEY (ID_Author) REFERENCES Accounts (ID),
  FOREIGN KEY (ID_Target) REFERENCES Accounts (ID)
);