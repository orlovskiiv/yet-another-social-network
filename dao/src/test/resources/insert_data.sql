INSERT INTO Accounts (ID, First_name, Last_name, Middle_Name, Date_of_Birth, Email, Password, ICQ, Skype, Additional_Information, Role, Photo, Registration, Deleted) VALUES (1, 'Vladimir', 'Orlovskii', 'Igorevich', parsedatetime('28-07-1993', 'dd-MM-yyyy'), 'Vvv@vvv.ru', 'password', '123456', 'Vovanuw', 'qwerty', 'ADMIN', 22, parsedatetime('01-03-2018', 'dd-MM-yyyy'), FALSE);
INSERT INTO Accounts (ID, First_name, Last_name, Email, Password, Role, Registration, Deleted) VALUES (2, 'Mihail', 'Mihalich', 'AaAa@Aa.com', 'pswd', 'USER', parsedatetime('01-03-2018', 'dd-MM-yyyy'), FALSE);
INSERT INTO Accounts (ID, First_name, Last_name, Middle_Name, Date_of_Birth, Email, Password, Additional_Information, Role, Registration, Deleted) VALUES (3, 'Vlad', 'Vladov', 'Vladislavovich', parsedatetime('9-12-1985', 'dd-MM-yyyy'), 'ii@ii.ru', 'psss', 'zxcvbn', 'USER', parsedatetime('25-02-2018', 'dd-MM-yyyy'), FALSE);
INSERT INTO Accounts (ID, First_name, Last_name, Email, Password, Role, Registration, Deleted) VALUES (4, 'Vasya', 'Vas', 'vas@vs.com', '1234', 'USER', parsedatetime('15-04-2018', 'dd-MM-yyyy'), TRUE);
INSERT INTO Accounts (ID, First_name, Last_name, Email, Password, Role, Registration, Deleted) VALUES (5, 'YYY', 'Ya', 'Y@Y.com', '4444', 'USER', parsedatetime('22-02-2018', 'dd-MM-yyyy'), FALSE);
INSERT INTO Accounts (ID, First_name, Last_name, Email, Password, Role, Registration, Deleted) VALUES (6, 'YYY', 'Ya', 'Y@YYYY.com', '4444', 'USER', parsedatetime('22-02-2018', 'dd-MM-yyyy'), FALSE);

INSERT INTO Friends (ID_min, ID_max, Status, Action_User_ID) VALUES (1, 2, 'PENDING', 1);
INSERT INTO Friends (ID_min, ID_max, Status, Action_User_ID) VALUES (1, 3, 'ACCEPTED', 1);
INSERT INTO Friends (ID_min, ID_max, Status, Action_User_ID) VALUES (1, 4, 'ACCEPTED', 4);
INSERT INTO Friends (ID_min, ID_max, Status, Action_User_ID) VALUES (2, 3, 'DECLINED', 3);
INSERT INTO Friends (ID_min, ID_max, Status, Action_User_ID) VALUES (1, 5, 'PENDING', 1);

INSERT INTO Phones (ID, Account_ID, Type, Number) VALUES (1, 1, 'PERSONAL', '89850000000');
INSERT INTO Phones (ID, Account_ID, Type, Number) VALUES (2, 1, 'PERSONAL', '89851111111');
INSERT INTO Phones (ID, Account_ID, Type, Number) VALUES (3, 1, 'WORKING', '89859999999');
INSERT INTO Phones (ID, Account_ID, Type, Number) VALUES (4, 1, 'WORKING', '89858888888');
INSERT INTO Phones (ID, Account_ID, Type, Number) VALUES (5, 3, 'WORKING', '12345678900');

INSERT INTO Addresses (ID, Account_ID, Type, Country, Region, City, Street, House) VALUES (1, 1, 'HOME', 'Russia', 'Moscow', 'Him', '11111', '222222');
INSERT INTO Addresses (ID, Account_ID, Type, Country, Region, City, Street, House) VALUES (2, 1, 'WORKING', 'USA', 'California', 'DDD', '99999', '88888');
INSERT INTO Addresses (ID, Account_ID, Type, Country) VALUES (3, 3, 'HOME', 'UK');

INSERT INTO Groups (ID, Name, Description, Creator_ID, Photo, Date_of_Creation, Deleted) VALUES (1, 'News', 'Good news', 1, 33, parsedatetime('01-03-2018', 'dd-MM-yyyy'), FALSE);
INSERT INTO Groups (ID, Name, Creator_ID, Date_of_Creation, Deleted) VALUES (2, 'SMT', 2, parsedatetime('22-02-2018', 'dd-MM-yyyy'), FALSE);
INSERT INTO Groups (ID, Name, Creator_ID, Date_of_Creation, Deleted) VALUES (3, 'WMT', 3, parsedatetime('5-03-2018', 'dd-MM-yyyy'), TRUE);

INSERT INTO Group_Members (ID_User, ID_Group, Status) VALUES (1, 1, 'USER');
INSERT INTO Group_Members (ID_User, ID_Group, Status) VALUES (1, 2, 'USER');
INSERT INTO Group_Members (ID_User, ID_Group, Status) VALUES (2, 1, 'USER');

INSERT INTO Group_Messages (ID, ID_Author, ID_Target, Text, Photo, Date_of_Creation, Edited, Removed) VALUES (1, 1, 2, 'Text', 33, parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE);
INSERT INTO Group_Messages (ID, ID_Author, ID_Target, Text, Date_of_Creation, Edited, Removed) VALUES (2, 1, 2, 'Text', parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE);
INSERT INTO Group_Messages (ID, ID_Author, ID_Target, Text, Date_of_Creation, Edited, Removed) VALUES (3, 3, 2, 'DDD', parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE);

INSERT INTO Wall_Messages (ID, ID_Author, ID_Target, Text, Photo, Date_of_Creation, Edited, Removed) VALUES (1, 1, 2, 'Text', 33, parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE);
INSERT INTO Wall_Messages (ID, ID_Author, ID_Target, Text, Date_of_Creation, Edited, Removed) VALUES (2, 1, 2, 'Text', parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE);
INSERT INTO Wall_Messages (ID, ID_Author, ID_Target, Text, Date_of_Creation, Edited, Removed) VALUES (3, 3, 2, 'DDD', parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE);

INSERT INTO Private_Messages (ID, ID_Author, ID_Target, Text, Photo, Date_of_Creation, Viewed, Edited, Removed_by_Author, Removed_by_Target) VALUES (1, 2, 1, 'Text', 33, parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE, FALSE, FALSE);
INSERT INTO Private_Messages (ID, ID_Author, ID_Target, Text, Date_of_Creation, Viewed, Edited, Removed_by_Author, Removed_by_Target) VALUES (2, 1, 2, 'Text', parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE, FALSE, FALSE);
INSERT INTO Private_Messages (ID, ID_Author, ID_Target, Text, Date_of_Creation, Viewed, Edited, Removed_by_Author, Removed_by_Target) VALUES (3, 2, 1, 'DDD', parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE, FALSE, FALSE);
INSERT INTO Private_Messages (ID, ID_Author, ID_Target, Text, Date_of_Creation, Viewed, Edited, Removed_by_Author, Removed_by_Target) VALUES (4, 2, 3, 'FFF', parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE, FALSE, FALSE);
INSERT INTO Private_Messages (ID, ID_Author, ID_Target, Text, Date_of_Creation, Viewed, Edited, Removed_by_Author, Removed_by_Target) VALUES (5, 3, 2, 'VVV', parsedatetime('05-04-2018', 'dd-MM-yyyy'), FALSE, FALSE, FALSE, FALSE);