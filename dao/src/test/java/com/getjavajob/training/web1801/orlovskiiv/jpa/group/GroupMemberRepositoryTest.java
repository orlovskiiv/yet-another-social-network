package com.getjavajob.training.web1801.orlovskiiv.jpa.group;

import com.getjavajob.training.web1801.orlovskiiv.configuration.TestDaoConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMember;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDaoConfiguration.class)
@Transactional
public class GroupMemberRepositoryTest {
    @Autowired
    private GroupMemberRepository groupMemberRepository;

    @Test
    public void testFindById() {
        GroupMember expected = new GroupMember(1, 1, USER);
        assertEquals(expected, groupMemberRepository.findById(new GroupMemberId(1, 1)).orElse(null));
    }

    @Test
    public void testFindByIdNull() {
        assertEquals(Optional.empty(), groupMemberRepository.findById(new GroupMemberId(99, 99)));
    }

    @Test
    public void testDeleteById() {
        groupMemberRepository.deleteById(new GroupMemberId(1, 1));
        assertEquals(Optional.empty(),
                groupMemberRepository.findById(new GroupMemberId(1, 1)));
    }

    @Test
    public void testSave() {
        assertEquals(groupMemberRepository.save(new GroupMember(2, 3, PENDING)),
                groupMemberRepository.findById(new GroupMemberId(2, 3)).orElse(null));
    }

    @Test
    public void testSaveUpdate() {
        assertEquals(groupMemberRepository.save(new GroupMember(1, 1, MODER)),
                groupMemberRepository.findById(new GroupMemberId(1, 1)).orElse(null));
    }
}
