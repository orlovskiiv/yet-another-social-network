package com.getjavajob.training.web1801.orlovskiiv.jpa.message;

import com.getjavajob.training.web1801.orlovskiiv.configuration.TestDaoConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.message.PrivateMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.time.LocalDateTime.of;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDaoConfiguration.class)
@Transactional
public class PrivateMessageRepositoryTest {
    @Autowired
    private PrivateMessageRepository privateMessageRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void testFindById() {
        assertEquals(getMessageId1(), privateMessageRepository.findById(1).orElse(null));
    }

    @Test
    public void testFindByIdNull() {
        assertEquals(Optional.empty(), privateMessageRepository.findById(99));
    }

    @Test
    public void testUpdateAllRemovedFieldsTrue() {
        assertTrue(privateMessageRepository.updateAllRemovedFields(1) > 0);
    }

    @Test
    public void testUpdateAllRemovedFieldsFalse() {
        assertFalse(privateMessageRepository.updateAllRemovedFields(99) > 0);
    }

    @Test
    public void testUpdateAllRemovedFieldsValue() {
        privateMessageRepository.updateAllRemovedFields(1);
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PrivateMessage> select = builder.createQuery(PrivateMessage.class);
        Root<PrivateMessage> root = select.from(PrivateMessage.class);
        select.where(builder.equal(root.get("id"), 1), builder.equal(root.get("removedByAuthor"), false),
                builder.equal(root.get("removedByTarget"), false));
        List<PrivateMessage> messages = entityManager.createQuery(select).getResultList();
        if (!messages.isEmpty()) {
            fail();
        }
    }

    @Test
    public void testSave() {
        PrivateMessage message = new PrivateMessage();
        message.setIdAuthor(1);
        message.setIdTarget(3);
        message.setText("111");
        assertEquals(privateMessageRepository.save(message),
                privateMessageRepository.findById(message.getId()).orElse(null));
    }

    @Test
    public void testSaveUpdate() {
        PrivateMessage message = new PrivateMessage();
        message.setId(1);
        message.setIdAuthor(2);
        message.setIdTarget(1);
        message.setText("RRR");
        message.setEdited(true);
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        assertEquals(privateMessageRepository.save(message), privateMessageRepository.findById(1).orElse(null));
    }

    @Test
    public void testUpdateAuthorRemovedFieldTrue() {
        assertTrue(privateMessageRepository.updateAuthorRemovedField(1) > 0);
    }

    @Test
    public void testUpdateAuthorRemovedFieldFalse() {
        assertFalse(privateMessageRepository.updateAuthorRemovedField(99) > 0);
    }

    @Test
    public void testUpdateAuthorRemovedFieldValue() {
        privateMessageRepository.updateAuthorRemovedField(1);
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PrivateMessage> select = builder.createQuery(PrivateMessage.class);
        Root<PrivateMessage> root = select.from(PrivateMessage.class);
        select.where(builder.equal(root.get("id"), 1), builder.equal(root.get("removedByAuthor"), false));
        List<PrivateMessage> messages = entityManager.createQuery(select).getResultList();
        if (!messages.isEmpty()) {
            fail();
        }
    }

    @Test
    public void testUpdateTargetRemovedFieldTrue() {
        assertTrue(privateMessageRepository.updateTargetRemovedField(1) > 0);
    }

    @Test
    public void testUpdateTargetRemovedFieldFalse() {
        assertFalse(privateMessageRepository.updateTargetRemovedField(99) > 0);
    }

    @Test
    public void testUpdateTargetRemovedFieldValue() {
        privateMessageRepository.updateTargetRemovedField(1);
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PrivateMessage> select = builder.createQuery(PrivateMessage.class);
        Root<PrivateMessage> root = select.from(PrivateMessage.class);
        select.where(builder.equal(root.get("id"), 1), builder.equal(root.get("removedByTarget"), false));
        List<PrivateMessage> messages = entityManager.createQuery(select).getResultList();
        if (!messages.isEmpty()) {
            fail();
        }
    }

    @Test
    public void testGetChatMessages() {
        Set<PrivateMessage> expectedMessages = new LinkedHashSet<>();
        expectedMessages.add(getMessageId3());
        expectedMessages.add(getMessageId2());
        expectedMessages.add(getMessageId1());
        assertEquals(expectedMessages, privateMessageRepository.getChatMessages(2, 1));
    }

    @Test
    public void testGetChatMessagesEmpty() {
        assertEquals(new LinkedHashSet<>(), privateMessageRepository.getChatMessages(10, 15));
    }

    @Test
    public void testGetLastMessages() {
        Set<PrivateMessage> expectedMessages = new LinkedHashSet<>();
        expectedMessages.add(getMessageId3());
        expectedMessages.add(getMessageId5());
        assertEquals(expectedMessages, privateMessageRepository.getLastMessages(2));
    }

    @Test
    public void testGetLastMessagesEmpty() {
        assertEquals(new LinkedHashSet<>(), privateMessageRepository.getLastMessages(10));
    }

    private PrivateMessage getMessageId1() {
        PrivateMessage message = new PrivateMessage();
        message.setId(1);
        message.setIdAuthor(2);
        message.setIdTarget(1);
        message.setText("Text");
        message.setPhoto(new byte[]{0, 0, 0, 33});
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        return message;
    }

    private PrivateMessage getMessageId2() {
        PrivateMessage message = new PrivateMessage();
        message.setId(2);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("Text");
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        return message;
    }

    private PrivateMessage getMessageId3() {
        PrivateMessage message = new PrivateMessage();
        message.setId(3);
        message.setIdAuthor(2);
        message.setIdTarget(1);
        message.setText("DDD");
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        return message;
    }

    private PrivateMessage getMessageId5() {
        PrivateMessage message = new PrivateMessage();
        message.setId(5);
        message.setIdAuthor(3);
        message.setIdTarget(2);
        message.setText("VVV");
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        return message;
    }
}
