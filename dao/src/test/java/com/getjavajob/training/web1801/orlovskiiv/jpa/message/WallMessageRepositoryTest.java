package com.getjavajob.training.web1801.orlovskiiv.jpa.message;

import com.getjavajob.training.web1801.orlovskiiv.configuration.TestDaoConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.message.WallMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.time.LocalDateTime.of;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDaoConfiguration.class)
@Transactional
public class WallMessageRepositoryTest {
    @Autowired
    private WallMessageRepository wallMessageRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void testFindById() {
        assertEquals(getMessageId1(), wallMessageRepository.findById(1).orElse(null));
    }

    @Test
    public void testFindByIdNull() {
        assertEquals(Optional.empty(), wallMessageRepository.findById(99));
    }

    @Test
    public void testUpdateRemovedFieldTrue() {
        assertTrue(wallMessageRepository.updateRemovedField(1) > 0);
    }

    @Test
    public void testUpdateRemovedFieldFalse() {
        assertFalse(wallMessageRepository.updateRemovedField(99) > 0);
    }

    @Test
    public void testUpdateRemovedFieldValue() {
        wallMessageRepository.updateRemovedField(1);
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<WallMessage> select = builder.createQuery(WallMessage.class);
        Root<WallMessage> root = select.from(WallMessage.class);
        select.where(builder.equal(root.get("id"), 1), builder.equal(root.get("removed"), true));
        List<WallMessage> wallMessages = entityManager.createQuery(select).getResultList();
        if (wallMessages.isEmpty()) {
            fail();
        }
    }

    @Test
    public void testSave() {
        WallMessage message = new WallMessage();
        message.setIdAuthor(1);
        message.setIdTarget(3);
        message.setText("111");
        assertEquals(wallMessageRepository.save(message),
                wallMessageRepository.findById(message.getId()).orElse(null));
    }

    @Test
    public void testSaveUpdate() {
        WallMessage message = new WallMessage();
        message.setId(1);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("RRR");
        message.setEdited(true);
        assertEquals(wallMessageRepository.save(message), wallMessageRepository.findById(1).orElse(null));
    }

    @Test
    public void testFindByIdTargetAndRemovedOrderByDateOfCreationDesc() {
        Set<WallMessage> expectedMessages = new LinkedHashSet<>();
        expectedMessages.add(getMessageId1());
        expectedMessages.add(getMessageId2());
        expectedMessages.add(getMessageId3());
        assertEquals(expectedMessages,
                wallMessageRepository.findByIdTargetAndRemovedOrderByDateOfCreationDesc(2, false));
    }

    @Test
    public void testFindByIdTargetAndRemovedOrderByDateOfCreationDescEmpty() {
        assertEquals(new LinkedHashSet<>(),
                wallMessageRepository.findByIdTargetAndRemovedOrderByDateOfCreationDesc(10, false));
    }

    private WallMessage getMessageId1() {
        WallMessage message = new WallMessage();
        message.setId(1);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("Text");
        message.setPhoto(new byte[]{0, 0, 0, 33});
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        return message;
    }

    private WallMessage getMessageId2() {
        WallMessage message = new WallMessage();
        message.setId(2);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("Text");
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        return message;
    }

    private WallMessage getMessageId3() {
        WallMessage message = new WallMessage();
        message.setId(3);
        message.setIdAuthor(3);
        message.setIdTarget(2);
        message.setText("DDD");
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        return message;
    }
}
