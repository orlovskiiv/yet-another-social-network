package com.getjavajob.training.web1801.orlovskiiv.jpa.account;

import com.getjavajob.training.web1801.orlovskiiv.account.friend.Friendship;
import com.getjavajob.training.web1801.orlovskiiv.account.friend.FriendshipId;
import com.getjavajob.training.web1801.orlovskiiv.configuration.TestDaoConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

import static com.getjavajob.training.web1801.orlovskiiv.account.friend.FriendStatus.*;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDaoConfiguration.class)
@Transactional
public class FriendshipRepositoryTest {
    @Autowired
    private FriendshipRepository friendshipRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void testFindById() {
        Friendship expected = new Friendship(1, 4, ACCEPTED, 4);
        assertEquals(expected, friendshipRepository.findById(new FriendshipId(1, 4)).orElse(null));
    }

    @Test
    public void testFindByIdEmpty() {
        assertEquals(Optional.empty(), friendshipRepository.findById(new FriendshipId(98, 99)));
    }

    @Test
    public void testDeleteById() {
        friendshipRepository.deleteById(new FriendshipId(1, 2));
        assertEquals(Optional.empty(), friendshipRepository.findById(new FriendshipId(2, 4)));
    }

    @Test
    public void testSave() {
        assertEquals(friendshipRepository.save(new Friendship(2, 4, PENDING, 2)),
                friendshipRepository.findById(new FriendshipId(2, 4)).orElse(null));
    }

    @Test
    public void testUpdateFriendStatusTrue() {
        assertTrue(friendshipRepository.updateFriendStatus(ACCEPTED, 1, 1, 2) > 0);
    }

    @Test
    public void testUpdateFriendStatusFalse() {
        assertFalse(friendshipRepository.updateFriendStatus(DECLINED, 1, 1, 10) > 0);
    }

    @Test
    public void testUpdateFriendStatusValue() {
        friendshipRepository.updateFriendStatus(ACCEPTED, 2, 1, 2);
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Friendship> criteria = builder.createQuery(Friendship.class);
        Root<Friendship> root = criteria.from(Friendship.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("id").get("minAccountId"), "1"),
                builder.equal(root.get("id").get("maxAccountId"), "2"),
                builder.equal(root.get("status"), ACCEPTED));
        List<Friendship> friends = entityManager.createQuery(criteria).getResultList();
        if (friends.isEmpty()) {
            fail();
        }
    }
}
