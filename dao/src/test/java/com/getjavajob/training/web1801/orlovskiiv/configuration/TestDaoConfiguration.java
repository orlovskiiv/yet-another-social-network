package com.getjavajob.training.web1801.orlovskiiv.configuration;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableJpaRepositories("com.getjavajob.training.web1801.orlovskiiv")
@EnableTransactionManagement
@ComponentScan("com.getjavajob.training.web1801.orlovskiiv")
@PropertySource("classpath:/db_connection.properties")
public class TestDaoConfiguration {
    private static final String ENTITY_BASE_PACKAGE = "com.getjavajob.training.web1801.orlovskiiv";
    private static final String FORMAT_SQL_PROPERTY = "hibernate.format_sql";
    private static final String FORMAT_SQL_VALUE = "true";
    private static final String DIALECT_PROPERTY = "hibernate.dialect";
    private static final String DIALECT_VALUE = "org.hibernate.dialect.H2Dialect";
    private static final String DDL_AUTO_PROPERTY = "hibernate.hbm2ddl.auto";
    private static final String DDL_AUTO_VALUE = "create-drop";
    private static final String DDL_IMPORT_PROPERTY = "hibernate.hbm2ddl.import_files";
    private static final String DDL_IMPORT_VALUE = "insert_data.sql";

    @Value("${driver}")
    private String driver;
    @Value("${url}")
    private String url;
    @Value("${username}")
    private String username;
    @Value("${password}")
    private String password;

    @Bean
    public DataSource dataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(driver);
        basicDataSource.setUrl(url);
        basicDataSource.setUsername(username);
        basicDataSource.setPassword(password);
        return basicDataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory(dataSource(), jpaVendorAdapter()).getObject());
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
                                                                       JpaVendorAdapter jpaVendorAdapter) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setPackagesToScan(ENTITY_BASE_PACKAGE);
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactoryBean.setJpaProperties(jpaProperties());
        return entityManagerFactoryBean;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(true);
        hibernateJpaVendorAdapter.setDatabase(Database.H2);
        return hibernateJpaVendorAdapter;
    }

    @Bean
    public Properties jpaProperties() {
        Properties properties = new Properties();
        properties.put(FORMAT_SQL_PROPERTY, FORMAT_SQL_VALUE);
        properties.put(DIALECT_PROPERTY, DIALECT_VALUE);
        properties.put(DDL_AUTO_PROPERTY, DDL_AUTO_VALUE);
        properties.put(DDL_IMPORT_PROPERTY, DDL_IMPORT_VALUE);
        return properties;
    }
}
