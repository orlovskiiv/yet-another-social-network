package com.getjavajob.training.web1801.orlovskiiv.jpa.group;

import com.getjavajob.training.web1801.orlovskiiv.configuration.TestDaoConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.group.Group;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus.PENDING;
import static com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus.USER;
import static java.time.LocalDate.now;
import static java.time.LocalDate.of;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDaoConfiguration.class)
@Transactional
public class GroupRepositoryTest {
    @Autowired
    private GroupRepository groupRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void testFindByIdMax() {
        assertEquals(getGroupId1(), groupRepository.findById(1).orElse(null));
    }

    @Test
    public void testFindByIdMin() {
        assertEquals(getGroupId2(), groupRepository.findById(2).orElse(null));
    }

    @Test
    public void testUpdateDeletedFieldTrue() {
        assertTrue(groupRepository.updateDeletedField(true, 1) > 0);
    }

    @Test
    public void testUpdateDeletedFieldFalse() {
        assertFalse(groupRepository.updateDeletedField(true, 88) > 0);
    }

    @Test
    public void testUpdateDeletedFieldValue() {
        groupRepository.updateDeletedField(true, 1);
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> criteria = builder.createQuery(Group.class);
        Root<Group> root = criteria.from(Group.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("deleted"), "1")).where(builder.equal(root.get("id"), "1"));
        List<Group> groups = entityManager.createQuery(criteria).getResultList();
        if (groups.isEmpty()) {
            fail();
        }
    }

    @Test
    public void testSaveMax() {
        Group group = getNewFullInfoGroup();
        assertEquals(groupRepository.save(group), groupRepository.findById(group.getId()).orElse(null));
    }

    private Group getNewFullInfoGroup() {
        Group group = new Group();
        group.setName("NewGroup");
        group.setDescription("Description");
        group.setCreatorId(3);
        group.setPhoto(new byte[]{33});
        group.setDateOfCreation(now());
        return group;
    }

    @Test
    public void testSaveMin() {
        Group group = getNewMinInfoGroup();
        assertEquals(groupRepository.save(group), groupRepository.findById(group.getId()).orElse(null));
    }

    private Group getNewMinInfoGroup() {
        Group group = new Group();
        group.setName("Group");
        group.setCreatorId(3);
        group.setDateOfCreation(now());
        return group;
    }

    @Test
    public void testSaveUpdate() {
        Group group = getNewFullInfoGroup();
        group.setId(2);
        group.setDescription("Desc111");
        assertEquals(groupRepository.save(group), groupRepository.findById(2).orElse(null));
    }

    @Test
    public void testSelectGroupsByAccountIdAndStatus() {
        Set<Group> expectedGroups = new HashSet<>();
        expectedGroups.add(getGroupId1());
        expectedGroups.add(getGroupId2());
        assertEquals(expectedGroups, groupRepository.selectGroupsByAccountIdAndStatus(1, USER));
    }

    @Test
    public void testSelectGroupsByAccountIdAndStatusEmpty() {
        assertEquals(new HashSet<>(), groupRepository.selectGroupsByAccountIdAndStatus(1, PENDING));
    }

    @Test
    public void testSearchByName() {
        List<Group> expectedGroups = new ArrayList<>();
        expectedGroups.add(getGroupId2());
        expectedGroups.add(getGroupId3());
        Page<Group> page = groupRepository.searchByName("t", PageRequest.of(0, 5));
        assertEquals(expectedGroups, page.getContent());
    }

    @Test
    public void testSearchByNameEmpty() {
        Page<Group> page = groupRepository.searchByName("ttttt", PageRequest.of(0, 5));
        assertEquals(new ArrayList<>(), page.getContent());
    }

    @Test
    public void testFindByName() {
        assertEquals(getGroupId1(), groupRepository.findByName("News"));
    }

    @Test
    public void testFindByNameNull() {
        assertNull(groupRepository.findByName("KKK"));
    }

    private Group getGroupId1() {
        Group group = new Group();
        group.setId(1);
        group.setName("News");
        group.setDescription("Good news");
        group.setCreatorId(1);
        group.setPhoto(new byte[]{0, 0, 0, 33});
        group.setDateOfCreation(of(2018, 3, 1));
        return group;
    }

    private Group getGroupId2() {
        Group group = new Group();
        group.setId(2);
        group.setName("SMT");
        group.setCreatorId(2);
        group.setDateOfCreation(of(2018, 2, 22));
        return group;
    }

    private Group getGroupId3() {
        Group group = new Group();
        group.setId(3);
        group.setName("WMT");
        group.setCreatorId(3);
        group.setDateOfCreation(of(2018, 3, 5));
        group.setDeleted(true);
        return group;
    }
}
