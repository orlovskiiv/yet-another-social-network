package com.getjavajob.training.web1801.orlovskiiv.jpa.message;

import com.getjavajob.training.web1801.orlovskiiv.configuration.TestDaoConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.message.GroupMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static java.time.LocalDateTime.of;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDaoConfiguration.class)
@Transactional
public class GroupMessageRepositoryTest {
    @Autowired
    private GroupMessageRepository groupMessageRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void testFindById() {
        assertEquals(getMessageId1(), groupMessageRepository.findById(1).orElse(null));
    }

    @Test
    public void testFindByIdNull() {
        assertEquals(Optional.empty(), groupMessageRepository.findById(99));
    }

    @Test
    public void testUpdateRemovedFieldTrue() {
        assertTrue(groupMessageRepository.updateRemovedField(1) > 0);
    }

    @Test
    public void testUpdateRemovedFieldFalse() {
        assertFalse(groupMessageRepository.updateRemovedField(99) > 0);
    }

    @Test
    public void testUpdateRemovedFieldValue() {
        groupMessageRepository.updateRemovedField(1);
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GroupMessage> select = builder.createQuery(GroupMessage.class);
        Root<GroupMessage> root = select.from(GroupMessage.class);
        select.where(builder.equal(root.get("id"), 1), builder.equal(root.get("removed"), true));
        List<GroupMessage> groupMessages = entityManager.createQuery(select).getResultList();
        if (groupMessages.isEmpty()) {
            fail();
        }
    }

    @Test
    public void testSave() {
        GroupMessage message = new GroupMessage();
        message.setIdAuthor(1);
        message.setIdTarget(3);
        message.setText("111");
        message.setPhoto(new byte[]{51});
        assertEquals(groupMessageRepository.save(message),
                groupMessageRepository.findById(message.getId()).orElse(null));
    }

    @Test
    public void testSaveUpdate() {
        GroupMessage message = new GroupMessage();
        message.setId(1);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("RRR");
        message.setEdited(true);
        assertEquals(groupMessageRepository.save(message), groupMessageRepository.findById(1).orElse(null));
    }

    @Test
    public void testFindByIdTargetAndRemovedOrderByDateOfCreationDesc() {
        Set<GroupMessage> expectedMessages = new LinkedHashSet<>();
        expectedMessages.add(getMessageId1());
        expectedMessages.add(getMessageId2());
        expectedMessages.add(getMessageId3());
        assertEquals(expectedMessages,
                groupMessageRepository.findByIdTargetAndRemovedOrderByDateOfCreationDesc(2, false));
    }

    @Test
    public void testFindByIdTargetAndRemovedOrderByDateOfCreationDescEmpty() {
        assertEquals(new LinkedHashSet<>(),
                groupMessageRepository.findByIdTargetAndRemovedOrderByDateOfCreationDesc(10, false));
    }

    private GroupMessage getMessageId1() {
        GroupMessage message = new GroupMessage();
        message.setId(1);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("Text");
        message.setPhoto(new byte[]{0, 0, 0, 33});
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        return message;
    }

    private GroupMessage getMessageId2() {
        GroupMessage message = new GroupMessage();
        message.setId(2);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("Text");
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        return message;
    }

    private GroupMessage getMessageId3() {
        GroupMessage message = new GroupMessage();
        message.setId(3);
        message.setIdAuthor(3);
        message.setIdTarget(2);
        message.setText("DDD");
        message.setDateOfCreation(of(2018, 4, 5, 0, 0));
        return message;
    }
}
