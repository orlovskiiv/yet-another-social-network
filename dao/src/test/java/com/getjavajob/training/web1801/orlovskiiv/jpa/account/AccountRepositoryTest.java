package com.getjavajob.training.web1801.orlovskiiv.jpa.account;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountRole;
import com.getjavajob.training.web1801.orlovskiiv.account.address.Address;
import com.getjavajob.training.web1801.orlovskiiv.account.address.AddressType;
import com.getjavajob.training.web1801.orlovskiiv.account.phone.Phone;
import com.getjavajob.training.web1801.orlovskiiv.configuration.TestDaoConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.*;

import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRole.ADMIN;
import static com.getjavajob.training.web1801.orlovskiiv.account.phone.PhoneType.PERSONAL;
import static com.getjavajob.training.web1801.orlovskiiv.account.phone.PhoneType.WORKING;
import static java.time.LocalDate.of;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDaoConfiguration.class)
@Transactional
public class AccountRepositoryTest {
    @Autowired
    private AccountRepository accountRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void testFindByIdMax() {
        Account expectedAccount = getAccountId1();
        Account actualAccount = accountRepository.findById(1).orElse(null);
        assertEquals(expectedAccount, actualAccount);
    }

    @Test
    public void testFindByIdMin() {
        Account expectedAccount = getAccountId2();
        Account actualAccount = accountRepository.findById(2).orElse(null);
        assertEquals(expectedAccount, actualAccount);
    }

    @Test
    public void testFindByIdMiddle() {
        assertEquals(getAccountId3(), accountRepository.findById(3).orElse(null));
    }

    @Test
    public void testFindByIdNull() {
        assertEquals(Optional.empty(), accountRepository.findById(88));
    }

    @Test
    public void testFindFullAccountById() {
        Account expectedAccount = getAccountId1();
        Account actualAccount = accountRepository.findFullAccountById(1);
        assertEquals(expectedAccount, actualAccount);
    }

    @Test
    public void testFindFullAccountByIdNull() {
        assertNull(accountRepository.findFullAccountById(88));
    }

    @Test
    public void testUpdateDeletedFieldTrue() {
        assertTrue(accountRepository.updateDeletedField(true, 1) > 0);
    }

    @Test
    public void testUpdateDeletedFieldFalse() {
        assertFalse(accountRepository.updateDeletedField(true, 88) > 0);
    }

    @Test
    public void testUpdateDeletedFieldValue() {
        accountRepository.updateDeletedField(true, 1);
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteria = builder.createQuery(Account.class);
        Root<Account> root = criteria.from(Account.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("deleted"), "1")).where(builder.equal(root.get("id"), "1"));
        List<Account> accounts = entityManager.createQuery(criteria).getResultList();
        if (accounts.isEmpty()) {
            fail();
        }
    }

    @Test
    public void testSaveMax() {
        Account account = getNewFullInfoAccount(null);
        assertEquals(accountRepository.save(account), accountRepository.findById(account.getId()).orElse(null));
    }

    @Test
    public void testSaveMin() {
        Account account = new Account();
        account.setFirstName("TTT");
        account.setLastName("TT");
        account.setEmail("Y@Y");
        account.setPassword("1234566");
        account.setRole(AccountRole.USER);
        assertEquals(accountRepository.save(account), accountRepository.findById(account.getId()).orElse(null));
    }

    @Test
    public void testUpdateFullToFull() {
        Account updateAccount = getNewFullInfoAccount(1);
        assertEquals(accountRepository.save(updateAccount), accountRepository.findById(1).orElse(null));
    }

    @Test
    public void testUpdateMinToFull() {
        Account updateAccount = getNewMinInfoAccount();
        assertEquals(accountRepository.save(updateAccount), accountRepository.findById(1).orElse(null));
    }

    @Test
    public void testFindAccountByEmail() {
        assertEquals(getAccountId1(), accountRepository.findAccountByEmail("Vvv@vvv.ru"));
    }

    @Test
    public void testFindAccountByEmailNull() {
        assertNull(accountRepository.findAccountByEmail("V@vvv.ru"));
    }

    @Test
    public void testGetFriends() {
        Set<Account> expectedFriends = new LinkedHashSet<>();
        expectedFriends.add(getAccountId3());
        expectedFriends.add(getAccountId4());
        Set<Account> actualFriends = accountRepository.getFriends(1);
        assertEquals(expectedFriends, actualFriends);
    }

    @Test
    public void testGetFriendsEmpty() {
        assertEquals(new LinkedHashSet<>(), accountRepository.getFriends(10));
    }

    @Test
    public void testGetFriendRequests() {
        Set<Account> expectedFriendRequests = new LinkedHashSet<>();
        expectedFriendRequests.add(getAccountId1());
        Set<Account> actualFriendRequests = accountRepository.getFriendRequests(2);
        assertEquals(expectedFriendRequests, actualFriendRequests);
    }

    @Test
    public void testGetFriendRequestsEmpty() {
        assertEquals(new LinkedHashSet<>(), accountRepository.getFriendRequests(10));
    }

    @Test
    public void testGetSentRequestsUnconfirmed() {
        Set<Account> expectedSentRequests = new LinkedHashSet<>();
        expectedSentRequests.add(getAccountId2());
        expectedSentRequests.add(getAccountId5());
        Set<Account> actualSentRequests = accountRepository.getSentRequestsUnconfirmed(1);
        assertEquals(expectedSentRequests, actualSentRequests);
    }

    @Test
    public void testGetSentRequestsUnconfirmedEmpty() {
        assertEquals(new LinkedHashSet<>(), accountRepository.getSentRequestsUnconfirmed(10));
    }

    @Test
    public void testGetSentRequestsDeclined() {
        Set<Account> expectedSentRequests = new LinkedHashSet<>();
        expectedSentRequests.add(getAccountId3());
        Set<Account> actualSentRequests = accountRepository.getSentRequestsDeclined(2);
        assertEquals(expectedSentRequests, actualSentRequests);
    }

    @Test
    public void testGetSentRequestsDeclinedEmpty() {
        assertEquals(new LinkedHashSet<>(), accountRepository.getSentRequestsDeclined(10));
    }

    @Test
    public void testGetSubscribers() {
        Set<Account> expectedSubscribers = new LinkedHashSet<>();
        expectedSubscribers.add(getAccountId2());
        Set<Account> actualSubscribers = accountRepository.getSubscribers(3);
        assertEquals(expectedSubscribers, actualSubscribers);
    }

    @Test
    public void testGetSubscribersEmpty() {
        assertEquals(new LinkedHashSet<>(), accountRepository.getSubscribers(10));
    }

    @Test
    public void testSearchByFirstAndLastName() {
        List<Account> expected = new ArrayList<>();
        expected.add(getAccountId1());
        expected.add(getAccountId3());
        expected.add(getAccountId4());
        Page<Account> actualAccounts = accountRepository.searchByFirstAndLastName("v", PageRequest.of(0, 5));
        assertEquals(expected, actualAccounts.getContent());
    }

    @Test
    public void testSearchByFirstAndLastNameEmpty() {
        assertEquals(new ArrayList<>(), accountRepository
                .searchByFirstAndLastName("abc", PageRequest.of(0, 2)).getContent());
    }

    @Test
    public void testGetAccountsInGroup() {
        Set<Account> expectedAccountsInGroup = new LinkedHashSet<>();
        expectedAccountsInGroup.add(getAccountId1());
        expectedAccountsInGroup.add(getAccountId2());
        Set<Account> actualAccountsInGroup = accountRepository.getAccountsInGroup(1, GroupMemberStatus.USER);
        assertEquals(expectedAccountsInGroup, actualAccountsInGroup);
    }

    @Test
    public void testGetAccountsInGroupEmpty() {
        assertEquals(new LinkedHashSet<>(), accountRepository.getAccountsInGroup(10, GroupMemberStatus.USER));
    }

    private Account getAccountId1() {
        Account fullInfoAccount = new Account();
        fullInfoAccount.setId(1);
        fullInfoAccount.setFirstName("Vladimir");
        fullInfoAccount.setLastName("Orlovskii");
        fullInfoAccount.setMiddleName("Igorevich");
        fullInfoAccount.setDateOfBirth(of(1993, 7, 28));
        fullInfoAccount.setEmail("Vvv@vvv.ru");
        fullInfoAccount.setPassword("password");
        fullInfoAccount.setIcq("123456");
        fullInfoAccount.setSkype("Vovanuw");
        fullInfoAccount.setAdditionalInformation("qwerty");
        fullInfoAccount.setRole(ADMIN);
        fullInfoAccount.setPhoto(new byte[]{0, 0, 0, 22});
        fullInfoAccount.setRegistration(of(2018, 3, 1));
        fullInfoAccount.setDeleted(false);

        fullInfoAccount.getHomeAddress().add(getAddress1(fullInfoAccount));
        fullInfoAccount.getWorkingAddress().add(getAddress2(fullInfoAccount));
        fullInfoAccount.setPersonalPhones(getPersonalPhones1(fullInfoAccount));
        fullInfoAccount.setWorkingPhones(getWorkingPhones1(fullInfoAccount));

        return fullInfoAccount;
    }

    private Address getAddress1(Account account) {
        Address address = new Address();
        address.setId(1);
        address.setAccount(account);
        address.setType(AddressType.HOME);
        address.setCountry("Russia");
        address.setRegion("Moscow");
        address.setCity("Him");
        address.setStreet("11111");
        address.setHouse("222222");
        return address;
    }

    private Address getAddress2(Account account) {
        Address address = new Address();
        address.setId(2);
        address.setAccount(account);
        address.setType(AddressType.WORKING);
        address.setCountry("USA");
        address.setRegion("California");
        address.setCity("DDD");
        address.setStreet("99999");
        address.setHouse("88888");
        return address;
    }

    private Set<Phone> getPersonalPhones1(Account account) {
        Set<Phone> phones = new LinkedHashSet<>();
        phones.add(new Phone(1, account, PERSONAL, "89850000000"));
        phones.add(new Phone(2, account, PERSONAL, "89851111111"));
        return phones;
    }

    private Set<Phone> getWorkingPhones1(Account account) {
        Set<Phone> phones = new LinkedHashSet<>();
        phones.add(new Phone(3, account, WORKING, "89859999999"));
        phones.add(new Phone(4, account, WORKING, "89858888888"));
        return phones;
    }

    private Account getAccountId2() {
        Account minInfoAccount = new Account();
        minInfoAccount.setId(2);
        minInfoAccount.setFirstName("Mihail");
        minInfoAccount.setLastName("Mihalich");
        minInfoAccount.setEmail("AaAa@Aa.com");
        minInfoAccount.setPassword("pswd");
        minInfoAccount.setRegistration(of(2018, 3, 1));
        return minInfoAccount;
    }

    private Account getAccountId3() {
        Account middleInfoAccount = new Account();
        middleInfoAccount.setId(3);
        middleInfoAccount.setFirstName("Vlad");
        middleInfoAccount.setLastName("Vladov");
        middleInfoAccount.setMiddleName("Vladislavovich");
        middleInfoAccount.setDateOfBirth(of(1985, 12, 9));
        middleInfoAccount.setEmail("ii@ii.ru");
        middleInfoAccount.setPassword("psss");
        middleInfoAccount.setAdditionalInformation("zxcvbn");
        middleInfoAccount.setRegistration(of(2018, 2, 25));
        middleInfoAccount.getHomeAddress().add(getAddress3(middleInfoAccount));
        middleInfoAccount.getWorkingPhones().add(new Phone(5, middleInfoAccount, WORKING, "12345678900"));
        return middleInfoAccount;
    }

    private Address getAddress3(Account account) {
        Address address = new Address();
        address.setId(3);
        address.setAccount(account);
        address.setType(AddressType.HOME);
        address.setCountry("UK");
        return address;
    }

    private Account getAccountId4() {
        Account deletedAccount = new Account();
        deletedAccount.setId(4);
        deletedAccount.setFirstName("Vasya");
        deletedAccount.setLastName("Vas");
        deletedAccount.setEmail("vas@vs.com");
        deletedAccount.setPassword("1234");
        deletedAccount.setRegistration(of(2018, 4, 15));
        deletedAccount.setDeleted(true);
        return deletedAccount;
    }

    private Account getAccountId5() {
        Account deletedAccount = new Account();
        deletedAccount.setId(5);
        deletedAccount.setFirstName("YYY");
        deletedAccount.setLastName("Ya");
        deletedAccount.setEmail("Y@Y.com");
        deletedAccount.setPassword("4444");
        deletedAccount.setRegistration(of(2018, 2, 22));
        return deletedAccount;
    }

    private Account getNewFullInfoAccount(Integer accountId) {
        Account account = new Account();
        account.setId(accountId);
        account.setFirstName("TTT");
        account.setLastName("TT");
        account.setMiddleName("T");
        account.setDateOfBirth(of(1999, 12, 31));
        account.setEmail("ttt@tthh.ru");
        account.setPassword("999");
        account.setIcq("654321");
        account.setSkype("Ttttt");
        account.setAdditionalInformation("asdfgh");
        account.setRole(AccountRole.USER);
        account.setPhoto(new byte[]{33});

        account.getPersonalPhones().add(new Phone(account, PERSONAL, "99988877712"));
        account.getWorkingPhones().add(new Phone(account, WORKING, "88877766655"));
        account.getWorkingPhones().add(new Phone(account, WORKING, "11122211121"));
        account.getHomeAddress().add(new Address(account, AddressType.HOME, "BBB", "ZZZ", null, null, null));
        account.getWorkingAddress().add(new Address(account, AddressType.WORKING, "USA", null, null, null, null));
        return account;
    }

    private Account getNewMinInfoAccount() {
        Account account = new Account();
        account.setId(1);
        account.setFirstName("TTT");
        account.setLastName("TT");
        account.setEmail("tt@tt.ru");
        account.setPassword("1234566");
        account.setRole(AccountRole.USER);
        return account;
    }
}
