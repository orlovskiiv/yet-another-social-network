package com.getjavajob.training.web1801.orlovskiiv.jpa.message;

import com.getjavajob.training.web1801.orlovskiiv.message.GroupMessage;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface GroupMessageRepository extends CrudRepository<GroupMessage, Integer> {

    @Modifying
    @Query("UPDATE GroupMessage gm SET gm.removed = TRUE WHERE gm.id = ?1")
    int updateRemovedField(int id);

    Set<GroupMessage> findByIdTargetAndRemovedOrderByDateOfCreationDesc(int idTarget, boolean removed);
}
