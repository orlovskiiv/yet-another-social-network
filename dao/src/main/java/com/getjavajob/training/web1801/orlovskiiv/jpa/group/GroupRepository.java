package com.getjavajob.training.web1801.orlovskiiv.jpa.group;

import com.getjavajob.training.web1801.orlovskiiv.group.Group;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface GroupRepository extends CrudRepository<Group, Integer> {

    @Modifying
    @Query("UPDATE Group gr SET gr.deleted = ?1 WHERE gr.id = ?2")
    int updateDeletedField(boolean deleted, int id);

    @Query("SELECT gr FROM Group gr INNER JOIN GroupMember mb ON gr.id = mb.id.groupId WHERE " +
            "mb.id.accountId = ?1 AND mb.status = ?2")
    Set<Group> selectGroupsByAccountIdAndStatus(int accountId, GroupMemberStatus status);

    @Query("SELECT gr FROM Group gr WHERE LOWER(gr.name) LIKE CONCAT('%', LOWER(?1), '%')")
    Page<Group> searchByName(String substring, Pageable pageable);

    Group findByName(String name);
}
