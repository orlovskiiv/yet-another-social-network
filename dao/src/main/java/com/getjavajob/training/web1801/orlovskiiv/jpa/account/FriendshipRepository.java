package com.getjavajob.training.web1801.orlovskiiv.jpa.account;

import com.getjavajob.training.web1801.orlovskiiv.account.friend.FriendStatus;
import com.getjavajob.training.web1801.orlovskiiv.account.friend.Friendship;
import com.getjavajob.training.web1801.orlovskiiv.account.friend.FriendshipId;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface FriendshipRepository extends CrudRepository<Friendship, FriendshipId> {

    @Modifying
    @Query("UPDATE Friendship fr SET fr.status = ?1, fr.fromId = ?2 " +
            "WHERE fr.id.minAccountId = ?3 AND fr.id.maxAccountId = ?4")
    int updateFriendStatus(FriendStatus status, int fromId, int minId, int maxId);
}
