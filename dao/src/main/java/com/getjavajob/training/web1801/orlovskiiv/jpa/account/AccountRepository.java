package com.getjavajob.training.web1801.orlovskiiv.jpa.account;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface AccountRepository extends CrudRepository<Account, Integer> {

    @Modifying
    @Query("UPDATE Account account SET account.deleted = ?1 WHERE account.id = ?2")
    int updateDeletedField(boolean deleted, int id);

    @Query("SELECT account FROM Account account " +
            "LEFT OUTER JOIN FETCH account.homeAddress " +
            "LEFT OUTER JOIN FETCH account.workingAddress " +
            "LEFT OUTER JOIN FETCH account.personalPhones " +
            "LEFT OUTER JOIN FETCH account.workingPhones " +
            "WHERE account.id = ?1")
    Account findFullAccountById(int id);

    Account findAccountByEmail(String email);

    @Query("SELECT account FROM Account account INNER JOIN " +
            "Friendship fr ON account.id = fr.id.minAccountId OR account.id = fr.id.maxAccountId " +
            "WHERE (fr.id.minAccountId = ?1 OR fr.id.maxAccountId = ?1) " +
            "AND account.id <> ?1 AND fr.status = 'ACCEPTED'")
    Set<Account> getFriends(int id);

    @Query("SELECT account FROM Account account INNER JOIN " +
            "Friendship fr ON account.id = fr.id.minAccountId OR account.id = fr.id.maxAccountId " +
            "WHERE (fr.id.minAccountId = ?1 OR fr.id.maxAccountId = ?1) " +
            "AND account.id <> ?1 AND fr.status = 'PENDING' AND fr.fromId <> ?1")
    Set<Account> getFriendRequests(int id);

    @Query("SELECT account FROM Account account INNER JOIN " +
            "Friendship fr ON account.id = fr.id.minAccountId OR account.id = fr.id.maxAccountId " +
            "WHERE (fr.id.minAccountId = ?1 OR fr.id.maxAccountId = ?1) " +
            "AND account.id <> ?1 AND fr.status = 'PENDING' AND fr.fromId = ?1")
    Set<Account> getSentRequestsUnconfirmed(int id);

    @Query("SELECT account FROM Account account INNER JOIN " +
            "Friendship fr ON account.id = fr.id.minAccountId OR account.id = fr.id.maxAccountId " +
            "WHERE (fr.id.minAccountId = ?1 OR fr.id.maxAccountId = ?1) " +
            "AND account.id <> ?1 AND fr.status = 'DECLINED' AND fr.fromId <> ?1")
    Set<Account> getSentRequestsDeclined(int id);

    @Query("SELECT account FROM Account account INNER JOIN " +
            "Friendship fr ON account.id = fr.id.minAccountId OR account.id = fr.id.maxAccountId " +
            "WHERE (fr.id.minAccountId = ?1 OR fr.id.maxAccountId = ?1) " +
            "AND account.id <> ?1 AND fr.status = 'DECLINED' AND fr.fromId = ?1")
    Set<Account> getSubscribers(int id);

    @Query("SELECT account FROM Account account WHERE LOWER(account.firstName) LIKE CONCAT('%', LOWER(?1), '%') OR " +
            "LOWER(account.lastName) LIKE CONCAT('%', LOWER(?1), '%')")
    Page<Account> searchByFirstAndLastName(String substring, Pageable pageable);

    @Query("SELECT account FROM Account account WHERE account.id IN (SELECT gm.id.accountId " +
            "FROM GroupMember gm WHERE gm.id.groupId = ?1 AND gm.status= ?2)")
    Set<Account> getAccountsInGroup(int groupId, GroupMemberStatus status);
}
