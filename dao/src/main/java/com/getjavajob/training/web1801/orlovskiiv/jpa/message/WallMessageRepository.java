package com.getjavajob.training.web1801.orlovskiiv.jpa.message;

import com.getjavajob.training.web1801.orlovskiiv.message.WallMessage;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface WallMessageRepository extends CrudRepository<WallMessage, Integer> {

    @Modifying
    @Query("UPDATE WallMessage wm SET wm.removed = TRUE WHERE wm.id = ?1")
    int updateRemovedField(int id);

    Set<WallMessage> findByIdTargetAndRemovedOrderByDateOfCreationDesc(int idTarget, boolean removed);
}
