package com.getjavajob.training.web1801.orlovskiiv.jpa.group;

import com.getjavajob.training.web1801.orlovskiiv.group.GroupMember;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberId;
import org.springframework.data.repository.CrudRepository;

public interface GroupMemberRepository extends CrudRepository<GroupMember, GroupMemberId> {

}
