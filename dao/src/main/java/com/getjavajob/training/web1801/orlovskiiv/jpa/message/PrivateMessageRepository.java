package com.getjavajob.training.web1801.orlovskiiv.jpa.message;

import com.getjavajob.training.web1801.orlovskiiv.message.PrivateMessage;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface PrivateMessageRepository extends CrudRepository<PrivateMessage, Integer> {

    @Modifying
    @Query("UPDATE PrivateMessage msg SET msg.removedByAuthor = TRUE WHERE msg.id = ?1")
    int updateAuthorRemovedField(int id);

    @Modifying
    @Query("UPDATE PrivateMessage msg SET msg.removedByTarget = TRUE WHERE msg.id = ?1")
    int updateTargetRemovedField(int id);

    @Modifying
    @Query("UPDATE PrivateMessage msg SET msg.removedByAuthor = TRUE, msg.removedByTarget = TRUE WHERE msg.id = ?1")
    int updateAllRemovedFields(int id);

    @Query("SELECT msg FROM PrivateMessage msg " +
            "WHERE ((msg.idAuthor = ?1 AND msg.idTarget = ?2) OR (msg.idTarget = ?1 AND msg.idAuthor = ?2)) AND " +
            "((msg.idAuthor = ?1 AND msg.removedByAuthor = false) OR " +
            "(msg.idAuthor = ?2 AND msg.removedByTarget = false)) " +
            "ORDER BY msg.dateOfCreation")
    Set<PrivateMessage> getChatMessages(int chatOwnerId, int interlocutorId);

    @Query(value = "SELECT ID, ID_Author, ID_Target, Text, Photo, Date_of_Creation, Edited, Removed_by_Author, " +
            "Removed_by_Target, Viewed " +
            "FROM Private_Messages INNER JOIN (" +
            "SELECT MAX(ID) AS max FROM Private_Messages WHERE (ID_Author = ?1 AND Removed_by_Author = FALSE) OR " +
            "(ID_Target = ?1 AND Removed_by_Target = FALSE) " +
            "GROUP BY CASE WHEN ID_Author > ID_Target THEN ID_Target ELSE ID_Author END, " +
            "CASE WHEN ID_Author < ID_Target THEN ID_Target ELSE ID_Author END" +
            ") AS t ON Private_Messages.ID = t.max ORDER BY Date_of_Creation DESC", nativeQuery = true)
    Set<PrivateMessage> getLastMessages(int accountId);
}
