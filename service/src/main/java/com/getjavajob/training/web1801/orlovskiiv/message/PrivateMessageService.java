package com.getjavajob.training.web1801.orlovskiiv.message;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.AccountRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.message.PrivateMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Transactional
@Service
public class PrivateMessageService {
    private PrivateMessageRepository privateMessageRepository;
    private AccountRepository accountRepository;

    @Autowired
    public PrivateMessageService(PrivateMessageRepository privateMessageRepository,
                                 AccountRepository accountRepository) {
        this.privateMessageRepository = privateMessageRepository;
        this.accountRepository = accountRepository;
    }

    public PrivateMessage createUpdateMessage(PrivateMessage message) {
        return privateMessageRepository.save(message);
    }

    public PrivateMessage getMessageById(int messageId) {
        return privateMessageRepository.findById(messageId).orElse(null);
    }

    public boolean deleteMessageById(int messageId) {
        return privateMessageRepository.updateAllRemovedFields(messageId) > 0;
    }

    public boolean deleteMessageFromChat(int messageId, boolean isAuthor) {
        return isAuthor ? privateMessageRepository.updateAuthorRemovedField(messageId) > 0
                : privateMessageRepository.updateTargetRemovedField(messageId) > 0;
    }

    public Map<PrivateMessage, Account> getChatMessages(int chatOwnerId, int interlocutorId) {
        Set<PrivateMessage> chatMessages = privateMessageRepository.getChatMessages(chatOwnerId, interlocutorId);
        Map<PrivateMessage, Account> messagesWithAuthor = new LinkedHashMap<>();
        for (PrivateMessage message : chatMessages) {
            messagesWithAuthor.put(message, getAuthorAccount(message, chatOwnerId, interlocutorId));
        }
        return messagesWithAuthor;
    }

    private Account getAuthorAccount(PrivateMessage message, int chatOwnerId, int interlocutorId) {
        return message.getIdAuthor() == chatOwnerId ? accountRepository.findById(chatOwnerId).orElse(null)
                : accountRepository.findById(interlocutorId).orElse(null);
    }

    public Map<PrivateMessage, Account> getLastMessages(int accountId) {
        Set<PrivateMessage> lastMessages = privateMessageRepository.getLastMessages(accountId);
        Map<PrivateMessage, Account> messagesWithChatAccount = new LinkedHashMap<>();
        for (PrivateMessage message : lastMessages) {
            messagesWithChatAccount.put(message, getInterlocutorAccount(message, accountId));
        }
        return messagesWithChatAccount;
    }

    private Account getInterlocutorAccount(PrivateMessage message, int accountId) {
        return message.getIdAuthor() == accountId ? accountRepository.findById(message.getIdTarget()).orElse(null)
                : accountRepository.findById(message.getIdAuthor()).orElse(null);
    }
}


