package com.getjavajob.training.web1801.orlovskiiv.group;

import com.getjavajob.training.web1801.orlovskiiv.jpa.group.GroupMemberRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.group.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus.MODER;
import static com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus.NONE;

@Transactional
@Service
public class GroupService {
    private GroupRepository groupRepository;
    private GroupMemberRepository groupMemberRepository;

    @Autowired
    public GroupService(GroupRepository groupRepository,
                        GroupMemberRepository groupMemberRepository) {
        this.groupRepository = groupRepository;
        this.groupMemberRepository = groupMemberRepository;
    }

    public Group createGroup(Group group) {
        Group resultGroup = groupRepository.save(group);
        groupMemberRepository.save(new GroupMember(resultGroup.getCreatorId(), resultGroup.getId(), MODER));
        return resultGroup;
    }

    public Group updateGroup(Group group) {
        return groupRepository.save(group);
    }

    public Group getGroupById(int id) {
        return groupRepository.findById(id).orElse(null);
    }

    public boolean deleteGroupById(int id) {
        return groupRepository.updateDeletedField(true, id) > 0;
    }

    public boolean recoverGroupById(int id) {
        return groupRepository.updateDeletedField(false, id) > 0;
    }

    public Set<Group> getGroupsByAccountIdAndStatus(int accountId, GroupMemberStatus status) {
        return groupRepository.selectGroupsByAccountIdAndStatus(accountId, status);
    }

    public Page<Group> getFoundGroups(String substring, int pageNumber, int pageSize) {
        return groupRepository.searchByName(substring, PageRequest.of(pageNumber, pageSize));
    }

    public boolean groupNameExists(String groupName) {
        return groupRepository.findByName(groupName) != null;
    }

    public GroupMember createUpdateGroupMember(int accountId, int groupId, GroupMemberStatus status) {
        return groupMemberRepository.save(new GroupMember(accountId, groupId, status));
    }

    public void deleteGroupMemberById(int userId, int groupId) {
        groupMemberRepository.deleteById(new GroupMemberId(userId, groupId));
    }

    public GroupMemberStatus getMemberStatus(int accountId, int groupId) {
        GroupMember member = groupMemberRepository.findById(new GroupMemberId(accountId, groupId)).orElse(null);
        return member != null ? member.getStatus() : NONE;
    }
}
