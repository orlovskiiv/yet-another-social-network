package com.getjavajob.training.web1801.orlovskiiv.message;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.AccountRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.message.GroupMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Transactional
@Service
public class GroupMessageService {
    private GroupMessageRepository groupMessageRepository;
    private AccountRepository accountRepository;

    @Autowired
    public GroupMessageService(GroupMessageRepository groupMessageRepository,
                               AccountRepository accountRepository) {
        this.groupMessageRepository = groupMessageRepository;
        this.accountRepository = accountRepository;
    }

    public GroupMessage createUpdateMessage(GroupMessage message) {
        return groupMessageRepository.save(message);
    }

    public GroupMessage getMessageById(int messageId) {
        return groupMessageRepository.findById(messageId).orElse(null);
    }

    public boolean deleteMessageById(int messageId) {
        return groupMessageRepository.updateRemovedField(messageId) > 0;
    }

    public Map<GroupMessage, Account> getGroupMessagesWithAuthor(int groupId) {
        Set<GroupMessage> groupMessages =
                groupMessageRepository.findByIdTargetAndRemovedOrderByDateOfCreationDesc(groupId, false);
        Map<GroupMessage, Account> messagesWithAuthor = new LinkedHashMap<>();
        for (GroupMessage message : groupMessages) {
            messagesWithAuthor.put(message, accountRepository.findById(message.getIdAuthor()).orElse(null));
        }
        return messagesWithAuthor;
    }
}
