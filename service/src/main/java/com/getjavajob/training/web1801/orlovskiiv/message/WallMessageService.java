package com.getjavajob.training.web1801.orlovskiiv.message;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.AccountRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.message.WallMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Transactional
@Service
public class WallMessageService {
    private WallMessageRepository wallMessageRepository;
    private AccountRepository accountRepository;

    @Autowired
    public WallMessageService(WallMessageRepository wallMessageRepository,
                              AccountRepository accountRepository) {
        this.wallMessageRepository = wallMessageRepository;
        this.accountRepository = accountRepository;
    }

    public WallMessage createUpdateMessage(WallMessage message) {
        return wallMessageRepository.save(message);
    }

    public WallMessage getMessageById(int messageId) {
        return wallMessageRepository.findById(messageId).orElse(null);
    }

    public boolean deleteMessageById(int messageId) {
        return wallMessageRepository.updateRemovedField(messageId) > 0;
    }

    public Map<WallMessage, Account> getUserMessagesWithAuthor(int accountId) {
        Set<WallMessage> userMessages = wallMessageRepository
                .findByIdTargetAndRemovedOrderByDateOfCreationDesc(accountId, false);
        Map<WallMessage, Account> messagesWithAuthor = new LinkedHashMap<>();
        for (WallMessage message : userMessages) {
            messagesWithAuthor.put(message, accountRepository.findById(message.getIdAuthor()).orElse(null));
        }
        return messagesWithAuthor;
    }
}
