package com.getjavajob.training.web1801.orlovskiiv.account;

import com.getjavajob.training.web1801.orlovskiiv.account.address.Address;
import com.getjavajob.training.web1801.orlovskiiv.account.friend.Friendship;
import com.getjavajob.training.web1801.orlovskiiv.account.friend.FriendshipId;
import com.getjavajob.training.web1801.orlovskiiv.account.phone.Phone;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.AccountRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.FriendshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRelationshipStatus.*;
import static com.getjavajob.training.web1801.orlovskiiv.account.friend.FriendStatus.*;

@Transactional
@Service
public class AccountService {
    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    private AccountRepository accountRepository;
    private FriendshipRepository friendshipRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository,
                          FriendshipRepository friendshipRepository) {
        this.accountRepository = accountRepository;
        this.friendshipRepository = friendshipRepository;
    }

    public Account createUpdateAccount(Account account) {
        addAccountRefToPhones(account, account.getPersonalPhones());
        addAccountRefToPhones(account, account.getWorkingPhones());
        addAccountRefToAddresses(account, account.getHomeAddress());
        addAccountRefToAddresses(account, account.getWorkingAddress());
        return accountRepository.save(account);
    }

    private void addAccountRefToPhones(Account account, Set<Phone> phones) {
        for (Phone phone : phones) {
            phone.setAccount(account);
        }
    }

    private void addAccountRefToAddresses(Account account, Set<Address> addresses) {
        for (Address address : addresses) {
            address.setAccount(account);
        }
    }

    public Account getFullAccountById(int accountId) {
        return accountRepository.findFullAccountById(accountId);
    }

    public Account getAccountById(int accountId) {
        return accountRepository.findById(accountId).orElse(null);
    }

    public Account getAccountByEmail(String email) {
        return accountRepository.findAccountByEmail(email);
    }

    @Secured(ROLE_ADMIN)
    public boolean deleteAccountById(int id) {
        return accountRepository.updateDeletedField(true, id) > 0;
    }

    @Secured(ROLE_ADMIN)
    public boolean recoverAccountById(int id) {
        return accountRepository.updateDeletedField(false, id) > 0;
    }

    public Friendship addFriend(int fromId, int toId) {
        Friendship friendship = new Friendship(getMinId(fromId, toId), getMaxId(fromId, toId), PENDING, fromId);
        return friendshipRepository.save(friendship);
    }

    private int getMinId(int fromId, int toId) {
        return fromId < toId ? fromId : toId;
    }

    private int getMaxId(int fromId, int toId) {
        return fromId > toId ? fromId : toId;
    }

    public void withdrawFriendRequest(int fromId, int toId) {
        friendshipRepository.deleteById(new FriendshipId(getMinId(fromId, toId), getMaxId(fromId, toId)));
    }

    public boolean acceptFriendRequest(int fromId, int toId) {
        return friendshipRepository
                .updateFriendStatus(ACCEPTED, fromId, getMinId(fromId, toId), getMaxId(fromId, toId)) > 0;
    }

    public boolean declineFriendRequest(int fromId, int toId) {
        return friendshipRepository
                .updateFriendStatus(DECLINED, fromId, getMinId(fromId, toId), getMaxId(fromId, toId)) > 0;
    }

    public Set<Account> getFriends(int id) {
        return accountRepository.getFriends(id);
    }

    public Set<Account> getFriendRequests(int id) {
        return accountRepository.getFriendRequests(id);
    }

    public Set<Account> getSentRequests(int id) {
        Set<Account> allSentRequests = new HashSet<>();
        allSentRequests.addAll(accountRepository.getSentRequestsUnconfirmed(id));
        allSentRequests.addAll(accountRepository.getSentRequestsDeclined(id));
        return allSentRequests;
    }

    public Set<Account> getSubscribers(int id) {
        return accountRepository.getSubscribers(id);
    }

    public Page<Account> getAllFoundAccounts(String searchValue, int pageNumber, int pageSize) {
        return accountRepository.searchByFirstAndLastName(searchValue, PageRequest.of(pageNumber, pageSize));
    }

    public boolean emailExists(String email) {
        return accountRepository.findAccountByEmail(email) != null;
    }

    public AccountRelationshipStatus getAccountStatus(int fromId, int toId) {
        Friendship friendship = friendshipRepository
                .findById(new FriendshipId(getMinId(fromId, toId), getMaxId(fromId, toId))).orElse(null);
        return friendship != null ? getStatusValue(fromId, friendship) : NONE;
    }

    private AccountRelationshipStatus getStatusValue(int fromId, Friendship friendship) {
        if (friendship.getStatus() == PENDING && friendship.getFromId() != fromId) {
            return FRIEND_REQUEST;
        } else if (friendship.getStatus() == PENDING) {
            return SENT_REQUEST;
        } else if (friendship.getStatus() == DECLINED && friendship.getFromId() != fromId) {
            return SENT_REQUEST;
        } else if (friendship.getStatus() == DECLINED) {
            return SUBSCRIBER;
        } else if (friendship.getStatus() == ACCEPTED) {
            return FRIEND;
        } else {
            return INVALID_STATUS;
        }
    }

    public Set<Account> getAccountsInGroup(int groupId, GroupMemberStatus status) {
        return accountRepository.getAccountsInGroup(groupId, status);
    }
}
