package com.getjavajob.training.web1801.orlovskiiv.configuration;

import com.getjavajob.training.web1801.orlovskiiv.jpa.account.AccountRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.FriendshipRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.group.GroupMemberRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.group.GroupRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.message.GroupMessageRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.message.PrivateMessageRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.message.WallMessageRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

import static org.mockito.Mockito.mock;

@Configuration
@ComponentScan(basePackages = "com.getjavajob.training.web1801.orlovskiiv",
        excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX,
                pattern = "com.getjavajob.training.web1801.orlovskiiv.configuration.*"))
public class TestServiceConfiguration {

    @Bean
    public AccountRepository accountRepository() {
        return mock(AccountRepository.class);
    }

    @Bean
    public FriendshipRepository friendshipRepository() {
        return mock(FriendshipRepository.class);
    }

    @Bean
    public GroupRepository groupRepository() {
        return mock(GroupRepository.class);
    }

    @Bean
    public GroupMemberRepository groupMemberRepository() {
        return mock(GroupMemberRepository.class);
    }

    @Bean
    public WallMessageRepository wallMessageRepository() {
        return mock(WallMessageRepository.class);
    }

    @Bean
    public GroupMessageRepository groupMessageRepository() {
        return mock(GroupMessageRepository.class);
    }

    @Bean
    public PrivateMessageRepository privateMessageRepository() {
        return mock(PrivateMessageRepository.class);
    }
}
