package com.getjavajob.training.web1801.orlovskiiv.security;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountRole;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.configuration.TestServiceConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRole.ADMIN;
import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRole.USER;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestServiceConfiguration.class)
public class UserDetailsServiceImplTest {
    private static final String ROLE_USER = "ROLE_USER";
    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    @Autowired
    private AccountService accountService;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Test
    public void testLoadUserByUsernameUser() {
        when(accountService.getAccountByEmail("vvv")).thenReturn(getAccount(USER));
        assertEquals(new User("vvv", "xxx",
                getSecurityRoles(ROLE_USER)), userDetailsService.loadUserByUsername("vvv"));
    }

    @Test
    public void testLoadUserByUsernameAdmin() {
        when(accountService.getAccountByEmail("vvv")).thenReturn(getAccount(ADMIN));
        assertEquals(new User("vvv", "xxx",
                getSecurityRoles(ROLE_ADMIN)), userDetailsService.loadUserByUsername("vvv"));
    }

    private Account getAccount(AccountRole role) {
        Account account = new Account();
        account.setEmail("vvv");
        account.setPassword("xxx");
        account.setRole(role);
        return account;
    }

    private Set<GrantedAuthority> getSecurityRoles(String securityRole) {
        Set<GrantedAuthority> roles = new HashSet<>();
        roles.add(new SimpleGrantedAuthority(securityRole));
        return roles;
    }
}
