package com.getjavajob.training.web1801.orlovskiiv.group;

import com.getjavajob.training.web1801.orlovskiiv.configuration.TestServiceConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.jpa.group.GroupMemberRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.group.GroupRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus.*;
import static java.time.LocalDate.of;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestServiceConfiguration.class)
public class GroupServiceTest {
    @Autowired
    private GroupService groupService;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private GroupMemberRepository groupMemberRepository;

    @Test
    public void testGetGroupById() {
        Group group = getGroup1();
        when(groupRepository.findById(1)).thenReturn(Optional.of(group));
        assertEquals(group, groupService.getGroupById(1));
    }

    @Test
    public void testDeleteGroupByIdTrue() {
        when(groupRepository.updateDeletedField(true, 1)).thenReturn(1);
        assertTrue(groupService.deleteGroupById(1));
    }

    @Test
    public void testDeleteGroupByIdFalse() {
        when(groupRepository.updateDeletedField(true, 1)).thenReturn(0);
        assertFalse(groupService.deleteGroupById(1));
    }

    @Test
    public void testCreateGroup() {
        Group group = getGroup1();
        when(groupRepository.save(group)).thenReturn(group);
        when(groupMemberRepository.save(new GroupMember(1, 1, MODER)))
                .thenReturn(new GroupMember(1, 1, MODER));
        assertEquals(group, groupService.createGroup(group));
    }

    @Test
    public void testUpdateGroup() {
        Group group = getGroup1();
        when(groupRepository.save(group)).thenReturn(group);
        assertEquals(group, groupService.updateGroup(group));
    }

    @Test
    public void testRecoverGroupByIdTrue() {
        when(groupRepository.updateDeletedField(false, 1)).thenReturn(1);
        assertTrue(groupService.recoverGroupById(1));
    }

    @Test
    public void testRecoverGroupByIdFalse() {
        when(groupRepository.updateDeletedField(false, 1)).thenReturn(0);
        assertFalse(groupService.recoverGroupById(1));
    }

    @Test
    public void testGroupNameExistsTrue() {
        when(groupRepository.findByName("Name")).thenReturn(new Group());
        assertTrue(groupService.groupNameExists("Name"));
    }

    @Test
    public void testGroupNameExistsFalse() {
        when(groupRepository.findByName("News")).thenReturn(null);
        assertFalse(groupService.groupNameExists("News"));
    }

    @Test
    public void testGetGroupsByAccountIdAndStatus() {
        Set<Group> groups = new LinkedHashSet<>();
        groups.add(getGroup1());
        groups.add(getGroup2());
        when(groupRepository.selectGroupsByAccountIdAndStatus(1, USER)).thenReturn(groups);
        assertEquals(groups, groupService.getGroupsByAccountIdAndStatus(1, USER));
    }

    @Test
    public void testGetFoundGroups() {
        List<Group> groups = new ArrayList<>();
        groups.add(getGroup1());
        Page<Group> expectedPage = new PageImpl<>(groups, PageRequest.of(0, 5), groups.size());
        when(groupRepository.searchByName("N", PageRequest.of(0, 5))).thenReturn(expectedPage);
        assertEquals(expectedPage, groupService.getFoundGroups("N", 0, 5));
    }

    @Test
    public void testDeleteGroupMemberById() {
        groupService.deleteGroupMemberById(1, 1);
    }

    @Test
    public void testCreateUpdateGroupMember() {
        when(groupMemberRepository.save(new GroupMember(1, 1, PENDING)))
                .thenReturn(new GroupMember(1, 1, PENDING));
        assertEquals(new GroupMember(1, 1, PENDING),
                groupService.createUpdateGroupMember(1, 1, PENDING));
    }

    @Test
    public void testGetMemberStatusUser() {
        when(groupMemberRepository.findById(new GroupMemberId(1, 1)))
                .thenReturn(Optional.of(new GroupMember(1, 1, USER)));
        assertEquals(USER, groupService.getMemberStatus(1, 1));
    }

    @Test
    public void testGetMemberStatusNone() {
        when(groupMemberRepository.findById(new GroupMemberId(1, 1))).thenReturn(Optional.empty());
        assertEquals(NONE, groupService.getMemberStatus(1, 1));
    }

    private Group getGroup1() {
        Group group = new Group();
        group.setId(1);
        group.setName("News");
        group.setDescription("Good news");
        group.setCreatorId(1);
        group.setPhoto(new byte[]{51});
        group.setDateOfCreation(of(2018, 3, 1));
        return group;
    }

    private Group getGroup2() {
        Group group = new Group();
        group.setId(2);
        group.setName("FF");
        group.setDescription("FF");
        group.setCreatorId(1);
        group.setDateOfCreation(of(2018, 3, 1));
        return group;
    }
}
