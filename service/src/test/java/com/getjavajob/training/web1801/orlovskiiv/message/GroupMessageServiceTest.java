package com.getjavajob.training.web1801.orlovskiiv.message;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.configuration.TestServiceConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.AccountRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.message.GroupMessageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static java.time.LocalDate.of;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestServiceConfiguration.class)
public class GroupMessageServiceTest {
    @Autowired
    private GroupMessageService groupMessageService;
    @Autowired
    private GroupMessageRepository groupMessageRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void testGetMessageById() {
        GroupMessage message = getMessage1();
        when(groupMessageRepository.findById(1)).thenReturn(Optional.of(message));
        assertEquals(message, groupMessageService.getMessageById(1));
    }

    @Test
    public void testDeleteMessageByIdTrue() {
        when(groupMessageRepository.updateRemovedField(1)).thenReturn(1);
        assertTrue(groupMessageService.deleteMessageById(1));
    }

    @Test
    public void testDeleteMessageByIdFalse() {
        when(groupMessageRepository.updateRemovedField(1)).thenReturn(0);
        assertFalse(groupMessageService.deleteMessageById(1));
    }

    @Test
    public void testCreateUpdateMessage() {
        GroupMessage message = getMessage1();
        when(groupMessageRepository.save(message)).thenReturn(message);
        assertEquals(message, groupMessageService.createUpdateMessage(message));
    }

    @Test
    public void testGetGroupMessagesWithAuthor() {
        GroupMessage message1 = getMessage1();
        GroupMessage message2 = getMessage2();
        Set<GroupMessage> messages = new LinkedHashSet<>();
        messages.add(message1);
        messages.add(message2);
        Account account = getAccount();
        Map<GroupMessage, Account> messagesWithAuthor = new LinkedHashMap<>();
        messagesWithAuthor.put(message1, account);
        messagesWithAuthor.put(message2, account);

        when(groupMessageRepository
                .findByIdTargetAndRemovedOrderByDateOfCreationDesc(2, false)).thenReturn(messages);
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        assertEquals(messagesWithAuthor, groupMessageService.getGroupMessagesWithAuthor(2));
    }

    private GroupMessage getMessage1() {
        GroupMessage message = new GroupMessage();
        message.setId(1);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("Text");
        return message;
    }

    private GroupMessage getMessage2() {
        GroupMessage message = new GroupMessage();
        message.setId(2);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("FF");
        return message;
    }

    private Account getAccount() {
        Account account = new Account();
        account.setId(1);
        account.setFirstName("TTT");
        account.setLastName("TT");
        account.setEmail("ttt@tt.ru");
        account.setPassword("123456");
        account.setRegistration(of(2018, 1, 12));
        return account;
    }
}
