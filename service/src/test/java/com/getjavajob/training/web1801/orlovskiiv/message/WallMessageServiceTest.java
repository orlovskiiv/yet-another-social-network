package com.getjavajob.training.web1801.orlovskiiv.message;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.configuration.TestServiceConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.AccountRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.message.WallMessageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static java.time.LocalDate.of;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestServiceConfiguration.class)
public class WallMessageServiceTest {
    @Autowired
    private WallMessageService wallMessageService;
    @Autowired
    private WallMessageRepository wallMessageRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void testGetMessageById() {
        WallMessage message = getMessage1();
        when(wallMessageRepository.findById(1)).thenReturn(Optional.of(message));
        assertEquals(message, wallMessageService.getMessageById(1));
    }

    @Test
    public void testDeleteMessageByIdTrue() {
        when(wallMessageRepository.updateRemovedField(1)).thenReturn(1);
        assertTrue(wallMessageService.deleteMessageById(1));
    }

    @Test
    public void testDeleteMessageByIdFalse() {
        when(wallMessageRepository.updateRemovedField(1)).thenReturn(0);
        assertFalse(wallMessageService.deleteMessageById(1));
    }

    @Test
    public void testCreateUpdateMessage() {
        WallMessage message = getMessage1();
        when(wallMessageRepository.save(message)).thenReturn(message);
        assertEquals(message, wallMessageService.createUpdateMessage(message));
    }

    @Test
    public void testGetUserMessagesWithAuthor() {
        WallMessage message1 = getMessage1();
        WallMessage message2 = getMessage2();
        Set<WallMessage> messages = new LinkedHashSet<>();
        messages.add(getMessage1());
        messages.add(getMessage2());
        Account account = getAccount();
        Map<WallMessage, Account> messagesWithAuthor = new LinkedHashMap<>();
        messagesWithAuthor.put(message1, account);
        messagesWithAuthor.put(message2, account);

        when(wallMessageRepository.findByIdTargetAndRemovedOrderByDateOfCreationDesc(2, false)).thenReturn(messages);
        when(accountRepository.findById(account.getId())).thenReturn(Optional.of(account));
        assertEquals(messagesWithAuthor, wallMessageService.getUserMessagesWithAuthor(2));
    }

    private WallMessage getMessage1() {
        WallMessage message = new WallMessage();
        message.setId(1);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("Text");
        return message;
    }

    private WallMessage getMessage2() {
        WallMessage message = new WallMessage();
        message.setId(2);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("FF");
        return message;
    }

    private Account getAccount() {
        Account account = new Account();
        account.setId(1);
        account.setFirstName("TTT");
        account.setLastName("TT");
        account.setEmail("ttt@tt.ru");
        account.setPassword("123456");
        account.setRegistration(of(2018, 1, 12));
        return account;
    }
}
