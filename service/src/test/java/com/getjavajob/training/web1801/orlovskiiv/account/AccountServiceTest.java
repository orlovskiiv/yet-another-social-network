package com.getjavajob.training.web1801.orlovskiiv.account;

import com.getjavajob.training.web1801.orlovskiiv.account.address.Address;
import com.getjavajob.training.web1801.orlovskiiv.account.address.AddressType;
import com.getjavajob.training.web1801.orlovskiiv.account.friend.Friendship;
import com.getjavajob.training.web1801.orlovskiiv.account.friend.FriendshipId;
import com.getjavajob.training.web1801.orlovskiiv.account.phone.Phone;
import com.getjavajob.training.web1801.orlovskiiv.account.phone.PhoneType;
import com.getjavajob.training.web1801.orlovskiiv.configuration.TestServiceConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.AccountRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.FriendshipRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRelationshipStatus.*;
import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRole.ADMIN;
import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRole.USER;
import static com.getjavajob.training.web1801.orlovskiiv.account.address.AddressType.HOME;
import static com.getjavajob.training.web1801.orlovskiiv.account.friend.FriendStatus.*;
import static com.getjavajob.training.web1801.orlovskiiv.account.phone.PhoneType.PERSONAL;
import static com.getjavajob.training.web1801.orlovskiiv.account.phone.PhoneType.WORKING;
import static java.time.LocalDate.of;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestServiceConfiguration.class)
public class AccountServiceTest {
    @Autowired
    private AccountService accountService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private FriendshipRepository friendshipRepository;

    @Test
    public void testGetFullAccountById() {
        Account account = getFullInfoAccount();
        when(accountRepository.findFullAccountById(1)).thenReturn(account);
        assertEquals(getFullInfoAccount(), accountService.getFullAccountById(1));
    }

    @Test
    public void testGetFullAccountByIdNull() {
        when(accountRepository.findFullAccountById(1)).thenReturn(null);
        assertNull(accountService.getFullAccountById(1));
    }

    @Test
    public void testGetAccountById() {
        when(accountRepository.findById(2)).thenReturn(Optional.of(getMinInfoAccount()));
        assertEquals(getMinInfoAccount(), accountService.getAccountById(2));
    }

    @Test
    public void testCreateFullAccount() {
        Account expectedAccount = getFullInfoAccount();
        when(accountRepository.save(expectedAccount)).thenReturn(expectedAccount);
        assertEquals(expectedAccount, accountService.createUpdateAccount(expectedAccount));
    }

    @Test
    public void testCreateMinAccount() {
        Account expectedAccount = getMinInfoAccount();
        when(accountRepository.save(expectedAccount)).thenReturn(expectedAccount);
        assertEquals(expectedAccount, accountService.createUpdateAccount(expectedAccount));
    }

    @Test
    public void testGetAccountByEmail() {
        Account expectedAccount = getMinInfoAccount();
        when(accountRepository.findAccountByEmail("ttt@tth.ru")).thenReturn(expectedAccount);
        assertEquals(expectedAccount, accountService.getAccountByEmail("ttt@tth.ru"));
    }

    @Test
    public void testDeleteAccountByIdTrue() {
        when(accountRepository.updateDeletedField(true, 1)).thenReturn(1);
        assertTrue(accountService.deleteAccountById(getAccount().getId()));
    }

    @Test
    public void testDeleteAccountByIdFalse() {
        when(accountRepository.updateDeletedField(true, 1)).thenReturn(0);
        assertFalse(accountService.deleteAccountById(getAccount().getId()));
    }

    @Test
    public void testRecoverAccountByIdTrue() {
        when(accountRepository.updateDeletedField(false, 1)).thenReturn(1);
        assertTrue(accountService.recoverAccountById(1));
    }

    @Test
    public void testRecoverAccountByIdFalse() {
        when(accountRepository.updateDeletedField(false, 1)).thenReturn(0);
        assertFalse(accountService.recoverAccountById(1));
    }

    @Test
    public void testEmailExistsTrue() {
        when(accountRepository.findAccountByEmail("ttt@ttn.ru")).thenReturn(new Account());
        assertTrue(accountService.emailExists("ttt@ttn.ru"));
    }

    @Test
    public void testEmailExistsFalse() {
        when(accountRepository.findAccountByEmail("swef@fgw.ru")).thenReturn(null);
        assertFalse(accountService.emailExists("swef@fgw.ru"));
    }

    @Test
    public void testAddFriend() {
        when(friendshipRepository.save(new Friendship(1, 2, PENDING, 1)))
                .thenReturn(new Friendship(1, 2, PENDING, 1));
        assertEquals(new Friendship(1, 2, PENDING, 1), accountService.addFriend(1, 2));
    }

    @Test
    public void testWithdrawFriendRequest() {
        accountService.withdrawFriendRequest(1, 2);
    }

    @Test
    public void testAcceptFriendRequestTrue() {
        when(friendshipRepository.updateFriendStatus(ACCEPTED, 2, 1, 2)).thenReturn(1);
        assertTrue(accountService.acceptFriendRequest(2, 1));
    }

    @Test
    public void testAcceptFriendRequestFalse() {
        when(friendshipRepository.updateFriendStatus(ACCEPTED, 1, 1, 2)).thenReturn(0);
        assertFalse(accountService.acceptFriendRequest(1, 2));
    }

    @Test
    public void testDeclineFriendRequestTrue() {
        when(friendshipRepository.updateFriendStatus(DECLINED, 1, 1, 2)).thenReturn(1);
        assertTrue(accountService.declineFriendRequest(1, 2));
    }

    @Test
    public void testDeclineFriendRequestFalse() {
        when(friendshipRepository.updateFriendStatus(DECLINED, 1, 1, 2)).thenReturn(0);
        assertFalse(accountService.declineFriendRequest(1, 2));
    }

    @Test
    public void testGetFriends() {
        Set<Account> accounts = new LinkedHashSet<>();
        accounts.add(getMinInfoAccount());
        when(accountRepository.getFriends(1)).thenReturn(accounts);
        assertEquals(accounts, accountService.getFriends(1));
    }

    @Test
    public void testGetFriendRequests() {
        Set<Account> accounts = new LinkedHashSet<>();
        accounts.add(getMinInfoAccount());
        when(accountRepository.getFriendRequests(1)).thenReturn(accounts);
        assertEquals(accounts, accountService.getFriendRequests(1));
    }

    @Test
    public void testGetSentRequests() {
        Set<Account> unconfirmedAccounts = new LinkedHashSet<>();
        unconfirmedAccounts.add(getAccount());
        Set<Account> declinedAccounts = new LinkedHashSet<>();
        declinedAccounts.add(getMinInfoAccount());

        Set<Account> allAccounts = new LinkedHashSet<>();
        allAccounts.addAll(unconfirmedAccounts);
        allAccounts.addAll(declinedAccounts);

        when(accountRepository.getSentRequestsUnconfirmed(3)).thenReturn(unconfirmedAccounts);
        when(accountRepository.getSentRequestsDeclined(3)).thenReturn(declinedAccounts);
        assertEquals(allAccounts, accountService.getSentRequests(3));
    }

    @Test
    public void testGetSubscribers() {
        Set<Account> accounts = new LinkedHashSet<>();
        accounts.add(getMinInfoAccount());
        when(accountRepository.getSubscribers(1)).thenReturn(accounts);
        assertEquals(accounts, accountService.getSubscribers(1));
    }

    @Test
    public void testGetAllFoundAccounts() {
        List<Account> accounts = new ArrayList<>();
        accounts.add(getAccount());
        accounts.add(getMinInfoAccount());
        Page<Account> expectedPage = new PageImpl<>(accounts, PageRequest.of(0, 5), accounts.size());
        when(accountRepository.searchByFirstAndLastName("T", PageRequest.of(0, 5))).thenReturn(expectedPage);
        assertEquals(expectedPage, accountService.getAllFoundAccounts("T", 0, 5));
    }

    @Test
    public void testGetAccountStatusFriend() {
        when(friendshipRepository.findById(new FriendshipId(1, 2)))
                .thenReturn(Optional.of(new Friendship(1, 2, ACCEPTED, 1)));
        assertEquals(FRIEND, accountService.getAccountStatus(1, 2));
    }

    @Test
    public void testGetAccountStatusFriendRequest() {
        when(friendshipRepository.findById(new FriendshipId(1, 2)))
                .thenReturn(Optional.of(new Friendship(1, 2, PENDING, 1)));
        assertEquals(FRIEND_REQUEST, accountService.getAccountStatus(2, 1));
    }

    @Test
    public void testGetAccountStatusSentRequest() {
        when(friendshipRepository.findById(new FriendshipId(1, 2)))
                .thenReturn(Optional.of(new Friendship(1, 2, PENDING, 1)));
        assertEquals(SENT_REQUEST, accountService.getAccountStatus(1, 2));
    }

    @Test
    public void testGetAccountStatusSentRequestDeclined() {
        when(friendshipRepository.findById(new FriendshipId(1, 2)))
                .thenReturn(Optional.of(new Friendship(1, 2, DECLINED, 1)));
        assertEquals(SENT_REQUEST, accountService.getAccountStatus(2, 1));
    }

    @Test
    public void testGetAccountStatusSubscriber() {
        when(friendshipRepository.findById(new FriendshipId(1, 2)))
                .thenReturn(Optional.of(new Friendship(1, 2, DECLINED, 1)));
        assertEquals(SUBSCRIBER, accountService.getAccountStatus(1, 2));
    }

    @Test
    public void testGetAccountStatusNone() {
        when(friendshipRepository.findById(new FriendshipId(1, 2))).thenReturn(Optional.empty());
        assertEquals(NONE, accountService.getAccountStatus(1, 2));
    }

    @Test
    public void testGetAccountStatusInvalid() {
        when(friendshipRepository.findById(new FriendshipId(1, 2)))
                .thenReturn(Optional.of(new Friendship(1, 2, null, 1)));
        assertEquals(INVALID_STATUS, accountService.getAccountStatus(1, 2));
    }

    @Test
    public void testGetAccountsInGroup() {
        Set<Account> accounts = new LinkedHashSet<>();
        accounts.add(getAccount());
        accounts.add(getMinInfoAccount());
        when(accountRepository.getAccountsInGroup(1, GroupMemberStatus.USER)).thenReturn(accounts);
        assertEquals(accounts, accountService.getAccountsInGroup(1, GroupMemberStatus.USER));
    }

    private Account getMinInfoAccount() {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("TTT");
        account.setLastName("TT");
        account.setEmail("ttt@tt.ru");
        account.setPassword("1234567");
        account.setRole(USER);
        account.setRegistration(of(2018, 1, 12));
        return account;
    }

    private Account getFullInfoAccount() {
        Account account = getAccount();
        account.setPersonalPhones(getPersonalPhones(account));
        account.setWorkingPhones(getWorkingPhones(account));
        account.setHomeAddress(getFullHomeAddress(account));
        account.setWorkingAddress(getFullWorkingAddress(account));
        return account;
    }

    private Account getAccount() {
        Account account = new Account();
        account.setId(1);
        account.setFirstName("TTTT");
        account.setLastName("TT");
        account.setMiddleName("T");
        account.setDateOfBirth(of(1999, 12, 31));
        account.setEmail("tttt@tt.ru");
        account.setPassword("999");
        account.setIcq("654321");
        account.setSkype("Tttttt");
        account.setAdditionalInformation("asdfgh");
        account.setRole(ADMIN);
        account.setPhoto(new byte[]{33});
        account.setRegistration(of(2018, 2, 22));
        return account;
    }

    private Set<Phone> getPersonalPhones(Account account) {
        Set<Phone> personalPhones = new HashSet<>();
        Phone phone1 = getPhoneAccountId1(PERSONAL, "00000000000");
        phone1.setAccount(account);
        personalPhones.add(phone1);

        Phone phone2 = getPhoneAccountId1(PERSONAL, "11111111111");
        phone2.setAccount(account);
        personalPhones.add(phone2);
        return personalPhones;
    }

    private Set<Phone> getWorkingPhones(Account account) {
        Set<Phone> workingPhones = new HashSet<>();
        Phone phone1 = getPhoneAccountId1(WORKING, "99999999999");
        phone1.setAccount(account);
        workingPhones.add(phone1);

        Phone phone2 = getPhoneAccountId1(WORKING, "88888888888");
        phone2.setAccount(account);
        workingPhones.add(phone2);
        return workingPhones;
    }

    private Phone getPhoneAccountId1(PhoneType type, String s) {
        return new Phone(null, type, s);
    }

    private Set<Address> getFullHomeAddress(Account account) {
        Address homeAddress = new Address();
        homeAddress.setId(1);
        homeAddress.setAccount(account);
        homeAddress.setType(HOME);
        homeAddress.setCountry("Russia");
        homeAddress.setRegion("Krim");
        homeAddress.setCity("Sevastopol");
        homeAddress.setStreet("000");
        homeAddress.setHouse("999");
        Set<Address> addresses = new HashSet<>();
        addresses.add(homeAddress);
        return addresses;
    }

    private Set<Address> getFullWorkingAddress(Account account) {
        Address workingAddress = new Address();
        workingAddress.setId(1);
        workingAddress.setAccount(account);
        workingAddress.setType(AddressType.WORKING);
        workingAddress.setCountry("Russia");
        workingAddress.setRegion("Moscow region");
        workingAddress.setCity("PPP");
        workingAddress.setStreet("123");
        workingAddress.setHouse("321");
        Set<Address> addresses = new HashSet<>();
        addresses.add(workingAddress);
        return addresses;
    }
}
