package com.getjavajob.training.web1801.orlovskiiv.message;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.configuration.TestServiceConfiguration;
import com.getjavajob.training.web1801.orlovskiiv.jpa.account.AccountRepository;
import com.getjavajob.training.web1801.orlovskiiv.jpa.message.PrivateMessageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static java.time.LocalDate.of;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestServiceConfiguration.class)
public class PrivateMessageServiceTest {
    @Autowired
    private PrivateMessageService privateMessageService;
    @Autowired
    private PrivateMessageRepository privateMessageRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void testGetMessageById() {
        PrivateMessage message = getMessage1();
        when(privateMessageRepository.findById(1)).thenReturn(Optional.of(message));
        assertEquals(message, privateMessageService.getMessageById(1));
    }

    @Test
    public void testDeleteMessageByIdTrue() {
        when(privateMessageRepository.updateAllRemovedFields(1)).thenReturn(1);
        assertTrue(privateMessageService.deleteMessageById(1));
    }

    @Test
    public void testDeleteMessageByIdFalse() {
        when(privateMessageRepository.updateAllRemovedFields(1)).thenReturn(0);
        assertFalse(privateMessageService.deleteMessageById(1));
    }

    @Test
    public void testCreateUpdateMessage() {
        PrivateMessage message = getMessage1();
        when(privateMessageRepository.save(message)).thenReturn(message);
        assertEquals(message, privateMessageService.createUpdateMessage(message));
    }

    @Test
    public void testDeleteMessageFromChatTrue() {
        when(privateMessageRepository.updateAuthorRemovedField(1)).thenReturn(1);
        assertTrue(privateMessageService.deleteMessageFromChat(1, true));
    }

    @Test
    public void tesDeleteMessageFromChatFalse() {
        when(privateMessageRepository.updateAuthorRemovedField(1)).thenReturn(0);
        assertFalse(privateMessageService.deleteMessageFromChat(1, false));
    }

    @Test
    public void testGetChatMessages() {
        PrivateMessage message1 = getMessage1();
        PrivateMessage message2 = getMessage2();
        Set<PrivateMessage> messages = new LinkedHashSet<>();
        messages.add(message1);
        messages.add(message2);
        Account account1 = getAccount1();
        Account account2 = getAccount2();

        Map<PrivateMessage, Account> messagesWithAuthor = new LinkedHashMap<>();
        messagesWithAuthor.put(message1, account1);
        messagesWithAuthor.put(message2, account2);

        when(privateMessageRepository.getChatMessages(1, 2)).thenReturn(messages);
        when(accountRepository.findById(account1.getId())).thenReturn(Optional.of(account1));
        when(accountRepository.findById(account2.getId())).thenReturn(Optional.of(account2));
        assertEquals(messagesWithAuthor, privateMessageService.getChatMessages(1, 2));
    }

    @Test
    public void testGetLastMessages() {
        PrivateMessage message2 = getMessage2();
        PrivateMessage message3 = getMessage3();
        Set<PrivateMessage> messages = new LinkedHashSet<>();
        messages.add(message2);
        messages.add(message3);

        Account account2 = getAccount2();
        Account account3 = getAccount3();
        Map<PrivateMessage, Account> messagesWithChatAccount = new LinkedHashMap<>();
        messagesWithChatAccount.put(message2, account2);
        messagesWithChatAccount.put(message3, account3);

        when(privateMessageRepository.getLastMessages(1)).thenReturn(messages);
        when(accountRepository.findById(account2.getId())).thenReturn(Optional.of(account2));
        when(accountRepository.findById(account3.getId())).thenReturn(Optional.of(account3));
        assertEquals(messagesWithChatAccount, privateMessageService.getLastMessages(1));
    }

    private PrivateMessage getMessage1() {
        PrivateMessage message = new PrivateMessage();
        message.setId(1);
        message.setIdAuthor(1);
        message.setIdTarget(2);
        message.setText("Text");
        return message;
    }

    private PrivateMessage getMessage2() {
        PrivateMessage message = new PrivateMessage();
        message.setId(2);
        message.setIdAuthor(2);
        message.setIdTarget(1);
        message.setText("FF");
        return message;
    }

    private PrivateMessage getMessage3() {
        PrivateMessage message = new PrivateMessage();
        message.setId(3);
        message.setIdAuthor(1);
        message.setIdTarget(3);
        message.setText("FF");
        return message;
    }

    private Account getAccount1() {
        Account account = new Account();
        account.setId(1);
        account.setFirstName("TTT");
        account.setLastName("TT");
        account.setEmail("ttt@tt.ru");
        account.setPassword("12345678");
        account.setRegistration(of(2018, 1, 12));
        return account;
    }

    private Account getAccount2() {
        Account account = new Account();
        account.setId(2);
        account.setFirstName("VVV");
        account.setLastName("VV");
        account.setEmail("vvv@vv.ru");
        account.setPassword("123456789");
        account.setRegistration(of(2018, 1, 12));
        return account;
    }

    private Account getAccount3() {
        Account account = new Account();
        account.setId(3);
        account.setFirstName("RRR");
        account.setLastName("RR");
        account.setEmail("rrr@rr.ru");
        account.setPassword("123456");
        account.setRegistration(of(2018, 1, 12));
        return account;
    }
}
