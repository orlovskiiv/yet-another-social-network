<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>${requestScope.group.name} участники</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white border rounded p-3 mb-3">
            <div class="text-center">
                <h2 class="pb-3">Список участников</h2>
            </div>
            <%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>
            <div class="table-responsive">
                <table class="clickableTable table table-hover mb-0">
                    <c:forEach var="moder" items="${requestScope.moderators}">
                        <tr>
                            <td><a href="${pageContext.request.contextPath}/account/${moder.id}"><img
                                    src="${pageContext.request.contextPath}/image/account/${moder.id}"
                                    width="75" class="img-fluid rounded"></a></td>
                            <td class="clickableCell align-middle"><a
                                    href="${pageContext.request.contextPath}/account/${moder.id}">
                                <h6><c:out value="${moder.lastName += ' ' += moder.firstName}"/></h6></a></td>
                            <td class="align-middle text-right"></td>
                        </tr>
                    </c:forEach>
                    <c:forEach var="user" items="${requestScope.users}">
                        <tr>
                            <td><a href="${pageContext.request.contextPath}/account/${user.id}"><img
                                    src="${pageContext.request.contextPath}/image/account/${user.id}"
                                    width="75" class="img-fluid rounded"></a></td>
                            <td class="align-middle"><a href="${pageContext.request.contextPath}/account/${user.id}">
                                <h6><c:out value="${user.lastName += ' ' += user.firstName}"/></h6></a></td>
                            <td class="align-middle text-right">
                                <c:if test="${sessionAccount.id == user.id}">
                                    <a class="btn btn-outline-danger"
                                       href="${pageContext.request.contextPath}/unsubscribe/group/${requestScope.group.id}">Отписаться</a>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>