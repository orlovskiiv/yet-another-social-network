<%@ page contentType="text/html;charset=utf-8" %>

<script src="${pageContext.request.contextPath}/webjars/font-awesome/5.3.1/js/all.min.js"></script>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary" role="navigation">
    <a class="navbar-brand m-0 display-1" href="${pageContext.request.contextPath}/"><i class="fab fa-black-tie"></i>
        Социальная сеть</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent"
            aria-controls="navbarContent" aria-expanded="false" aria-label="Навигация">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarContent">
        <div class="mx-auto">
            <form method="GET" class="form-inline my-2 my-lg-0" action="${pageContext.request.contextPath}/search"
                  role="search">
                <div class="input-group">
                    <input type="text" class="form-control" name="value" id="headerSearch" placeholder="Поиск">
                </div>
                <div class="input-group ml-1">
                    <button type="submit" class="btn btn-light">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </form>
        </div>

        <%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>
        <a class="navbar-brand m-0 display-1 pr-2 pl-2"
           href="<c:out value="${'/account/' += sessionAccount.id}"/>"><c:out
                value="${sessionAccount.lastName += ' ' += sessionAccount.firstName}"/></a>
        <form action="${pageContext.request.contextPath}/logout" method="post">
            <input class="btn btn-light" value="Выйти" type="submit">
        </form>
    </div>
</nav>