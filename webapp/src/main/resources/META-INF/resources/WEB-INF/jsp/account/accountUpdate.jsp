<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Редактирование аккаунта</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white border rounded p-3">
            <div class="text-center">
                <h2 class="pb-5">Редактирование аккаунта</h2>
                <spring:hasBindErrors name="account">
                    <div class="alert alert-danger" role="alert">
                        Ошибка валидации данных
                    </div>
                </spring:hasBindErrors>
            </div>

            <a class="btn btn-primary btn-block mb-3"
               href="${pageContext.request.contextPath}/account/${account.id}/account" download>Выгрузить в xml</a>

            <%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>
            <form method="POST" action="${pageContext.request.contextPath}/account/${account.id}/xml"
                  enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="xml" id="xmlLabel"
                           class="col-md-3 col-form-label text-md-right">Выбрать xml</label>
                    <div class="col-md-9 pl-0">
                        <input type="file" class="form-control-file" id="xml" data-toggle="collapse"
                               accept="text/xml" name="xml">
                    </div>
                </div>

                <input type="submit" class="btn btn-primary btn-block mb-3" value="Загрузить xml">
            </form>

            <%--@elvariable id="account" type="com.getjavajob.training.web1801.orlovskiiv.dto.form.AccountDto"--%>
            <form:form method="POST" modelAttribute="account"
                       action="${pageContext.request.contextPath}/account/${account.id}/update"
                       enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="firstName" id="firstNameLabel"
                           class="col-md-3 col-form-label text-md-right">Имя<sup class="text-danger">*</sup></label>
                    <div class="col-md-9 pl-0">
                        <input type="text"
                               class="form-control" id="firstName" data-toggle="collapse" data-target="#firstNameHelp"
                               name="firstName"
                               value="<c:out value="${requestScope.account.firstName}"/>" required>
                        <small class="form-text text-danger"><form:errors path="firstName"/></small>
                        <div class="collapse" id="firstNameHelp">
                            <small class="form-text text-muted">Максимальная длина 20 символов</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lastName" id="lastNameLabel"
                           class="col-md-3 col-form-label text-md-right">Фамилия<sup class="text-danger">*</sup></label>
                    <div class="col-md-9 pl-0">
                        <input type="text"
                               class="form-control" id="lastName" data-toggle="collapse" data-target="#lastNameHelp"
                               name="lastName"
                               value="<c:out value="${requestScope.account.lastName}"/>" required>
                        <small class="form-text text-danger"><form:errors path="lastName"/></small>
                        <div class="collapse" id="lastNameHelp">
                            <small class="form-text text-muted">Максимальная длина 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="middleName" id="middleNameLabel"
                           class="col-md-3 col-form-label text-md-right">Отчество</label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="middleName" data-toggle="collapse"
                               data-target="#middleNameHelp" name="middleName"
                               value="<c:out value="${requestScope.account.middleName}"/>">
                        <small class="form-text text-danger"><form:errors path="middleName"/></small>
                        <div class="collapse" id="middleNameHelp">
                            <small class="form-text text-muted">Максимальная длина 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="dateOfBirth" id="dateOfBirthLabel" class="col-md-3 col-form-label text-md-right">Дата
                        рождения</label>
                    <div class="col-md-9 pl-0">
                        <input type="date" class="form-control" id="dateOfBirth" data-toggle="collapse"
                               data-target="#dateOfBirthHelp" name="dateOfBirth"
                               value="<c:out value="${requestScope.account.dateOfBirth}"/>">
                        <small class="form-text text-danger"><form:errors path="dateOfBirth"/></small>
                        <div class="collapse.show" id="dateOfBirthHelp" style="display: none">
                            <small class="form-text text-danger">От 1.01.1900 до сегодняшнего дня</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="photo" id="photoLabel"
                           class="col-md-3 col-form-label text-md-right">Изменить фото</label>
                    <div class="col-md-9 pl-0">
                        <input type="file" class="form-control-file" id="photo" data-toggle="collapse"
                               data-target="#photoHelp" accept="image/jpeg" name="photo">
                        <small class="form-text text-danger"><form:errors path="photo"/></small>
                        <div class="collapse" id="photoHelp">
                            <small class="form-text text-muted">Размер фото не более 500 кб</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row pb-5">
                    <label class="form-check-label col-md-3 text-md-right" for="deletePhoto">Удалить фото</label>
                    <div class="col-md-9 pl-3">
                        <input type="checkbox" class="form-check-input" id="deletePhoto" name="deletePhoto"
                               value="deletePhoto" <c:if test="${account.deletePhoto eq 'deletePhoto'}">checked</c:if>>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" id="emailLabel"
                           class="col-md-3 col-form-label text-md-right">Эл. почта<sup
                            class="text-danger">*</sup></label>
                    <div class="col-md-9 pl-0">
                        <input type="email"
                               class="form-control" id="email" data-toggle="collapse" data-target="#emailHelp"
                               name="email"
                               value="<c:out value="${account.email}"/>" required>
                        <small class="form-text text-danger"><form:errors path="email"/></small>
                        <div class="collapse" id="emailHelp">
                            <small class="form-text text-muted emailExample">example@test.com</small>
                            <small class="form-text text-danger duplicateEmail" style="display: none">Пользователь с
                                таким эл. адресом уже
                                существует
                            </small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="oldPassword" id="oldPasswordLabel"
                           class="col-md-3 col-form-label text-md-right">Старый пароль</label>
                    <div class="col-md-9 pl-0">
                        <input type="password" class="form-control" id="oldPassword" data-toggle="collapse"
                               data-target="#oldPasswordHelp"
                               name="oldPassword">
                        <small class="form-text text-danger"><form:errors path="oldPassword"/></small>
                        <div class="collapse invalidOldPassword" id="oldPasswordHelp" style="display: none">
                            <small class="form-text text-danger">Пароль не верен</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" id="passwordLabel"
                           class="col-md-3 col-form-label text-md-right">Пароль</label>
                    <div class="col-md-9 pl-0">
                        <input type="password" class="form-control" id="password" data-toggle="collapse"
                               data-target="#passwordHelp" name="password">
                        <small class="form-text text-danger"><form:errors path="password"/></small>
                        <small class="form-text text-danger"><form:errors/></small>
                        <div class="collapse" id="passwordHelp">
                            <small class="form-text text-muted passwordLength">от 6 до 20 символов</small>
                            <small class="form-text text-muted passwordMismatch" style="display: none">Несовпадение
                                паролей
                            </small>
                        </div>
                    </div>
                </div>
                <div class="form-group row pb-5">
                    <label for="passwordCopy" id="passwordCopyLabel"
                           class="col-md-3 col-form-label text-md-right">Повторите пароль</label>
                    <div class="col-md-9 pl-0">
                        <input type="password" class="form-control" id="passwordCopy" data-toggle="collapse"
                               data-target="#passwordCopyHelp"
                               name="passwordCopy">
                        <small class="form-text text-danger"><form:errors path="passwordCopy"/></small>
                    </div>
                </div>

                <div class="form-group row" id="inputPhone">
                    <label for="phone" id="phoneLabel" class="col-md-3 col-form-label text-md-right">Телефоны</label>
                    <div class="col-md-9 pl-0">
                        <div class="input-group">
                            <input type="text" class="form-control" id="phone" data-toggle="collapse"
                                   data-target="#phoneHelp">
                            <div class="input-group-append">
                                <select class="form-control" id="type" name="type" title="type">
                                    <option>Личный</option>
                                    <option>Рабочий</option>
                                </select>
                                <button class="btn btn-primary add" type="button">Добавить</button>
                            </div>
                        </div>
                        <div class="collapse" id="phoneHelp">
                            <small class="form-text text-muted">Длина номера 11 цифр</small>
                        </div>
                    </div>
                </div>

                <c:forEach var="phone" items="${account.personalPhones}" varStatus="count">
                    <div class="form-group row">
                        <label for="phone" class="col-md-3 col-form-label text-md-right"></label>
                        <div class="col-md-9 pl-0">
                            <div class="input-group">
                                <input type="text" readonly class="form-control personal"
                                       value="<c:out value="${phone.number}"/>"
                                       name="personalPhones[${count.index}].number" title="phone">
                                <div class="input-group-append">
                                    <span class="input-group-text">Личный</span>
                                    <button class="btn btn-outline-danger remove" type="button">Удалить</button>
                                </div>
                            </div>
                            <small class="form-text text-danger"><form:errors
                                    path="personalPhones[${count.index}].number"/></small>
                        </div>
                    </div>
                </c:forEach>
                <c:forEach var="phone" items="${account.workingPhones}" varStatus="count">
                    <div class="form-group row">
                        <label for="phone" class="col-md-3 col-form-label text-md-right"></label>
                        <div class="col-md-9 pl-0">
                            <div class="input-group">
                                <input type="text" readonly class="form-control working"
                                       value="<c:out value="${phone.number}"/>"
                                       name="workingPhones[${count.index}].number" title="phone">
                                <div class="input-group-append">
                                    <span class="input-group-text">Рабочий</span>
                                    <button class="btn btn-outline-danger remove" type="button">Удалить</button>
                                </div>
                            </div>
                            <small class="form-text text-danger"><form:errors
                                    path="workingPhones[${count.index}].number"/></small>
                        </div>
                    </div>
                </c:forEach>

                <div class="form-group row">
                    <label for="homeCountry" id="homeCountryLabel"
                           class="col-md-3 col-form-label text-md-right">Домашний адрес</label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="homeCountry" data-toggle="collapse"
                               data-target="#homeCountryHelp" name="homeAddress.country"
                               placeholder="Страна" value="<c:out value="${account.homeAddress.country}"/>">
                        <small class="form-text text-danger"><form:errors path="homeAddress.country"/></small>
                        <div class="collapse" id="homeCountryHelp">
                            <small class="form-text text-muted">До 20 символов</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="homeRegion" id="homeRegionLabel" class="col-md-3 col-form-label text-md-right"></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="homeRegion" data-toggle="collapse"
                               data-target="#homeRegionHelp" name="homeAddress.region"
                               placeholder="Регион" value="<c:out value="${account.homeAddress.region}"/>">
                        <small class="form-text text-danger"><form:errors path="homeAddress.region"/></small>
                        <div class="collapse" id="homeRegionHelp">
                            <small class="form-text text-muted">До 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="homeCity" id="homeCityLabel" class="col-md-3 col-form-label text-md-right"></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="homeCity" data-toggle="collapse"
                               data-target="#homeCityHelp" name="homeAddress.city"
                               placeholder="Город" value="<c:out value="${account.homeAddress.city}"/>">
                        <small class="form-text text-danger"><form:errors path="homeAddress.city"/></small>
                        <div class="collapse" id="homeCityHelp">
                            <small class="form-text text-muted">До 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="homeStreet" id="homeStreetLabel" class="col-md-3 col-form-label text-md-right"></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="homeStreet" data-toggle="collapse"
                               data-target="#homeStreetHelp" name="homeAddress.street"
                               placeholder="Улица" value="<c:out value="${account.homeAddress.street}"/>">
                        <small class="form-text text-danger"><form:errors path="homeAddress.street"/></small>
                        <div class="collapse" id="homeStreetHelp">
                            <small class="form-text text-muted">До 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row pb-5">
                    <label for="homeHouse" id="homeHouseLabel" class="col-md-3 col-form-label text-md-right"></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="homeHouse" data-toggle="collapse"
                               data-target="#homeHouseHelp" name="homeAddress.house"
                               placeholder="Дом" value="<c:out value="${account.homeAddress.house}"/>">
                        <small class="form-text text-danger"><form:errors path="homeAddress.house"/></small>
                        <div class="collapse" id="homeHouseHelp">
                            <small class="form-text text-muted">До 20 символов</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="workingCountry" id="workingCountryLabel"
                           class="col-md-3 col-form-label text-md-right">Рабочий адрес</label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="workingCountry" data-toggle="collapse"
                               data-target="#workingCountryHelp"
                               name="workingAddress.country" placeholder="Страна"
                               value="<c:out value="${account.workingAddress.country}"/>">
                        <small class="form-text text-danger"><form:errors path="workingAddress.country"/></small>
                        <div class="collapse" id="workingCountryHelp">
                            <small class="form-text text-muted">До 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="workingRegion" id="workingRegionLabel"
                           class="col-md-3 col-form-label text-md-right"></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="workingRegion" data-toggle="collapse"
                               data-target="#workingRegionHelp"
                               name="workingAddress.region" placeholder="Регион"
                               value="<c:out value="${account.workingAddress.region}"/>">
                        <small class="form-text text-danger"><form:errors path="workingAddress.region"/></small>
                        <div class="collapse" id="workingRegionHelp">
                            <small class="form-text text-muted">До 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="workingCity" id="workingCityLabel"
                           class="col-md-3 col-form-label text-md-right"></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="workingCity" data-toggle="collapse"
                               data-target="#workingCityHelp" name="workingAddress.city"
                               placeholder="Город" value="<c:out value="${account.workingAddress.city}"/>">
                        <small class="form-text text-danger"><form:errors path="workingAddress.city"/></small>
                        <div class="collapse" id="workingCityHelp">
                            <small class="form-text text-muted">До 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="workingStreet" id="workingStreetLabel"
                           class="col-md-3 col-form-label text-md-right"></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="workingStreet" data-toggle="collapse"
                               data-target="#workingStreetHelp"
                               name="workingAddress.street" placeholder="Улица"
                               value="<c:out value="${account.workingAddress.street}"/>">
                        <small class="form-text text-danger"><form:errors path="workingAddress.street"/></small>
                        <div class="collapse" id="workingStreetHelp">
                            <small class="form-text text-muted">До 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row pb-5">
                    <label for="workingHouse" id="workingHouseLabel"
                           class="col-md-3 col-form-label text-md-right"></label>
                    <div class="col-md-9 pl-0">
                        <input type="text"
                               class="form-control" id="workingHouse" data-toggle="collapse"
                               data-target="#workingHouseHelp"
                               name="workingAddress.house" placeholder="Дом"
                               value="<c:out value="${account.workingAddress.house}"/>">
                        <small class="form-text text-danger"><form:errors path="workingAddress.house"/></small>
                        <div class="collapse" id="workingHouseHelp">
                            <small class="form-text text-muted">До 20 символов</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="icq" id="icqLabel" class="col-md-3 col-form-label text-md-right">ICQ</label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="icq" data-toggle="collapse" data-target="#icqHelp"
                               name="icq"
                               value="<c:out value="${account.icq}"/>">
                        <small class="form-text text-danger"><form:errors path="icq"/></small>
                        <div class="collapse" id="icqHelp">
                            <small class="form-text text-muted">От 5 до 9 цифр</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="skype" id="skypeLabel"
                           class="col-md-3 col-form-label text-md-right">Skype</label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="skype" data-toggle="collapse"
                               data-target="#skypeHelp" name="skype"
                               value="<c:out value="${account.skype}"/>">
                        <small class="form-text text-danger"><form:errors path="skype"/></small>
                        <div class="collapse" id="skypeHelp">
                            <small class="form-text text-muted">До 32 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row pb-3">
                    <label for="additionalInformation" id="additionalLabel"
                           class="col-md-3 col-form-label text-md-right">Дополнительная информация</label>
                    <div class="col-md-9 pl-0">
                        <textarea class="form-control" id="additionalInformation" data-toggle="collapse"
                                  data-target="#additionalInformationHelp" rows="5"
                                  name="additionalInformation"><c:out
                                value="${account.additionalInformation}"/></textarea>
                        <small class="form-text text-danger"><form:errors path="additionalInformation"/></small>
                        <div class="collapse" id="additionalInformationHelp">
                            <small class="form-text text-muted">До 255 символов</small>
                        </div>
                    </div>
                </div>

                <c:if test="${sessionAccount.admin}">
                    <div class="form-group row pb-3">
                        <label class="form-check-label col-md-3 text-md-right"
                               for="customControlAdmin">Администратор</label>
                        <div class="col-md-9 pl-3">
                            <input type="checkbox" class="form-check-input" id="customControlAdmin" name="admin"
                                   value="admin" <c:if test="${account.admin == 'admin'}">checked</c:if>>
                        </div>
                    </div>
                </c:if>

                <button type="button" class="btn btn-primary btn-block" data-toggle="modal"
                        data-target="#updateModal">Обновить аккаунт
                </button>
                <div class="modal fade" id="updateModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="updateModalTitle">Подтвердите обновление аккаунта</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Вы действительно хотите обновить аккаунт?
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary btn-block" value="Обновить">
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>

        <c:if test="${sessionAccount.admin}">
            <div class="col-md-8 offset-md-2 p-0">
                <div class="col-md-6 offset-md-3 p-3">
                    <button type="button" class="btn btn-block btn-outline-danger btn-sm" data-toggle="modal"
                            data-target="#deleteModal">Удалить аккаунт
                    </button>
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Подтвердите удаление</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Вы действительно хотите удалить аккаунт?
                                </div>
                                <div class="modal-footer">
                                    <a class="btn btn-danger btn-block"
                                       href="${pageContext.request.contextPath}/account/${account.id}/update/deleteAccount">Удалить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/validation.js"></script>
</body>
</html>