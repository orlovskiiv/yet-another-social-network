<%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>
<%@ page contentType="text/html;charset=utf-8" %>

<div class="col-md-2 p-0 ml-1 mr-1">
    <table class="clickableTable table table-hover">
        <tr>
            <td class="clickableCell"><a href="${pageContext.request.contextPath += '/account/' += sessionAccount.id}">Моя
                страница</a></td>
        </tr>
        <tr>
            <td class="clickableCell"><a
                    href="${pageContext.request.contextPath += '/account/' += sessionAccount.id += "/messages"}">Сообщения</a>
            </td>
        </tr>
        <tr>
            <td class="clickableCell"><a
                    href="${pageContext.request.contextPath += '/account/' += sessionAccount.id += "/friends"}">Друзья</a>
            </td>
        </tr>
        <tr>
            <td class="clickableCell"><a
                    href="${pageContext.request.contextPath += '/account/' += sessionAccount.id += "/groups"}">Группы</a>
            </td>
        </tr>
    </table>
</div>