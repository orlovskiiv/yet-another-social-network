<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Удаление аккаунта</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>

<%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>
<c:choose>
    <c:when test="${sessionAccount.admin}">
        <%@ include file="../include/header.jsp" %>
    </c:when>
    <c:otherwise>
        <%@ include file="../include/emptyHeader.jsp" %>
    </c:otherwise>
</c:choose>

<div class="container mt-2">
    <div class="row">
        <div class="col-md-8 offset-md-2 bg-white p-3 mt-3 border rounded">
            <div class="text-center">
                <c:choose>
                    <c:when test="${requestScope.status}">
                        <h1 class="pb-2">Аккаунт успешно удален</h1>
                    </c:when>
                    <c:when test="${not requestScope.status}">
                        <h1 class="pb-2">Ошибка при удалении аккаунта ID=${requestScope.id}</h1>
                    </c:when>
                </c:choose>
                <h3><a href="${pageContext.request.contextPath}/">На стартовую страницу</a></h3>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>