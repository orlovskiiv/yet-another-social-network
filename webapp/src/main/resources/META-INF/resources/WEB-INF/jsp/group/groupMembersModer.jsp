<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>${requestScope.group.name} участники</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>

<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white border rounded p-3 mb-3">
            <div class="text-center">
                <h2 class="pb-3">Список участников</h2>
            </div>

            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                       aria-controls="nav-home" aria-selected="true">Участники</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                       aria-controls="nav-profile" aria-selected="false">Заявки</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">

                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="table-responsive">
                        <table class="clickableTable table table-hover mb-0">
                            <c:forEach var="moder" items="${requestScope.moderators}">
                                <tr>
                                    <td><a href="${pageContext.request.contextPath}/account/${moder.id}"><img
                                            src="${pageContext.request.contextPath}/image/account/${moder.id}"
                                            width="75" class="img-fluid rounded"></a></td>
                                    <td class="clickableCell align-middle"><a
                                            href="${pageContext.request.contextPath}/account/${moder.id}"><h6><c:out
                                            value="${moder.lastName += ' ' += moder.firstName}"/></h6></a></td>
                                    <td class="align-middle text-right">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-outline-primary dropdown-toggle"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Опции
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item text-warning"
                                                   href="${pageContext.request.contextPath}/group/${requestScope.group.id}/changeMemberStatus/${moder.id}/user">Забрать
                                                    права модератора</a>
                                                <div class="dropdown-divider"></div>
                                                <c:choose>
                                                    <c:when test="${sessionAccount.id == moder.id}">
                                                        <a class="dropdown-item text-danger"
                                                           href="${pageContext.request.contextPath}/unsubscribe/group/${requestScope.group.id}">Отписаться</a>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <a class="dropdown-item text-danger"
                                                           href="${pageContext.request.contextPath}/group/${requestScope.group.id}/deleteGroupMember/${moder.id}">Исключить</a>
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </c:forEach>
                            <c:forEach var="user" items="${requestScope.users}">
                                <tr>
                                    <td><a href="${pageContext.request.contextPath}/account/${user.id}"><img
                                            src="${pageContext.request.contextPath}/image/account/${user.id}"
                                            width="75" class="img-fluid rounded"></a></td>
                                    <td class="clickableCell align-middle"><a
                                            href="${pageContext.request.contextPath}/account/${user.id}"><h6><c:out
                                            value="${user.lastName += ' ' += user.firstName}"/></h6></a></td>
                                    <td class="align-middle text-right">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-outline-primary dropdown-toggle"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Опции
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item"
                                                   href="${pageContext.request.contextPath}/group/${requestScope.group.id}/changeMemberStatus/${user.id}/moder">Сделать
                                                    модератором</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item text-danger"
                                                   href="${pageContext.request.contextPath}/group/${requestScope.group.id}/deleteGroupMember/${user.id}">Исключить</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="nav-profile" aria-labelledby="nav-profile-tab">
                    <div class="table-responsive pt-3">
                        <table class="clickableTable table table-hover mb-0">
                            <c:forEach var="pending" items="${requestScope.pendingUsers}">
                                <tr>
                                    <td><a href="${pageContext.request.contextPath}/account/${pending.id}"><img
                                            src="${pageContext.request.contextPath}/image/account/${pending.id}"
                                            width="75" class="img-fluid rounded"></a></td>
                                    <td class="clickableCell align-middle"><a
                                            href="${pageContext.request.contextPath}/account/${pending.id}"><h6><c:out
                                            value="${pending.lastName += ' ' += pending.firstName}"/></h6></a></td>
                                    <td class="align-middle text-right">
                                        <a class="btn btn-success"
                                           href="${pageContext.request.contextPath}/group/${requestScope.group.id}/changeMemberStatus/${pending.id}/user">Принять
                                            заявку</a>
                                        <a class="btn btn-outline-danger"
                                           href="${pageContext.request.contextPath}/group/${requestScope.group.id}/deleteGroupMember/${pending.id}">Отклонить
                                            заявку</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>