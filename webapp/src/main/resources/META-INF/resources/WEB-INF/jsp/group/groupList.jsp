<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Список групп</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white border rounded p-3 mb-3">
            <div class="text-center">
                <h2 class="pb-3">Список групп</h2>
            </div>

            <a class="btn btn-block btn-primary mb-3" href="${pageContext.request.contextPath}/createGroup">Создать
                группу</a>

            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                       aria-controls="nav-home" aria-selected="true">Группы</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                       aria-controls="nav-profile" aria-selected="false">Заявки</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">

                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="table-responsive">
                        <table class="clickableTable table table-hover mb-0">
                            <c:forEach var="group" items="${requestScope.memberGroups}">
                                <tr>
                                    <td><a href="${pageContext.request.contextPath}/group/${group.id}"><img
                                            src="${pageContext.request.contextPath}/image/group/${group.id}"
                                            width="75" class="img-fluid rounded"></a></td>
                                    <td class="clickableCell align-middle"><a
                                            href="${pageContext.request.contextPath}/group/${group.id}"><h6><c:out
                                            value="${group.name}"/></h6></a></td>
                                    <td class="align-middle text-right"><a class="btn btn-outline-danger"
                                                                           href="${pageContext.request.contextPath}/unsubscribe/group/${group.id}">Отписаться</a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="table-responsive pt-3">
                        <table class="table mb-0">
                            <c:forEach var="group" items="${requestScope.pendingGroups}">
                                <tr>
                                    <td><a href="${pageContext.request.contextPath}/group/${group.id}"><img
                                            src="${pageContext.request.contextPath}/image/group/${group.id}"
                                            width="75" class="img-fluid rounded"></a></td>
                                    <td class="align-middle"><a
                                            href="${pageContext.request.contextPath}/group/${group.id}"><h6><c:out
                                            value="${group.name}"/></h6></a></td>
                                    <td class="align-middle text-right"><a class="btn btn-outline-danger"
                                                                           href="${pageContext.request.contextPath}/unsubscribe/group/${group.id}">Отменить
                                        заявку</a></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>