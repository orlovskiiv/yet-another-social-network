<%@ page contentType="text/html;charset=utf-8" %>

<script src="${pageContext.request.contextPath}/webjars/font-awesome/5.3.1/js/all.min.js"></script>

<div class="navbar navbar-dark bg-primary">
    <a class="navbar-brand m-0 display-1" href="${pageContext.request.contextPath}/"><i
            class="fab fa-black-tie"></i> Социальная сеть</a>
</div>