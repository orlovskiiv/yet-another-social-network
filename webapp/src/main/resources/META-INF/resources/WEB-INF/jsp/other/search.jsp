<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Поиск</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white border rounded p-3 mb-3">
            <div id="pageSize" hidden>${pageSize}</div>
            <div class="text-center">
                <h2 class="pb-3">Поиск</h2>
            </div>

            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                       aria-controls="nav-home" aria-selected="true">Люди</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                       aria-controls="nav-profile" aria-selected="false">Группы</a>
                </div>
            </nav>

            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">


                    <nav aria-label="Page navigation example" class="pt-2">
                        <ul class="pagination">
                            <li id="prevAccountList" class="page-item disabled">
                                <a class="page-link" href="#" id="prevAccount" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li id="nextAccountList" class="page-item">
                                <a class="page-link" href="#" id="nextAccount" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>


                    <div class="table-responsive pt-2">
                        <table id="accountTargetTable" class="clickableTable table table-hover mb-0">
                            <c:forEach var="account" items="${requestScope.foundAccounts}">
                                <tr class="accounts">
                                    <td class="clickableCell"><a
                                            href="${pageContext.request.contextPath}/account/${account.id}"><img
                                            src="${pageContext.request.contextPath}/image/account/${account.id}"
                                            width="75" class="img-fluid rounded"></a></td>
                                    <td class="clickableCell align-middle"><a
                                            href="${pageContext.request.contextPath}/account/${account.id}"><h6><c:out
                                            value="${account.lastName += ' ' += account.firstName}"/></h6></a></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="table-responsive pt-3">


                        <nav aria-label="Page navigation example" class="pt-2">
                            <ul class="pagination">
                                <li id="prevGroupList" class="page-item disabled">
                                    <a class="page-link" href="#" id="prevGroup" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li id="nextGroupList" class="page-item">
                                    <a class="page-link" href="#" id="nextGroup" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>


                        <table id="groupTargetTable" class="clickableTable table table-hover mb-0">
                            <c:forEach var="group" items="${requestScope.foundGroups}">
                                <tr class="groups">
                                    <td class="clickableCell"><a
                                            href="${pageContext.request.contextPath}/group/${group.id}"><img
                                            src="${pageContext.request.contextPath}/image/group/${group.id}"
                                            width="75" class="img-fluid rounded"></a></td>
                                    <td class="clickableCell align-middle"><a
                                            href="${pageContext.request.contextPath}/group/${group.id}"><h6><c:out
                                            value="${group.name}"/></h6></a></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/paginatedAccountsSearch.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/paginatedGroupsSearch.js"></script>
</body>
</html>