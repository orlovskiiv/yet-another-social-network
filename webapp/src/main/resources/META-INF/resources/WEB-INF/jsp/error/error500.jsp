<%@ page contentType="text/html;charset=utf-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Социальная сеть</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${requestScope.group.id}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${requestScope.group.id}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/emptyHeader.jsp" %>

<div class="container mt-2">
    <div class="row">
        <div class="col-md-8 offset-md-2 bg-white p-3 border rounded">
            <div class="text-center">
                <h1 class="pb-2">При выполнении запроса произошла ошибка</h1>
                <h3><a href="${requestScope.group.id}/">На главную</a></h3>
            </div>
        </div>
    </div>
</div>

<%@ include file="../include/footer.jsp" %>
</body>
</html>