<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="function" uri="http://new-soc-net.com/functions" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Сообщения</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white border rounded p-3 mb-3">
            <div class="text-center">
                <h2 class="pb-3">Диалоги</h2>
            </div>

            <div class="table-responsive mt-2">
                <table class="clickableTable table table-hover mb-0">
                    <c:forEach var="entry" items="${requestScope.messagesWithChatAccount}">
                        <tr class="d-flex">
                            <td class="col-2"><a
                                    href="${pageContext.request.contextPath}/account/${entry.value.id}"><img
                                    src="${pageContext.request.contextPath}/image/account/${entry.value.id}"
                                    class="img-fluid rounded"></a></td>
                            <td class="clickableCell align-middle col-8">
                                    <%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>
                                <a href="${pageContext.request.contextPath}/account/${sessionAccount.id}/messages/${entry.value.id}">
                                    <h6
                                            class="font-weight-bold mb-0"><c:out
                                            value="${entry.value.firstName += ' ' += entry.value.lastName}"/></h6></a>
                                <p class="mb-0 mt-1"
                                   class="message">
                                    <c:out value="${entry.key.text}"/><br>
                                    <c:if test="${entry.key.photo != null}">[изображение]</c:if>
                                </p>
                            </td>
                            <td class="text-right col-2">
                                <p class="text-muted">${function:formatLocalDateTime(entry.key.dateOfCreation, 'dd.MM.yyyy')}</p>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>