<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Редактирование группы</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white p-3 border rounded">
            <div class="text-center">
                <h2 class="pb-5">Редактирование группы</h2>
                <spring:hasBindErrors name="group">
                    <div class="alert alert-danger" role="alert">
                        Ошибка валидации данных
                    </div>
                </spring:hasBindErrors>
            </div>

            <%--@elvariable id="group" type="com.getjavajob.training.web1801.orlovskiiv.dto.form.GroupDto"--%>
            <form:form method="POST" modelAttribute="group"
                       action="${pageContext.request.contextPath}/group/${group.id}/update"
                       enctype="multipart/form-data">
                <div class="form-group row pb-2">
                    <label for="groupName" class="col-md-3 col-form-label text-md-right">Название<sup
                            class="text-danger">*</sup></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="groupName" data-toggle="collapse"
                               data-target="#groupNameHelp" name="name"
                               value="<c:out value="${group.name}"/>">
                        <small class="form-text text-danger"><form:errors path="name"/></small>
                        <div class="collapse" id="groupNameHelp">
                            <small class="form-text text-muted">Максимальная длина 20 символов</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row pb-2">
                    <label for="photo"
                           class="col-md-3 col-form-label text-md-right">Изменить фото</label>
                    <div class="col-md-9 pl-0">
                        <input type="file" class="form-control-file" id="photo" data-toggle="collapse"
                               data-target="#photoHelp" accept="image/jpeg" name="photo">
                        <small class="form-text text-danger"><form:errors path="photo"/></small>
                        <div class="collapse" id="photoHelp">
                            <small class="form-text text-muted">Размер фото не более 500 кб</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row pb-2">
                    <label class="form-check-label col-md-3 text-md-right" for="deletePhoto">Удалить фото</label>
                    <div class="col-md-9 pl-3">
                        <input type="checkbox" class="form-check-input" id="deletePhoto" name="deletePhoto"
                               value="deletePhoto" <c:if test="${group.deletePhoto eq 'deletePhoto'}">checked</c:if>>
                    </div>
                </div>

                <div class="form-group row pb-2">
                    <label for="description" class="col-md-3 col-form-label text-md-right">Описание</label>
                    <div class="col-md-9 pl-0">
                        <textarea class="form-control" id="description" data-toggle="collapse"
                                  data-target="#descriptionHelp" rows="5"
                                  name="description"><c:out value="${group.description}"/></textarea>
                        <small class="form-text text-danger"><form:errors path="description"/></small>
                        <div class="collapse" id="descriptionHelp">
                            <small class="form-text text-muted">До 255 символов</small>
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary btn-block mt-2" value="Редактировать группу">
            </form:form>
        </div>

        <div class="col-md-8 offset-md-2 p-0">
            <div class="col-md-6 offset-md-3 p-3">
                <button type="button" class="btn btn-block btn-outline-danger btn-sm" data-toggle="modal"
                        data-target="#deleteModal">Удалить группу
                </button>

                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Подтвердите удаление</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Вы действительно хотите удалить группу?
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-danger btn-block"
                                   href="${pageContext.request.contextPath}/group/${group.id}/update/deleteGroup">Удалить</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>