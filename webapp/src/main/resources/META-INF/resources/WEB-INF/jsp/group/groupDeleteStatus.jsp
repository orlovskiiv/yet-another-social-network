<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Удаление группы</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white p-3 border rounded">
            <div class="text-center">
                <c:choose>
                    <c:when test="${requestScope.status}">
                        <h1 class="pb-2">Группа успешно удалена</h1>
                    </c:when>
                    <c:when test="${not requestScope.status}">
                        <h1 class="pb-2">Ошибка при удалении группы ID=${requestScope.id}</h1>
                    </c:when>
                </c:choose>
                <h3><a href="${pageContext.request.contextPath}/">На страницу аккаунта</a></h3>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>