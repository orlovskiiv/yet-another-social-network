<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Социальная сеть</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/emptyHeader.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="col-md-10 offset-1 p-3 mt-3 bg-white border rounded">
                <div class="text-center">
                    <a href="${pageContext.request.contextPath}/registration" id="registration"><p>Регистрация</p></a>
                    <c:if test="${param['error'] != null}">
                        <div class="alert alert-danger" role="alert">Неверный логин или пароль</div>
                    </c:if><c:if test="${param['logout'] != null}">
                    <div class="alert alert-primary" role="alert">Выход выполнен успешно</div>
                </c:if>
                </div>

                <form method="POST" action="${pageContext.request.contextPath}/login">
                    <div class="form-group">
                        <input type="email"
                               class="form-control <c:if test="${requestScope.failMessage eq 'invalidInput'}">is-invalid</c:if>"
                               id="email" aria-describedby="emailHelp" placeholder="Введите логин" name="username"
                               required>
                    </div>
                    <div class="form-group">
                        <input type="password"
                               class="form-control <c:if test="${requestScope.failMessage eq 'invalidInput'}">is-invalid</c:if>"
                               id="password" placeholder="Введите пароль" name="password" required>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing"
                               name="remember-me"
                               <c:if test="${requestScope.checkbox}">checked</c:if>>
                        <label class="custom-control-label" for="customControlAutosizing">Запомнить меня</label>
                    </div>
                    <input type="submit" class="btn btn-primary btn-block mt-2" value="Войти">
                </form>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="text-center">
        <p>
            <small class="text-muted">&copy; Все права защищены 2018</small>
        </p>
    </div>
</footer>
<%@ include file="../include/footer.jsp" %>
</html>