<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="function" uri="http://new-soc-net.com/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Диалог</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div id="chatOwnerFirstName" hidden>${chatOwnerFirstName}</div>
    <div id="chatOwnerLastName" hidden>${chatOwnerLastName}</div>
    <div id="interlocutorFirstName" hidden>${interlocutorFirstName}</div>
    <div id="interlocutorLastName" hidden>${interlocutorLastName}</div>
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white border rounded pb-0 pt-2 pr-2 pl-2 mb-3">
            <div class="pre-scrollable" id="scroll">
                <div class="table-responsive mt-2">
                    <table class="table mb-0">
                        <c:forEach var="entry" items="${requestScope.messagesWithAuthor}">
                            <tr class="d-flex">
                                <td class="col-2"><a
                                        href="${pageContext.request.contextPath}/account/${entry.value.id}"><img
                                        src="${pageContext.request.contextPath}/image/account/${entry.value.id}"
                                        class="img-fluid rounded"></a></td>
                                <td class="align-middle col-9">
                                    <a href="${pageContext.request.contextPath}/account/${entry.value.id}">
                                        <h6 class="font-weight-bold mb-0"><c:out
                                                value="${entry.value.firstName += ' ' += entry.value.lastName}"/></h6>
                                    </a>
                                    <small class="text-muted">${function:formatLocalDateTime(entry.key.dateOfCreation, 'dd.MM.yyyy в HH:mm')}
                                        <c:if test="${entry.key.edited}">(изменено)</c:if></small>
                                    <p class="mb-0 mt-1">
                                        <c:out value="${entry.key.text}"/>
                                    </p>
                                    <c:if test="${entry.key.photo != null}">
                                        <a target="_blank"
                                           href="${pageContext.request.contextPath}/image/privateMessage/${entry.key.id}"><img
                                                src="${pageContext.request.contextPath}/image/privateMessage/${entry.key.id}"
                                                class="img-fluid"></a>
                                    </c:if>
                                </td>
                                    <%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>
                                <td class="align-middle text-right col-1 pr-1">
                                    <c:choose>
                                        <c:when test="${sessionAccount.id eq entry.key.idAuthor}">
                                            <div class="btn-group">
                                                <button type="button"
                                                        class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item"
                                                       href="${pageContext.request.contextPath}/updatePrivateMessage/${entry.key.id}">Редактировать</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item text-danger"
                                                       href="${pageContext.request.contextPath}/deletePrivateMessage/${entry.key.id}">Удалить
                                                        сообщение</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item text-danger"
                                                       href="${pageContext.request.contextPath}/deletePrivateMessageFull/${entry.key.id}">Удалить
                                                        для всех</a>
                                                </div>
                                            </div>
                                        </c:when>
                                        <c:when test="${sessionAccount.id eq entry.key.idTarget}">
                                            <div class="btn-group">
                                                <button type="button"
                                                        class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item text-danger"
                                                       href="${pageContext.request.contextPath}/deletePrivateMessage/${entry.key.id}">Удалить
                                                        сообщение</a>
                                                </div>
                                            </div>
                                        </c:when>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                        <tr id="lastRow" class="d-flex"></tr>
                    </table>
                </div>
            </div>

            <div class="border-top">
                <%--@elvariable id="privateMessage" type="com.getjavajob.training.web1801.orlovskiiv.dto.form.PrivateMessageDto"--%>
                <div class="form-group row">
                    <div class="col-md-12">
                    <textarea
                            class="form-control" id="privateMessage" data-toggle="collapse"
                            data-target="#privateMessageHelp" rows="5" name="text"
                            placeholder="Введите сообщение"><c:out
                            value="${privateMessage.text}"/></textarea>
                        <small class="form-text text-danger"><form:errors path="text"/></small>
                        <small class="form-text text-danger"><form:errors/></small>
                        <div class="collapse" id="privateMessageHelp">
                            <small class="form-text text-muted">До 255 символов</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <a type="button" id="send" class="btn btn-primary">Отправить</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/lib/stomp.js"></script>
<script src="${pageContext.request.contextPath}/webjars/sockjs-client/1.1.2/sockjs.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/chat.js"></script>
</body>
</html>