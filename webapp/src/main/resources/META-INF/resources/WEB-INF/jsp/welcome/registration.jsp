<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Регистрация</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/emptyHeader.jsp" %>
<div class="container">
    <div class="row">
        <div class="col-md-8 offset-md-2 bg-white p-3 mt-3 border rounded mb-3">
            <div class="text-center">
                <h2 class="pb-5">Регистрация аккаунта</h2>
                <spring:hasBindErrors name="account">
                    <div class="alert alert-danger" role="alert">
                        Ошибка валидации данных
                    </div>
                </spring:hasBindErrors>
            </div>

            <%--@elvariable id="account" type="com.getjavajob.training.web1801.orlovskiiv.dto.form.AccountDto"--%>
            <form:form method="POST" modelAttribute="account" action="${pageContext.request.contextPath}/registration"
                       enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="firstName" id="firstNameLabel" class="col-md-3 col-form-label text-md-right">Имя<sup
                            class="text-danger">*</sup></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="firstName" data-toggle="collapse"
                               data-target="#firstNameHelp" name="firstName"
                               value="<c:out value="${account.firstName}" />">
                        <small class="form-text text-danger"><form:errors path="firstName"/></small>
                        <div class="collapse" id="firstNameHelp">
                            <small class="form-text text-muted">Максимальная длина 20 символов</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="lastName" id="lastNameLabel" class="col-md-3 col-form-label text-md-right">Фамилия<sup
                            class="text-danger">*</sup></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="lastName" data-toggle="collapse"
                               data-target="#lastNameHelp" name="lastName"
                               value="<c:out value="${account.lastName}"/>">
                        <small class="form-text text-danger"><form:errors path="lastName"/></small>
                        <div class="collapse" id="lastNameHelp">
                            <small class="form-text text-muted">Максимальная длина 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="middleName" id="middleNameLabel"
                           class="col-md-3 col-form-label text-md-right">Отчество</label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="middleName" data-toggle="collapse"
                               data-target="#middleNameHelp" name="middleName"
                               value="<c:out value="${account.middleName}"/>">
                        <small class="form-text text-danger"><form:errors path="middleName"/></small>
                        <div class="collapse" id="middleNameHelp">
                            <small class="form-text text-muted">Максимальная длина 20 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="dateOfBirth" id="dateOfBirthLabel" class="col-md-3 col-form-label text-md-right">Дата
                        рождения</label>
                    <div class="col-md-9 pl-0">
                        <input type="date" class="form-control" id="dateOfBirth" data-toggle="collapse"
                               data-target="#dateOfBirthHelp" name="dateOfBirth"
                               value="<c:out value="${account.dateOfBirth}"/>">
                        <small class="form-text text-danger"><form:errors path="dateOfBirth"/></small>
                        <div class="collapse.show" id="dateOfBirthHelp" style="display: none">
                            <small class="form-text text-danger">От 1.01.1900 до сегодняшнего дня</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row pb-5">
                    <label for="photo" id="photoLabel"
                           class="col-md-3 col-form-label text-md-right">Фото</label>
                    <div class="col-md-9 pl-0">
                        <input type="file" class="form-control-file" id="photo" data-toggle="collapse"
                               data-target="#photoHelp" accept="image/jpeg" name="photo">
                        <small class="form-text text-danger"><form:errors path="photo"/></small>
                        <div class="collapse" id="photoHelp">
                            <small class="form-text text-muted">Размер фото не более 500 кб</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" id="emailLabel"
                           class="col-md-3 col-form-label text-md-right">Эл. почта<sup
                            class="text-danger">*</sup></label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="email" data-toggle="collapse"
                               data-target="#emailHelp" name="email"
                               value="<c:out value="${account.email}"/>">
                        <small class="form-text text-danger"><form:errors path="email"/></small>
                        <div class="collapse" id="emailHelp">
                            <small class="form-text text-muted emailExample">example@test.com</small>
                            <small class="form-text text-danger duplicateEmail" style="display: none">Пользователь с
                                таким эл. адресом уже
                                существует
                            </small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" id="passwordLabel" class="col-md-3 col-form-label text-md-right">Пароль<sup
                            class="text-danger">*</sup></label>
                    <div class="col-md-9 pl-0">
                        <input type="password" class="form-control" id="password" data-toggle="collapse"
                               data-target="#passwordHelp"
                               name="password">
                        <small class="form-text text-danger"><form:errors path="password"/></small>
                        <small class="form-text text-danger"><form:errors/></small>
                        <div class="collapse" id="passwordHelp">
                            <small class="form-text text-muted passwordLength">от 6 до 20 символов</small>
                            <small class="form-text text-muted passwordMismatch" style="display: none">Несовпадение
                                паролей
                            </small>
                        </div>
                    </div>
                </div>
                <div class="form-group row pb-5">
                    <label for="passwordCopy" id="passwordCopyLabel" class="col-md-3 col-form-label text-md-right">Повторите
                        пароль<sup class="text-danger">*</sup></label>
                    <div class="col-md-9 pl-0">
                        <input type="password" class="form-control" id="passwordCopy" data-toggle="collapse"
                               data-target="#passwordCopyHelp"
                               name="passwordCopy">
                        <small class="form-text text-danger"><form:errors path="passwordCopy"/></small>
                    </div>
                </div>

                <div class="form-group row" id="inputPhone">
                    <label for="phone" id="phoneLabel" class="col-md-3 col-form-label text-md-right">Телефоны</label>
                    <div class="col-md-9 pl-0">
                        <div class="input-group">
                            <input type="text" class="form-control" id="phone" data-toggle="collapse"
                                   data-target="#phoneHelp">
                            <div class="input-group-append">
                                <select class="form-control" id="type" name="type" title="type">
                                    <option>Личный</option>
                                    <option>Рабочий</option>
                                </select>
                                <button class="btn btn-primary add" type="button">Добавить</button>
                            </div>
                        </div>
                        <div class="collapse" id="phoneHelp">
                            <small class="form-text text-muted">Длина номера 11 цифр</small>
                        </div>
                    </div>
                </div>

                <c:forEach var="phone" items="${account.personalPhones}" varStatus="count">
                    <div class="form-group row">
                        <label for="phone" class="col-md-3 col-form-label text-md-right"></label>
                        <div class="col-md-9 pl-0">
                            <div class="input-group">
                                <input type="text" readonly class="form-control personal"
                                       value="<c:out value="${phone.number}"/>"
                                       name="personalPhones[${count.index}].number" title="phone">
                                <div class="input-group-append">
                                    <span class="input-group-text">Личный</span>
                                    <button class="btn btn-outline-danger remove" type="button">Удалить</button>
                                </div>
                            </div>
                            <small class="form-text text-danger"><form:errors
                                    path="personalPhones[${count.index}].number"/></small>
                        </div>
                    </div>
                </c:forEach>
                <c:forEach var="phone" items="${account.workingPhones}" varStatus="count">
                    <div class="form-group row">
                        <label for="phone" class="col-md-3 col-form-label text-md-right"></label>
                        <div class="col-md-9 pl-0">
                            <div class="input-group">
                                <input type="text" readonly class="form-control working"
                                       value="<c:out value="${phone.number}"/>"
                                       name="workingPhones[${count.index}].number" title="phone">
                                <div class="input-group-append">
                                    <span class="input-group-text">Рабочий</span>
                                    <button class="btn btn-outline-danger remove" type="button">Удалить</button>
                                </div>
                            </div>
                            <small class="form-text text-danger"><form:errors
                                    path="workingPhones[${count.index}].number"/></small>
                        </div>
                    </div>
                </c:forEach>

                <div class="form-group row">
                    <label for="icq" id="icqLabel" class="col-md-3 col-form-label text-md-right">ICQ</label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="icq" data-toggle="collapse" data-target="#icqHelp"
                               name="icq"
                               value="<c:out value="${account.icq}"/>">
                        <small class="form-text text-danger"><form:errors path="icq"/></small>
                        <div class="collapse" id="icqHelp">
                            <small class="form-text text-muted">От 5 до 9 цифр</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="skype" id="skypeLabel" class="col-md-3 col-form-label text-md-right">Skype</label>
                    <div class="col-md-9 pl-0">
                        <input type="text" class="form-control" id="skype" data-toggle="collapse"
                               data-target="#skypeHelp" name="skype"
                               value="<c:out value="${account.skype}"/>">
                        <small class="form-text text-danger"><form:errors path="skype"/></small>
                        <div class="collapse" id="skypeHelp">
                            <small class="form-text text-muted">До 32 символов</small>
                        </div>
                    </div>
                </div>
                <div class="form-group row pb-5">
                    <label for="additionalInformation" id="additionalInformationLabel"
                           class="col-md-3 col-form-label text-md-right">Дополнительная информация</label>
                    <div class="col-md-9 pl-0">
                        <textarea class="form-control" id="additionalInformation" data-toggle="collapse"
                                  data-target="#additionalInformationoHelp" rows="5"
                                  name="additionalInformation"><c:out
                                value="${account.additionalInformation}"/></textarea>
                        <small class="form-text text-danger"><form:errors path="additionalInformation"/></small>
                        <div class="collapse" id="additionalInformationHelp">
                            <small class="form-text text-muted">До 255 символов</small>
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary btn-block mt-2" value="Зарегистрироваться">
            </form:form>
        </div>
    </div>
</div>

<footer>
    <div class="text-center">
        <p>
            <small class="text-muted">&copy; Все права защищены 2018</small>
        </p>
    </div>
</footer>
<%@ include file="../include/footer.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/validation.js"></script>
</body>
</html>