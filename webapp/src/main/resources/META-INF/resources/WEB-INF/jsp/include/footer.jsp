<%@ page contentType="text/html;charset=utf-8" %>

<link href="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet"
      type="text/css"/>
<script src="${pageContext.request.contextPath}/webjars/jquery/3.3.1/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-ui/1.12.1/jquery-ui.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/autocompleteSearch.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/support.js"></script>