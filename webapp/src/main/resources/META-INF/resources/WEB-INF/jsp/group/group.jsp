<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="function" uri="http://new-soc-net.com/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title><c:out value="${requestScope.group.name}"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-6 ml-1 mr-1 p-0 order-last">
            <div class="bg-white border rounded p-2 mb-3">
                <div class="text-center">
                    <h2 class="pb-3"><c:out value="${requestScope.group.name}"/></h2>
                </div>
                <c:choose>
                    <c:when test="${requestScope.group.deleted}">
                        <div class="text-center">
                            <h5 class="pb-3">Группа удалена</h5>
                        </div>
                    </c:when>
                    <c:when test="${requestScope.status == 'NONE' || requestScope.status == 'PENDING'}">
                        <div class="text-center">
                            <h5 class="pb-3">У вас нет прав для просмотра содержимого группы</h5>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <table class="table table-striped">
                            <tr>
                                <td>Создатель: <a
                                        href="${pageContext.request.contextPath}/account/${requestScope.group.creatorId}">${requestScope.creator.lastName += ' ' += requestScope.creator.firstName}</a>
                                </td>
                            </tr>
                            <c:if test="${not empty requestScope.group.description}">
                                <tr>
                                    <td>Описание: <c:out value="${requestScope.group.description}"/></td>
                                </tr>
                            </c:if>
                            <tr>
                                <td>Дата создания: <c:out value="${requestScope.group.dateOfCreation}"/></td>
                            </tr>
                        </table>


                        <h4 class="pt-5">Сообщения на стене</h4>
                        <%--@elvariable id="groupMessage" type="com.getjavajob.training.web1801.orlovskiiv.dto.form.GroupMessageDto"--%>
                        <form:form modelAttribute="groupMessage" method="POST"
                                   action="${pageContext.request.contextPath}/group/${requestScope.group.id}"
                                   enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <textarea
                                            class="form-control"
                                            id="wallMessage" data-toggle="collapse" data-target="#wallMessageHelp"
                                            rows="5" name="text" placeholder="Введите сообщение"><c:out
                                            value="${groupMessage.text}"/></textarea>
                                    <small class="form-text text-danger"><form:errors path="text"/></small>
                                    <small class="form-text text-danger"><form:errors/></small>
                                    <div class="collapse" id="wallMessageHelp">
                                        <small class="form-text text-muted">До 255 символов</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <input type="submit" class="btn btn-primary" value="Отправить">
                                </div>
                                <div class="form-group row col-md-8">
                                    <input type="file" class="form-control-file pt-1" id="photo" data-toggle="collapse"
                                           data-target="#photoHelp" accept="image/jpeg" name="photo">
                                    <small class="form-text text-danger"><form:errors path="photo"/></small>
                                    <div class="collapse" id="photoHelp">
                                        <small class="form-text text-muted">Размер фото не более 500 кб</small>
                                    </div>
                                </div>
                            </div>
                        </form:form>

                        <%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>
                        <div class="table-responsive mt-2">
                            <table class="table mb-0">
                                <c:forEach var="entry" items="${requestScope.messagesWithAuthor}">
                                    <tr class="d-flex">
                                        <td class="col-2 pl-0"><a
                                                href="${pageContext.request.contextPath}/account/${entry.key.idAuthor}"><img
                                                src="${pageContext.request.contextPath}/image/account/${entry.key.idAuthor}"
                                                class="img-fluid rounded"></a></td>
                                        <td class="align-middle col-9">
                                            <a href="${pageContext.request.contextPath}/account/${entry.key.idAuthor}">
                                                <h6 class="font-weight-bold mb-0"><c:out
                                                        value="${entry.value.firstName += ' ' += entry.value.lastName}"/></h6>
                                            </a>
                                            <small class="text-muted">${function:formatLocalDateTime(entry.key.dateOfCreation, 'dd.MM.yyyy в HH:mm')}
                                                <c:if test="${entry.key.edited}">(изменено)</c:if></small>
                                            <p class="mb-0 mt-1">
                                                <c:out value="${entry.key.text}"/>
                                            </p>
                                            <c:if test="${entry.key.photo != null}">
                                                <a target="_blank"
                                                   href="${pageContext.request.contextPath}/image/groupMessage/${entry.key.id}"><img
                                                        src="${pageContext.request.contextPath}/image/groupMessage/${entry.key.id}"
                                                        class="img-fluid"></a>
                                            </c:if>
                                        </td>

                                        <td class="align-middle text-right col-1 pr-1">
                                            <c:if test="${sessionAccount.id eq entry.key.idAuthor || requestScope.status == 'MODER' || sessionAccount.admin}">
                                                <div class="btn-group">
                                                    <button type="button"
                                                            class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item"
                                                           href="${pageContext.request.contextPath}/updateGroupMessage/${entry.key.id}">Редактировать</a>
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item text-danger"
                                                           href="${pageContext.request.contextPath}/deleteGroupMessage/${entry.key.id}">Удалить
                                                            сообщение</a>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>

        <div class="col-md-3 mr-1 ml-1 p-0 order-md-last">
            <div class="bg-white border rounded p-1">
                <div class="text-center">
                    <a target="_blank"
                       href="${pageContext.request.contextPath}/image/group/${requestScope.group.id}"><img
                            src="${pageContext.request.contextPath}/image/group/${requestScope.group.id}"
                            class="img-fluid"/></a>
                </div>


                <c:if test="${requestScope.group.deleted && requestScope.status == 'MODER'}">
                    <a class="btn btn-success btn-block mt-1"
                       href="${pageContext.request.contextPath}/group/${requestScope.group.id}/update/recoverGroup">Восстановить
                        группу</a>
                </c:if>
                <c:choose>
                    <c:when test="${requestScope.status == 'MODER'}">
                        <div class="text-center">
                            <a class="btn btn-link"
                               href="${pageContext.request.contextPath}/group/${requestScope.group.id}/members">Участники
                                группы</a>
                        </div>
                        <a class="btn btn-primary btn-block mt-1"
                           href="${pageContext.request.contextPath}/group/${requestScope.group.id}/update">Редактировать
                            группу</a>
                        <a class="btn btn-outline-danger btn-block mt-1"
                           href="${pageContext.request.contextPath}/unsubscribe/group/${requestScope.group.id}">Выйти из
                            группы</a>
                    </c:when>
                    <c:when test="${requestScope.status == 'USER'}">
                        <div class="text-center">
                            <a class="btn btn-link"
                               href="${pageContext.request.contextPath}/group/${requestScope.group.id}/members">Участники
                                группы</a>
                            <a class="btn btn-outline-danger btn-block mt-1"
                               href="${pageContext.request.contextPath}/unsubscribe/group/${requestScope.group.id}">Выйти
                                из
                                группы</a>
                        </div>
                    </c:when>
                    <c:when test="${requestScope.status == 'PENDING'}">
                        <a class="btn btn-outline-danger btn-block mt-1"
                           href="${pageContext.request.contextPath}/unsubscribe/group/${requestScope.group.id}">Отозвать
                            заявку</a>
                    </c:when>
                    <c:when test="${requestScope.status == 'NONE' && requestScope.group.deleted != true}">
                        <a class="btn btn-primary btn-block mt-1"
                           href="${pageContext.request.contextPath}/subscribe/group/${requestScope.group.id}">Подать
                            заявку</a>
                    </c:when>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>