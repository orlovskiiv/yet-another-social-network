<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Друзья</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white border rounded p-3 mb-3">
            <div class="text-center">
                <h2 class="pb-3">Список друзей</h2>
            </div>
            <nav>
                <div class="nav nav-tabs">
                    <a class="nav-item nav-link <c:if test="${requestScope.tab eq 'friends'}">active</c:if>"
                       id="nav-home-tab"
                       href="${pageContext.request.contextPath}/account/${requestScope.accountId}/friends"
                       role="tab" aria-controls="nav-home">Друзья</a>
                    <a class="nav-item nav-link <c:if test="${requestScope.tab eq 'friendRequests'}">active</c:if>"
                       id="nav-profile-tab"
                       href="${pageContext.request.contextPath}/account/${requestScope.accountId}/friendRequests"
                       role="tab" aria-controls="nav-profile">Заявки в друзья</a>
                    <a class="nav-item nav-link <c:if test="${requestScope.tab eq 'sentRequests'}">active</c:if>"
                       id="nav-sent"
                       href="${pageContext.request.contextPath}/account/${requestScope.accountId}/sentRequests"
                       role="tab" aria-controls="nav-profile">Отправленные заявки</a>
                    <a class="nav-item nav-link <c:if test="${requestScope.tab eq 'subscribers'}">active</c:if>"
                       id="subscribers"
                       href="${pageContext.request.contextPath}/account/${requestScope.accountId}/subscribers"
                       role="tab" aria-controls="nav-profile">Подписчики</a>
                </div>
            </nav>
            <div class="table-responsive pt-3">
                <table class="clickableTable table table-hover mb-0">
                    <c:forEach var="account" items="${requestScope.accounts}">
                    <tr>
                        <td><a href="${pageContext.request.contextPath}/account/${account.id}"><img
                                src="${pageContext.request.contextPath}/image/account/${account.id}"
                                width="75" class="img-fluid rounded"></a></td>
                        <td class="clickableCell align-middle"><a
                                href="${pageContext.request.contextPath}/account/${account.id}"><h6><c:out
                                value="${account.lastName += ' ' += account.firstName}"/></h6></a></td>
                        <td class="align-middle text-right">

                                <%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>
                            <c:choose>
                            <c:when test="${requestScope.tab eq 'friends'}">
                            <a class="btn btn-outline-danger"
                               href="${pageContext.request.contextPath}/account/${sessionAccount.id}/declineFriendRequest/${account.id}">Удалить
                                из друзей</a>
                            </c:when>
                            <c:when test="${requestScope.tab eq 'friendRequests'}">
                            <a class="btn btn-success"
                               href="${pageContext.request.contextPath}/account/${sessionAccount.id}/acceptFriendRequest/${account.id}">Принять
                                заявку</a>
                            <a class="btn btn-outline-danger"
                               href="${pageContext.request.contextPath}/account/${sessionAccount.id}/declineFriendRequest/${account.id}">Отклонить
                                заявку</a>
                            </c:when>
                            <c:when test="${requestScope.tab eq 'sentRequests'}">
                            <a class="btn btn-outline-danger"
                               href="${pageContext.request.contextPath}/account/${sessionAccount.id}/withdrawFriendRequest/${account.id}">Отменить
                                запрос</a>
                            </c:when>
                            <c:when test="${requestScope.tab eq 'subscriber'}">
                            <a class="btn btn-success"
                               href="${pageContext.request.contextPath}/account/${sessionAccount.id}/acceptFriendRequest/${account.id}>Принять
                                            заявку</a>
                                    </c:when>
                                </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>