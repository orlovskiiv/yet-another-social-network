<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Редактирование сообщения</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>

        <div class="col-md-8 bg-white border rounded p-3">
            <div class="text-center">
                <h2 class="pb-3">Редактирование сообщения</h2>
            </div>

            <%--@elvariable id="message" type="com.getjavajob.training.web1801.orlovskiiv.dto.form.MessageDto"--%>
            <form:form modelAttribute="message" method="POST"
                       action="${pageContext.request.contextPath}/${requestScope.target}/${requestScope.messageId}"
                       enctype="multipart/form-data">
                <div class="form-group row">
                    <div class="col-md-12">
                            <textarea
                                    class="form-control" id="wallMessage" data-toggle="collapse"
                                    data-target="#wallMessageHelp" rows="5" name="text"
                                    placeholder="Введите сообщение"><c:out
                                    value="${message.text}"/></textarea>
                        <small class="form-text text-danger"><form:errors path="text"/></small>
                        <small class="form-text text-danger"><form:errors/></small>
                        <div class="collapse" id="wallMessageHelp">
                            <small class="form-text text-muted">До 255 символов</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row pb-1">
                    <label class="form-check-label col-md-3 text-md-left" for="deletePhoto">Удалить фото</label>
                    <div class="col-md-9 pl-3">
                        <input type="checkbox" class="form-check-input text-md-left" id="deletePhoto" name="deletePhoto"
                               value="deletePhoto" <c:if test="${message.deletePhoto eq 'deletePhoto'}">checked</c:if>>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-3">
                        <input type="submit" class="btn btn-primary" value="Отправить">
                    </div>
                    <div class="form-group row col-md-8">
                        <input type="file" class="form-control-file pt-1" id="photo" data-toggle="collapse"
                               data-target="#photoHelp" accept="image/jpeg" name="photo">
                        <small class="form-text text-danger"><form:errors path="photo"/></small>
                        <div class="collapse" id="photoHelp">
                            <small class="form-text text-muted">Размер фото не более 500 кб</small>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>