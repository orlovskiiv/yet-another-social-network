<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="function" uri="http://new-soc-net.com/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title><c:out value="${requestScope.account.lastName += ' ' += requestScope.account.firstName}"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>

        <%--@elvariable id="account" type="com.getjavajob.training.web1801.orlovskiiv.account.Account"--%>
        <div class="col-md-3 mr-1 ml-1 p-0 ">
            <div class="bg-white border rounded p-1">
                <div class="text-center">
                    <a target="_blank"
                       href="${pageContext.request.contextPath}/image/account/${requestScope.account.id}"><img
                            src="${pageContext.request.contextPath}/image/account/${requestScope.account.id}"
                            class="img-fluid"/></a>
                </div>

                <%--@elvariable id="sessionAccount" type="com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount"--%>
                <c:choose>
                    <c:when test="${requestScope.account.deleted}">
                        <c:if test="${sessionAccount.admin}">
                            <a class="btn btn-success btn-block mt-1"
                               href="${pageContext.request.contextPath}/account/${requestScope.account.id}/update/recoverAccount">Восстановить
                                сраницу</a>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${sessionAccount.id eq requestScope.account.id || sessionAccount.admin}">
                            <a class="btn btn-primary btn-block mt-1"
                               href="${pageContext.request.contextPath}/account/${requestScope.account.id}/update">Редактировать
                                страницу</a>
                        </c:if>
                        <c:if test="${sessionAccount.id != requestScope.account.id}">
                            <a class="btn btn-primary btn-block mt-1"
                               href="${pageContext.request.contextPath}/account/${sessionAccount.id}/messages/${requestScope.account.id}">Написать
                                сообщение</a>
                            <c:choose>
                                <c:when test="${requestScope.status == 'FRIEND'}">
                                    <a class="btn btn-outline-danger btn-block mt-1"
                                       href="${pageContext.request.contextPath}/account/${sessionAccount.id}/declineFriendRequest/${requestScope.account.id}">Удалить
                                        из друзей</a>
                                </c:when>
                                <c:when test="${requestScope.status == 'FRIEND_REQUEST'}">
                                    <a class="btn btn-success btn-block mt-1"
                                       href="${pageContext.request.contextPath}/account/${sessionAccount.id}/acceptFriendRequest/${requestScope.account.id}">Принять
                                        заявку</a>
                                    <a class="btn btn-outline-danger btn-block mt-1"
                                       href="${pageContext.request.contextPath}/account/${sessionAccount.id}/declineFriendRequest/${requestScope.account.id}">Отклонить
                                        заявку</a>
                                </c:when>
                                <c:when test="${requestScope.status == 'SENT_REQUEST'}">
                                    <a class="btn btn-outline-danger btn-block mt-1"
                                       href="${pageContext.request.contextPath}/account/${sessionAccount.id}/withdrawFriendRequest/${requestScope.account.id}">Отменить
                                        запрос</a>
                                </c:when>
                                <c:when test="${requestScope.status == 'SUBSCRIBER'}">
                                    <a class="btn btn-success btn-block mt-1"
                                       href="${pageContext.request.contextPath}/account/${sessionAccount.id}/acceptFriendRequest/${requestScope.account.id}">Принять
                                        заявку</a>
                                </c:when>
                                <c:otherwise>
                                    <a class="btn btn-primary btn-block mt-1"
                                       href="${pageContext.request.contextPath}/account/${sessionAccount.id}/sendFriendRequest/${requestScope.account.id}">Добавить
                                        в друзья</a>
                                </c:otherwise>
                            </c:choose>
                        </c:if>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>

        <div class="col-md-6 ml-1 mr-1 p-0">
            <div class="bg-white border rounded p-2 mb-3">
                <div class="text-center">
                    <h2 class="pb-3"><c:out
                            value="${requestScope.account.lastName += ' ' += requestScope.account.firstName += ' ' += requestScope.account.middleName}"/></h2>
                </div>
                <c:choose>
                    <c:when test="${requestScope.account.deleted}">
                        <div class="text-center">
                            <h5 class="pb-3">Аккаунт удален</h5>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <table class="table table-striped">
                            <c:if test="${not empty requestScope.account.dateOfBirth}">
                                <tr>
                                    <td>Дата рождения: <c:out value="${requestScope.account.dateOfBirth}"/></td>
                                </tr>
                            </c:if>
                            <c:if test="${not empty requestScope.account.personalPhones}">
                                <tr>
                                    <td>Личные телефоны:
                                        <c:forEach var="phone"
                                                   items="${account.personalPhones}">
                                            <c:out value="${phone.number += ' '}"/>
                                        </c:forEach></td>
                                </tr>
                            </c:if>
                            <c:if test="${not empty requestScope.account.workingPhones}">
                                <tr>
                                    <td>Рабочие телефоны: <c:forEach var="phone"
                                                                     items="${account.workingPhones}">
                                        <c:out value="${phone.number += ' '}"/>
                                    </c:forEach></td>
                                </tr>
                            </c:if>
                            <c:if test="${not empty requestScope.account.homeAddress}">
                                <tr>
                                    <td>Домашний адрес:
                                        <c:forEach var="address"
                                                   items="${account.homeAddress}">
                                            <c:out value="${address += ' '}"/>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </c:if>
                            <c:if test="${not empty requestScope.account.workingAddress}">
                                <tr>
                                    <td>Рабочий адрес:
                                        <c:forEach var="address"
                                                   items="${account.workingAddress}">
                                            <c:out value="${address += ' '}"/>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </c:if>
                            <c:if test="${not empty requestScope.account.icq}">
                                <tr>
                                    <td>ICQ: <c:out value="${requestScope.account.icq}"/></td>
                                </tr>
                            </c:if>
                            <c:if test="${not empty requestScope.account.skype}">
                                <tr>
                                    <td>Skype: <c:out value="${requestScope.account.skype}"/></td>
                                </tr>
                            </c:if>
                            <c:if test="${not empty requestScope.account.additionalInformation}">
                                <tr>
                                    <td>Дополнительная информация: <c:out
                                            value="${requestScope.account.additionalInformation}"/></td>
                                </tr>
                            </c:if>
                            <tr>
                                <td>Дата регистрации: <c:out value="${requestScope.account.registration}"/></td>
                            </tr>
                        </table>

                        <%--@elvariable id="wallMessage" type="com.getjavajob.training.web1801.orlovskiiv.dto.form.WallMessageDto"--%>
                        <h4 class="pt-5">Сообщения на стене</h4>
                        <form:form modelAttribute="wallMessage" method="POST"
                                   action="${pageContext.request.contextPath}/account/${requestScope.account.id}"
                                   enctype="multipart/form-data">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <textarea
                                            class="form-control <c:if test="${requestScope.validation.text eq 'false' || requestScope.validation.emptyMessage eq 'false'}">is-invalid</c:if> <c:if test="${requestScope.validation.text eq 'true'}">is-valid</c:if>"
                                            id="wallMessage" data-toggle="collapse" data-target="#wallMessageHelp"
                                            rows="5" name="text" placeholder="Введите сообщение"><c:out
                                            value="${wallMessage.text}"/></textarea>
                                    <small class="form-text text-danger"><form:errors path="text"/></small>
                                    <small class="form-text text-danger"><form:errors/></small>
                                    <div class="collapse" id="wallMessageHelp">
                                        <small class="form-text text-muted">До 255 символов</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <input type="submit" class="btn btn-primary" value="Отправить">
                                </div>
                                <div class="form-group row col-md-8">
                                    <input type="file" class="form-control-file pt-1" id="photo" data-toggle="collapse"
                                           data-target="#photoHelp" accept="image/jpeg" name="photo">
                                    <small class="form-text text-danger"><form:errors path="photo"/></small>
                                    <div class="collapse" id="photoHelp">
                                        <small class="form-text text-muted">Размер фото не более 500 кб</small>
                                    </div>
                                </div>
                            </div>
                        </form:form>

                        <div class="table-responsive mt-2">
                            <table class="table mb-0">
                                <c:forEach var="entry" items="${requestScope.messagesWithAuthor}">

                                    <tr class="d-flex">
                                        <td class="col-2 pl-0"><a
                                                href="${pageContext.request.contextPath}/account/${entry.key.idAuthor}"><img
                                                src="${pageContext.request.contextPath}/image/account/${entry.key.idAuthor}"
                                                class="img-fluid rounded"></a></td>
                                        <td class="align-middle col-9">
                                            <a href="${pageContext.request.contextPath}/account/${entry.key.idAuthor}">
                                                <h6 class="font-weight-bold mb-0"><c:out
                                                        value="${entry.value.firstName += ' ' += entry.value.lastName}"/></h6>
                                            </a>
                                            <small class="text-muted">${function:formatLocalDateTime(entry.key.dateOfCreation, 'dd.MM.yyyy в HH:mm')}
                                                <c:if test="${entry.key.edited}">(изменено)</c:if></small>
                                            <p class="mb-0 mt-1">
                                                <c:out value="${entry.key.text}"/>
                                            </p>
                                            <c:if test="${entry.key.photo != null}">
                                                <a target="_blank"
                                                   href="${pageContext.request.contextPath}/image/wallMessage/${entry.key.id}"><img
                                                        src="${pageContext.request.contextPath}/image/wallMessage/${entry.key.id}"
                                                        class="img-fluid"></a>
                                            </c:if>
                                        </td>

                                        <td class="align-middle text-right col-1 pr-1">
                                            <c:choose>
                                                <c:when test="${sessionAccount.id eq entry.key.idAuthor || sessionAccount.admin}">
                                                    <div class="btn-group">
                                                        <button type="button"
                                                                class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item"
                                                               href="${pageContext.request.contextPath}/updateWallMessage/${entry.key.id}">Редактировать</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a class="dropdown-item text-danger"
                                                               href="${pageContext.request.contextPath}/deleteWallMessage/${entry.key.id}">Удалить
                                                                сообщение</a>
                                                        </div>
                                                    </div>
                                                </c:when>
                                                <c:when test="${sessionAccount.id eq entry.key.idTarget}">
                                                    <div class="btn-group">
                                                        <button type="button"
                                                                class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item text-danger"
                                                               href="${pageContext.request.contextPath}/deleteWallMessage/${entry.key.id}">Удалить
                                                                сообщение</a>
                                                        </div>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>