<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Создание группы</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/4.1.0/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
</head>
<body>
<%@ include file="../include/header.jsp" %>
<div class="container mt-2">
    <div class="row">
        <%@ include file="../include/menu.jsp" %>
        <div class="col-md-8 bg-white border rounded p-3">
            <div class="text-center">
                <h2 class="pb-5">Создание группы</h2>
                <spring:hasBindErrors name="group">
                    <div class="alert alert-danger" role="alert">
                        Ошибка валидации данных
                    </div>
                </spring:hasBindErrors>
            </div>

            <%--@elvariable id="group" type="com.getjavajob.training.web1801.orlovskiiv.dto.form.GroupDto"--%>
            <form:form method="POST" modelAttribute="group" action="${pageContext.request.contextPath}/createGroup"
                       enctype="multipart/form-data">
                <div class="form-group row pb-2">
                    <label for="groupName"
                           class="col-md-2 col-form-label text-md-right">Название<sup
                            class="text-danger">*</sup></label>
                    <div class="col-md-10 pl-0">
                        <input type="text" class="form-control" id="groupName" data-toggle="collapse"
                               data-target="#groupNameHelp" name="name"
                               value="<c:out value="${group.name}"/>">
                        <small class="form-text text-danger"><form:errors path="name"/></small>
                        <div class="collapse" id="groupNameHelp">
                            <small class="form-text text-muted">Максимальная длина 20 символов</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row pb-2">
                    <label for="photo"
                           class="col-md-2 col-form-label text-md-right">Фото</label>
                    <div class="col-md-10 pl-0">
                        <input type="file" class="form-control-file" id="photo" data-toggle="collapse"
                               data-target="#photoHelp" accept="image/jpeg" name="photo">
                        <small class="form-text text-danger"><form:errors path="photo"/></small>
                        <div class="collapse" id="photoHelp">
                            <small class="form-text text-muted">Размер фото не более 500 кб</small>
                        </div>
                    </div>
                </div>

                <div class="form-group row pb-2">
                    <label for="description" class="col-md-2 col-form-label text-md-right">Описание</label>
                    <div class="col-md-10 pl-0">
                        <textarea
                                class="form-control>" id="description" data-toggle="collapse"
                                data-target="#descriptionHelp" rows="5"
                                name="description"><c:out value="${group.description}"/></textarea>
                        <small class="form-text text-danger"><form:errors path="description"/></small>
                        <div class="collapse" id="descriptionHelp">
                            <small class="form-text text-muted">До 255 символов</small>
                        </div>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary btn-block mt-2" value="Создать группу">
            </form:form>
        </div>
    </div>
</div>
<%@ include file="../include/footer.jsp" %>
</body>
</html>