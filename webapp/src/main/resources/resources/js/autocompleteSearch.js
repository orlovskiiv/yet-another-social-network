$(function () {
    $("#headerSearch").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/search/autocomplete',
                data: {
                    value: request.term
                },
                success: function (data) {
                    response($.map(data, function (account) {
                        return {id: account.id, label: account.lastName + ' ' + account.firstName}
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            location.href = "/account/" + ui.item.id;
        }
    });
});