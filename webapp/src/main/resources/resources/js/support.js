$(document).click(function (e) {
    if (!$(e.target).is('a')) {
        $('.collapse').collapse('hide');
    }
});

$(".clickableTable tr .clickableCell").on("click", function () {
    window.location = $(this).children("a").attr("href");
});