$(function () {
    var pageSize = $("#pageSize").text();
    var totalAccounts;
    var pageNumber = 0;

    $.ajax({
        url: '/search/countAccounts',
        data: {
            value: getUrlParameter('value')
        },
        success: function (data) {
            totalAccounts = data;
            var remainingAccounts = data - pageSize * (pageNumber + 1);
            if (remainingAccounts <= 0) {
                $("#nextAccountList").addClass("disabled");
            }
        }
    });

    $("#prevAccount").click(function () {
        $.ajax({
            url: '/search/accounts/' + (--pageNumber),
            data: {
                value: getUrlParameter('value')
            },
            success: function (data) {
                if (pageNumber === 0) {
                    $("#prevAccountList").addClass("disabled");
                }
                $("#nextAccountList").removeClass("disabled");
                addAccountsToPage(data);
            }
        });
    });

    $("#nextAccount").click(function () {
        $.ajax({
            url: '/search/accounts/' + (++pageNumber),
            data: {
                value: getUrlParameter('value')
            },
            success: function (data) {
                var remainingAccounts = totalAccounts - pageSize * (pageNumber + 1);
                if (remainingAccounts <= 0) {
                    $("#nextAccountList").addClass("disabled");
                }
                $("#prevAccountList").removeClass("disabled");
                addAccountsToPage(data);
            }
        });
    });

    function addAccountsToPage(data) {
        $(".accounts").remove();
        var accounts = $.map(data, function (account) {
            return account
        });
        accounts.forEach(function (element) {
            $("#accountTargetTable").append(
                "<tr class='accounts'>\n" +
                "    <td class='clickableCell'><a\n" +
                "            href='/account/" + element.id + "'><img\n" +
                "            src='/image/account/" + element.id + "'\n" +
                "            width='75' class='img-fluid rounded'></a></td>\n" +
                "    <td class='clickableCell align-middle'><a\n" +
                "            href='/account/" + element.id + "'><h6>" +
                "            " + element.lastName + " " + element.firstName + "</h6></a></td>\n" +
                "</tr>");
        });
    }
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};