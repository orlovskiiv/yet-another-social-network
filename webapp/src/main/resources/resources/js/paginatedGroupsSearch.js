$(function () {
    var pageSize = $("#pageSize").text();
    var totalGroups;
    var pageNumber = 0;

    $.ajax({
        url: '/search/countGroups',
        data: {
            value: getUrlParameter('value')
        },
        success: function (data) {
            totalGroups = data;
            var remainingGroups = data - pageSize * (pageNumber + 1);
            if (remainingGroups <= 0) {
                $("#nextGroupList").addClass("disabled");
            }
        }
    });

    $("#prevGroup").click(function () {
        $.ajax({
            url: '/search/groups/' + (--pageNumber),
            data: {
                value: getUrlParameter('value')
            },
            success: function (data) {
                if (pageNumber === 0) {
                    $("#prevGroupList").addClass("disabled");
                }
                $("#nextGroupList").removeClass("disabled");
                addGroupsToPage(data);
            }
        });
    });

    $("#nextGroup").click(function () {
        $.ajax({
            url: '/search/groups/' + (++pageNumber),
            data: {
                value: getUrlParameter('value')
            },
            success: function (data) {
                var remainingGroups = totalGroups - pageSize * (pageNumber + 1);
                if (remainingGroups <= 0) {
                    $("#nextGroupList").addClass("disabled");
                }
                $("#prevGroupList").removeClass("disabled");
                addGroupsToPage(data);
            }
        });
    });

    function addGroupsToPage(data) {
        $(".groups").remove();
        var accounts = $.map(data, function (account) {
            return account
        });
        accounts.forEach(function (element) {
            $("#groupTargetTable").append(
                "<tr class='groups'>\n" +
                "    <td class='clickableCell'><a\n" +
                "            href='/group/" + element.id + "'><img\n" +
                "            src='/image/group/" + element.id + "'\n" +
                "            width='75' class='img-fluid rounded'></a></td>\n" +
                "    <td class='clickableCell align-middle'><a\n" +
                "            href='/group/" + element.id + "'><h6>\n" +
                "            " + element.name + "</h6></a></td>\n" +
                "</tr>");
        });
    }
});
