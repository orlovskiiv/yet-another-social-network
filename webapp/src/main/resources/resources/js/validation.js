$("#firstName").change(function () {
    validationRegex($("#firstName"), $("#firstNameLabel"), $("#firstNameHelp"), $("#firstNameHelp small"), /^.{1,20}$/);
});

$("#lastName").change(function () {
    validationRegex($("#lastName"), $("#lastNameLabel"), $("#lastNameHelp"), $("#lastNameHelp small"), /^.{1,20}$/);
});

$("#surName").change(function () {
    validationRegex($("#surName"), $("#surNameLabel"), $("#surNameHelp"), $("#surNameHelp small"), /^.{1,20}$/);
});

function validationRegex(input, label, help, helpSmall, regex) {
    var elementValue = input.val();

    if (elementValue === "") {
        validationNone(input, label, help, helpSmall);
    } else if (elementValue.match(regex)) {
        validationSuccess(input, label, help, helpSmall);
    } else {
        validationFail(input, label, help, helpSmall);
    }
}

function validationSuccess(input, label, help, helpSmall) {
    label.removeClass("text-danger").addClass("text-success");
    input.removeClass("is-invalid").addClass("is-valid");
    if (help !== undefined) {
        help.removeClass("collapse.show").addClass("collapse");
    }
    if (helpSmall !== undefined) {
        helpSmall.removeClass("text-muted text-danger").addClass("text-success");
    }
}

function validationFail(input, label, help, helpSmall) {
    label.removeClass("text-success").addClass("text-danger");
    input.removeClass("is-valid").addClass("is-invalid");
    if (help !== undefined) {
        help.removeClass("collapse").addClass("collapse.show");
    }
    if (helpSmall !== undefined) {
        helpSmall.removeClass("text-muted text-success").addClass("text-danger");
    }
}

function validationNone(input, label, help, helpSmall) {
    label.removeClass("text-success text-danger");
    input.removeClass("is-valid is-invalid");
    help.removeClass("collapse.show").addClass("collapse");
    helpSmall.removeClass("text-danger text-success").addClass("text-muted");
}

$(document).on("change", "#dateOfBirth", function () {
    var input = $("#dateOfBirth");
    var label = $("#dateOfBirthLabel");
    var help = $("#dateOfBirthHelp");
    var helpSmall = $("#dateOfBirthHelp small");

    var dateValue = input.val().split("-");
    var inputYear = parseInt(dateValue[0]);
    var inputMonth = parseInt(dateValue[1]);
    var inputDay = parseInt(dateValue[2]);

    var minYear = 1900;
    var minMonth = 1;
    var minDay = 1;

    var currentDate = new Date();
    var maxYear = currentDate.getFullYear();
    var maxMonth = currentDate.getMonth() + 1;
    var maxDay = currentDate.getDate();

    if (dateValue.length < 3) {
        validationNone(input, label, help, helpSmall);
        help.hide();
    } else if (inputYear >= minYear && inputYear < maxYear ||
        inputYear === maxYear && (inputMonth >= minMonth && inputMonth < maxMonth ||
            inputMonth === maxMonth && inputDay >= minDay && inputDay <= maxDay)) {
        validationSuccess(input, label, help, helpSmall);
        help.hide();
    } else {
        validationFail(input, label, help, helpSmall);
        help.show();
    }
});

$("#photo").change(function () {
    var input = $("#photo");
    var label = $("#photoLabel");
    var help = $("#photoHelp");
    var helpSmall = $("#photoHelp small");

    if (this.files[0] === undefined) {
        validationNone(input, label, help, helpSmall);
    } else if (this.files[0].size <= 512000) {
        validationSuccess(input, label, help, helpSmall);
    } else {
        validationFail(input, label, help, helpSmall);
        input.val("");
    }
});

$("#email").change(function () {
    $.post("registration/checkEmail",
        {
            email: $("#email").val()
        },
        function (data) {
            var input = $("#email");
            var label = $("#emailLabel");
            var help = $("#emailHelp");
            var helpSmall = $("#emailHelp small");

            if (data === "true") {
                $(".duplicateEmail").hide();
                $(".emailExample").show();
                validationNone(input, label, help, helpSmall);
            } else {
                $(".duplicateEmail").show();
                $(".emailExample").hide();
                validationFail(input, label, help, helpSmall);
            }
        });
});

$("#password, #passwordCopy").change(function () {
    var input = $("#password");
    var label = $("#passwordLabel");
    var help = $("#passwordHelp");
    var helpSmall = $("#passwordHelp small");

    var inputCopy = $("#passwordCopy");
    var labelCopy = $("#passwordCopyLabel");

    var inputValue = input.val();
    if (inputValue.match(/^.{6,20}$/) && passwordMatchingCheck()) {
        $(".passwordMismatch").hide();
        $(".passwordLength").show();
        validationSuccess(input, label, help, helpSmall);
        validationSuccess(inputCopy, labelCopy);
    } else if (!passwordMatchingCheck()) {
        $(".passwordMismatch").show();
        $(".passwordLength").hide();
        validationFail(input, label, help, helpSmall);
        validationFail(inputCopy, labelCopy);
    } else {
        $(".passwordMismatch").hide();
        $(".passwordLength").show();
        validationFail(input, label, help, helpSmall);
        validationFail(inputCopy, labelCopy);
    }
});

function passwordMatchingCheck() {
    var input = $("#password").val();
    var inputCopy = $("#passwordCopy").val();
    return input === inputCopy;
}

$(".add").click(function () {
    var input = $('#phone');
    var label = $("#phoneLabel");
    var help = $("#phoneHelp");
    var helpSmall = $("#phoneHelp small");

    var phoneValue = input.val();
    var typeValue = $('#type').val();


    if (phoneValue === "") {
        validationNone(input, label, help, helpSmall);
    } else if (phoneValue.match(/^(\d){11}$/)) {
        var inputPhone = $("<input type='text' readonly class='form-control'>");
        if (typeValue === "Личный") {
            inputPhone.attr("value", phoneValue).attr("name", "personalPhones[" + ($(".personal").length) + "].number").addClass('personal');
        } else if (typeValue === "Рабочий") {
            inputPhone.attr("value", phoneValue).attr("name", "workingPhones[" + ($(".working").length) + "].number").addClass('working');
        }
        var textType = $("<span class=\"input-group-text\"></span>")
            .text(typeValue);

        $("#inputPhone").after("<div class=\"form-group row\">\n" +
            "                    <label for=\"phone\" class=\"col-md-3 col-form-label text-md-right\"></label>\n" +
            "                    <div class=\"col-md-9 pl-0\">\n" +
            "                        <div class=\"input-group\">\n" +
            inputPhone[0].outerHTML +
            "                            <div class=\"input-group-append\">\n" +
            textType[0].outerHTML +
            "                                <button class='btn btn-outline-danger remove' type='button'>Удалить</button>\n" +
            "                            </div>\n" +
            "                        </div>\n" +
            "                    </div>\n" +
            "                </div>");

        input.val("");
        validationNone(input, label, help, helpSmall);
    } else {
        validationFail(input, label, help, helpSmall);
    }
});

$(document).on("click", ".remove", function () {
    $(this).parents("div.form-group.row").slideUp("normal", function () {
        $(this).remove();
        $('.personal').each(function (index) {
            $(this).attr("name", "personalPhones[" + index + "].number")
        });
        $('.working').each(function (index) {
            $(this).attr("name", "workingPhones[" + index + "].number")
        });
    });
});

$("#icq").change(function () {
    validationRegex($("#icq"), $("#icqLabel"), $("#icqHelp"), $("#icqHelp small"), /^.{5,9}\d$/);
});

$("#skype").change(function () {
    validationRegex($("#skype"), $("#skypeLabel"), $("#skypeHelp"), $("#skypeHelp small"), /^.{1,32}$/);
});

$("#additionalInformation").change(function () {
    validationRegex($("#additionalInformation"), $("#additionalInformationLabel"), $("#additionalInformationHelp"), $("#additionalInformationHelp small"), /^.{1,255}$/);
});

$("#oldPassword").change(function () {
    var attributes = decodeURIComponent(window.location.pathname.substring(1)).split("/");
    var id = attributes[1];

    $.post("/account/" + id + "/update/checkOldPassword",
        {
            oldPswd: $("#oldPassword").val()
        },
        function (data) {
            var input = $("#oldPassword");
            var label = $("#oldPasswordLabel");
            var help = $("#oldPasswordHelp");
            var helpSmall = $("#oldPasswordHelp small");

            if (data === "true") {
                help.hide();
                validationSuccess(input, label, help, helpSmall);
            } else {
                help.show();
                validationFail(input, label, help, helpSmall);
            }
        });
});

$("#homeCountry").change(function () {
    validationRegex($("#homeCountry"), $("#homeCountryLabel"), $("#homeCountryHelp"), $("#homeCountryHelp small"), /^.{1,20}$/);
});

$("#homeRegion").change(function () {
    validationRegex($("#homeRegion"), $("#homeRegionLabel"), $("#homeRegionHelp"), $("#homeRegionHelp small"), /^.{1,20}$/);
});

$("#homeCity").change(function () {
    validationRegex($("#homeCity"), $("#homeCityLabel"), $("#homeCityHelp"), $("#homeCityHelp small"), /^.{1,20}$/);
});

$("#homeStreet").change(function () {
    validationRegex($("#homeStreet"), $("#homeStreetLabel"), $("#homeStreetHelp"), $("#homeStreetHelp small"), /^.{1,20}$/);
});

$("#homeHouse").change(function () {
    validationRegex($("#homeHouse"), $("#homeHouseLabel"), $("#homeHouseHelp"), $("#homeHouseHelp small"), /^.{1,20}$/);
});

$("#workingCountry").change(function () {
    validationRegex($("#workingCountry"), $("#workingCountryLabel"), $("#workingCountryHelp"), $("#workingCountryHelp small"), /^.{1,20}$/);
});

$("#workingRegion").change(function () {
    validationRegex($("#workingRegion"), $("#workingRegionLabel"), $("#workingRegionHelp"), $("#workingRegionHelp small"), /^.{1,20}$/);
});

$("#workingCity").change(function () {
    validationRegex($("#workingCity"), $("#workingCityLabel"), $("#workingCityHelp"), $("#workingCityHelp small"), /^.{1,20}$/);
});

$("#workingStreet").change(function () {
    validationRegex($("#workingStreet"), $("#workingStreetLabel"), $("#workingStreetHelp"), $("#workingStreetHelp small"), /^.{1,20}$/);
});

$("#workingHouse").change(function () {
    validationRegex($("#workingHouse"), $("#workingHouseLabel"), $("#workingHouseHelp"), $("#workingHouseHelp small"), /^.{1,20}$/);
});