window.onload = function () {
    autoScroll();
};

function autoScroll() {
    var objDiv = document.getElementById("scroll");
    objDiv.scrollTop = objDiv.scrollHeight;
}

var stompClient = null;

$(function () {
    var attributes = decodeURIComponent(window.location.pathname.substring(1)).split("/");
    var idAuthor = attributes[1];
    var idTarget = attributes[3];

    function connect(idAuthor, idTarget) {
        disconnect();
        var socket = new SockJS('/room');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/topic' + getTopic(idAuthor, idTarget), function (message) {
                addMessage(JSON.parse(message.body));
                autoScroll();
            });
        });
    }

    function getTopic(idAuthor, idTarget) {
        if (idAuthor < idTarget) {
            return '/' + idAuthor + '/' + idTarget;
        } else {
            return '/' + idTarget + '/' + idAuthor;
        }
    }

    function disconnect() {
        if (stompClient != null) {
            stompClient.disconnect();
        }
        console.log("Disconnected");
    }

    function addMessage(message) {
        if (idAuthor === message.idAuthor) {
            appendMessageAuthor(message, $("#chatOwnerFirstName").text(), $("#chatOwnerLastName").text());
        } else {
            appendMessageTarget(message, $("#interlocutorFirstName").text(), $("#interlocutorLastName").text());
        }
    }

    function appendMessageAuthor(message, firstName, lastName) {
        $("#lastRow").before(
            "<tr class='d-flex'>\n" +
            "    <td class='col-2'><a\n" +
            "        href='/account/" + message.idAuthor + "'><img\n" +
            "        src='/image/account/" + message.idAuthor + "'\n" +
            "        class='img-fluid rounded'></a>" +
            "    </td>\n" +
            "    <td class='align-middle col-9'>\n" +
            "        <a href='/account/" + message.idAuthor + "'>\n" +
            "            <h6 class='font-weight-bold mb-0'>" + firstName + " " + lastName + "</h6>\n" +
            "        </a>\n" +
            "        <small class='text-muted'>" + message.dateOfCreation + "</small>\n" +
            "            <p class='mb-0 mt-1'>" + message.text + "</p>\n" +
            "    </td>" +

            "    <td class='align-middle text-right col-1 pr-1'>" +
            "        <div class='btn-group'>\n" +
            "            <button type='button'\n" +
            "                    class='btn btn-outline-secondary dropdown-toggle dropdown-toggle-split'\n" +
            "                    data-toggle='dropdown' aria-haspopup='true'\n" +
            "                    aria-expanded='false'>\n" +
            "            </button>\n" +
            "            <div class='dropdown-menu'>\n" +
            "                <a class='dropdown-item'\n" +
            "                   href='/updatePrivateMessage/" + message.id + "'>Редактировать</a>\n" +
            "                <div class='dropdown-divider'></div>\n" +
            "                <a class='dropdown-item text-danger'\n" +
            "                   href='/deletePrivateMessage/" + message.id + "'>Удалить сообщение</a>\n" +
            "                <div class='dropdown-divider'></div>\n" +
            "                <a class='dropdown-item text-danger'\n" +
            "                   href='/deletePrivateMessageFull/" + message.id + "'>Удалить для всех</a>\n" +
            "            </div>\n" +
            "        </div>" +
            "    </td>" +
            "</tr>");
    }

    function appendMessageTarget(message, firstName, lastName) {
        $("#lastRow").before(
            "<tr class='d-flex'>\n" +
            "    <td class='col-2'><a\n" +
            "        href='/account/" + message.idAuthor + "'><img\n" +
            "        src='/image/account/" + message.idAuthor + "'\n" +
            "        class='img-fluid rounded'></a>" +
            "    </td>\n" +
            "    <td class='align-middle col-9'>\n" +
            "        <a href='/account/" + message.idAuthor + "'>\n" +
            "            <h6 class='font-weight-bold mb-0'>" + firstName + " " + lastName + "</h6>\n" +
            "        </a>\n" +
            "        <small class='text-muted'>" + message.dateOfCreation + "</small>\n" +
            "            <p class='mb-0 mt-1'>" + message.text + "</p>\n" +
            "    </td>" +

            "    <td class='align-middle text-right col-1 pr-1'>" +
            "        <div class='btn-group'>\n" +
            "            <button type='button'\n" +
            "                    class='btn btn-outline-secondary dropdown-toggle dropdown-toggle-split'\n" +
            "                    data-toggle='dropdown' aria-haspopup='true'\n" +
            "                    aria-expanded='false'>\n" +
            "            </button>\n" +
            "            <div class='dropdown-menu'>\n" +
            "                <a class='dropdown-item text-danger'\n" +
            "                   href='/deletePrivateMessage/" + message.id + "'>Удалить сообщение</a>\n" +
            "            </div>\n" +
            "        </div>" +
            "    </td>" +
            "</tr>");
    }

    $("#send").click(function () {
        var privateMessage = $("#privateMessage");
        stompClient.send("/app/room" + getTopic(idAuthor, idTarget), {},
            JSON.stringify({'idAuthor': idAuthor, 'idTarget': idTarget, 'text': privateMessage.val()}));
        privateMessage.val('');
    });

    connect(idAuthor, idTarget);
});