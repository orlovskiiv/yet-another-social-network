package com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.support;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.MessageDto;
import com.getjavajob.training.web1801.orlovskiiv.message.Message;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class SupportMessageConverter {
    private static final String DELETE_PHOTO_ATTR = "deletePhoto";
    private static final Logger logger = LoggerFactory.getLogger(SupportMessageConverter.class);

    public <T extends Message, V extends MessageDto> T convertDtoToMessage(V dto, T message) {
        try {
            byte[] oldPhoto = message.getPhoto();
            BeanUtils.copyProperties(message, dto);
            boolean photoIsNull = dto.getPhoto() == null;
            String deletePhoto = dto.getDeletePhoto();
            boolean isDeletePhoto = deletePhoto != null && deletePhoto.equals(DELETE_PHOTO_ATTR);
            if (isDeletePhoto || photoIsNull) {
                message.setPhoto(getPhoto(oldPhoto, isDeletePhoto));
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn("BeanUtils exception ", e);
        }
        return message;
    }

    private byte[] getPhoto(byte[] oldPhoto, boolean isDeletePhoto) {
        return isDeletePhoto ? null : oldPhoto;
    }
}
