package com.getjavajob.training.web1801.orlovskiiv.dto.form.converter;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountRole;
import com.getjavajob.training.web1801.orlovskiiv.account.address.Address;
import com.getjavajob.training.web1801.orlovskiiv.account.phone.Phone;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.AccountDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.AddressDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.PhoneDto;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRole.ADMIN;
import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRole.USER;


@Component
public class AccountConverter implements Mapper<AccountDto, Account> {
    private static final String DELETE_PHOTO_ATTR = "deletePhoto";
    private static final String ADMIN_ATTR = "admin";
    private static final String ERROR_MESSAGE = "BeanUtils exception ";
    private static final Logger logger = LoggerFactory.getLogger(AccountConverter.class);

    private PasswordEncoder passwordEncoder;

    @Autowired
    public AccountConverter(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Account mapTargetToSource(AccountDto dto, Account account) {
        registerConvertersAccount();
        try {
            byte[] oldPhoto = account.getPhoto();
            String oldPassword = account.getPassword();
            BeanUtils.copyProperties(account, dto);

            boolean photoIsNull = dto.getPhoto() == null;
            String deletePhoto = dto.getDeletePhoto();

            boolean isDeletePhoto = deletePhoto != null && deletePhoto.equals(DELETE_PHOTO_ATTR);
            if (isDeletePhoto || photoIsNull) {
                account.setPhoto(getPhoto(oldPhoto, isDeletePhoto));
            }

            account.setPassword(getPassword(oldPassword, dto));
            account.setRole(getRole(dto));
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn(ERROR_MESSAGE, e);
        }


        return account;
    }

    private byte[] getPhoto(byte[] oldPhoto, boolean isDeletePhoto) {
        return isDeletePhoto ? null : oldPhoto;
    }

    private String getPassword(String oldPassword, AccountDto dto) {
        return dto.getPassword() == null ? oldPassword : passwordEncoder.encode(dto.getPassword());
    }

    private AccountRole getRole(AccountDto dto) {
        String admin = dto.getAdmin();
        return admin != null && admin.equals(ADMIN_ATTR) ? ADMIN : USER;
    }

    private void registerConvertersAccount() {
        ConvertUtils.register(new Converter() {
            @Override
            public <T> T convert(Class<T> aClass, Object o) {
                if (o instanceof List) {
                    Set<Phone> elements = convertPhoneDtoToPhone((List) o);
                    return (T) elements;
                } else if (o instanceof AddressDto) {
                    Set<Address> elements = convertAddressDtoToAddress(o);
                    return (T) elements;
                }
                return (T) new HashSet<>();
            }

            private Set<Phone> convertPhoneDtoToPhone(List o) {
                Set<Phone> elements = new HashSet<>();
                for (Object object : o) {
                    Phone phone = new Phone();
                    try {
                        BeanUtils.copyProperties(phone, object);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        logger.warn(ERROR_MESSAGE, e);
                    }
                    elements.add(phone);
                }
                return elements;
            }

            private Set<Address> convertAddressDtoToAddress(Object o) {
                Set<Address> elements = new HashSet<>();
                AddressDto dto = (AddressDto) o;
                if (!dto.isEmpty()) {
                    Address address = new Address();
                    try {
                        BeanUtils.copyProperties(address, o);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        logger.warn(ERROR_MESSAGE, e);
                    }
                    elements.add(address);
                }
                return elements;
            }
        }, Set.class);
    }

    @Override
    public AccountDto mapSourceToTarget(Account account) {
        registerConvertersDto();
        AccountDto dto = new AccountDto();
        try {
            BeanUtils.copyProperties(dto, account);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn(ERROR_MESSAGE, e);
        }

        if (account.getRole() == ADMIN) {
            dto.setAdmin(ADMIN_ATTR);
        }
        return dto;
    }

    private void registerConvertersDto() {
        ConvertUtils.register(new Converter() {
            @Override
            public <T> T convert(Class<T> aClass, Object o) {
                List<PhoneDto> elements = new ArrayList<>();
                if (o instanceof Set) {
                    for (Object object : (Set) o) {
                        PhoneDto phoneDto = new PhoneDto();
                        try {
                            BeanUtils.copyProperties(phoneDto, object);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            logger.warn(ERROR_MESSAGE, e);
                        }
                        elements.add(phoneDto);
                    }
                }
                return (T) elements;
            }
        }, List.class);
        registerAddressDtoConverter();
    }

    private void registerAddressDtoConverter() {
        ConvertUtils.register(new Converter() {
            @Override
            public <T> T convert(Class<T> aClass, Object o) {
                AddressDto addressDto = new AddressDto();
                if (o instanceof Set) {
                    for (Object object : (Set) o) {
                        try {
                            BeanUtils.copyProperties(addressDto, object);
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            logger.warn(ERROR_MESSAGE, e);
                        }
                    }
                }
                return (T) addressDto;
            }
        }, AddressDto.class);
    }
}
