package com.getjavajob.training.web1801.orlovskiiv.dto.form;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.constraints.OneFieldNotBlank;

import javax.validation.constraints.Size;
import java.util.Arrays;

@OneFieldNotBlank(fields = {"text", "photo"}, message = "Нельзя отправить пустое сообщение")
public class GroupMessageDto extends MessageDto {
    private static final String TEXT_LENGTH_MESSAGE = "Не более 255 символов";
    private static final String PHOTO_SIZE_MESSAGE = "Максимальный размер 500кб";

    @Size(max = 255, message = TEXT_LENGTH_MESSAGE)
    private String text;
    @Size(max = 512_000, message = PHOTO_SIZE_MESSAGE)
    private byte[] photo;
    private String deletePhoto;

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public byte[] getPhoto() {
        return photo;
    }

    @Override
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public String getDeletePhoto() {
        return deletePhoto;
    }

    @Override
    public void setDeletePhoto(String deletePhoto) {
        this.deletePhoto = deletePhoto;
    }

    @Override
    public String toString() {
        return "GroupMessageDto{" +
                "text='" + text + '\'' +
                ", photo=" + Arrays.toString(photo) +
                ", deletePhoto='" + deletePhoto + '\'' +
                '}';
    }
}
