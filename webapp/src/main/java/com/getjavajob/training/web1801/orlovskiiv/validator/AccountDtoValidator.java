package com.getjavajob.training.web1801.orlovskiiv.validator;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.AccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class AccountDtoValidator implements Validator {
    private static final String EMAIL_FIELD_NAME = "email";
    private static final String EMAIL_ERROR = "email.exist";
    private static final String EMAIL_MESSAGE = "Пользователь с таким эл. адресом уже существует";
    private static final String FIELD_NOT_EMPTY_MESSAGE = "Поле не должно быть пустым";
    private static final String INVALID_OLD_PSWD_MESSAGE = "Пароль не верен";
    private static final String PSWD_FIELD_NAME = "password";
    private static final String PSWD_COPY_FIELD_NAME = "passwordCopy";
    private static final String OLD_PSWD_FIELD_NAME = "oldPassword";
    private static final String PSWD_ERROR = "password.empty";
    private static final String OLD_PSWD_ERROR = "oldPassword.incorrect";

    private AccountService accountService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public AccountDtoValidator(AccountService accountService, PasswordEncoder passwordEncoder) {
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return AccountDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AccountDto dto = (AccountDto) target;
        if (dto.getId() == null) {
            checkingEmailForExistence(dto, errors);
        } else {
            Account oldAccount = accountService.getAccountById(dto.getId());
            if (!oldAccount.getEmail().equals(dto.getEmail())) {
                checkingEmailForExistence(dto, errors);
            }
            checkingPasswords(errors, dto, oldAccount);
        }
    }

    private void checkingEmailForExistence(AccountDto dto, Errors errors) {
        boolean emailExist = accountService.emailExists(dto.getEmail());
        if (emailExist) {
            errors.rejectValue(EMAIL_FIELD_NAME, EMAIL_ERROR, EMAIL_MESSAGE);
        }
    }

    private void checkingPasswords(Errors errors, AccountDto dto, Account oldAccount) {
        String dtoOldPassword = dto.getOldPassword();
        String dtoPassword = dto.getPassword();
        String dtoPasswordCopy = dto.getPasswordCopy();

        if (dtoOldPassword != null && dtoPassword == null && dtoPasswordCopy == null) {
            errors.rejectValue(PSWD_FIELD_NAME, PSWD_ERROR, FIELD_NOT_EMPTY_MESSAGE);
            errors.rejectValue(PSWD_COPY_FIELD_NAME, PSWD_ERROR, FIELD_NOT_EMPTY_MESSAGE);
        } else if (dtoOldPassword != null && !passwordEncoder.matches(dtoOldPassword, oldAccount.getPassword())) {
            errors.rejectValue(OLD_PSWD_FIELD_NAME, OLD_PSWD_ERROR, INVALID_OLD_PSWD_MESSAGE);
        } else if (dtoOldPassword == null && (dtoPassword != null || dtoPasswordCopy != null)) {
            errors.rejectValue(OLD_PSWD_FIELD_NAME, PSWD_ERROR, FIELD_NOT_EMPTY_MESSAGE);
        }
    }
}
