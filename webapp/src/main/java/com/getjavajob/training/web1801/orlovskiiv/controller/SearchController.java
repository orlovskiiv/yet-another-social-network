package com.getjavajob.training.web1801.orlovskiiv.controller;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.dto.ajax.AccountAjaxDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.ajax.GroupAjaxDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.ajax.converter.AccountAjaxConverter;
import com.getjavajob.training.web1801.orlovskiiv.dto.ajax.converter.GroupAjaxConverter;
import com.getjavajob.training.web1801.orlovskiiv.group.Group;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {
    private static final String FOUND_ACCOUNTS_ATTR = "foundAccounts";
    private static final String FOUND_GROUPS_ATTR = "foundGroups";
    private static final String MAIN_SEARCH_VIEW = "other/search";
    private static final String PAGE_SIZE_ATTR = "pageSize";
    private static final int PAGE_SIZE = 5;

    private AccountService accountService;
    private GroupService groupService;
    private AccountAjaxConverter accountAjaxConverter;
    private GroupAjaxConverter groupAjaxConverter;

    @Autowired
    public SearchController(AccountService accountService,
                            GroupService groupService,
                            AccountAjaxConverter accountAjaxConverter,
                            GroupAjaxConverter groupAjaxConverter) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.accountAjaxConverter = accountAjaxConverter;
        this.groupAjaxConverter = groupAjaxConverter;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String mainSearch(@RequestParam("value") String searchValue, Model model) {
        List<Account> foundAccounts = accountService.getAllFoundAccounts(searchValue, 0, PAGE_SIZE).getContent();
        model.addAttribute(FOUND_ACCOUNTS_ATTR, foundAccounts);
        List<Group> foundGroups = groupService.getFoundGroups(searchValue, 0, PAGE_SIZE).getContent();
        model.addAttribute(FOUND_GROUPS_ATTR, foundGroups);
        model.addAttribute(PAGE_SIZE_ATTR, PAGE_SIZE);
        return MAIN_SEARCH_VIEW;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/autocomplete")
    @ResponseBody
    public List<AccountAjaxDto> autocompleteSearch(@RequestParam("value") String searchValue) {
        Page<Account> foundAccounts = accountService.getAllFoundAccounts(searchValue, 0, PAGE_SIZE);
        List<AccountAjaxDto> dtos = new ArrayList<>();
        for (Account account : foundAccounts) {
            dtos.add(accountAjaxConverter.convertAccount(account));
        }
        return dtos;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{pageNumber}")
    @ResponseBody
    public List<AccountAjaxDto> getPaginatedAccounts(@PathVariable int pageNumber,
                                                     @RequestParam("value") String searchValue) {
        Page<Account> foundAccounts = accountService.getAllFoundAccounts(searchValue, pageNumber, PAGE_SIZE);
        List<AccountAjaxDto> dtos = new ArrayList<>();
        for (Account account : foundAccounts) {
            dtos.add(accountAjaxConverter.convertAccount(account));
        }
        return dtos;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/countAccounts")
    @ResponseBody
    public long getCountAccounts(@RequestParam("value") String searchValue) {
        return accountService.getAllFoundAccounts(searchValue, 0, 1).getTotalElements();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/groups/{pageNumber}")
    @ResponseBody
    public List<GroupAjaxDto> getPaginatedGroups(@PathVariable int pageNumber,
                                                 @RequestParam("value") String searchValue) {
        List<Group> foundGroups = groupService
                .getFoundGroups(searchValue, pageNumber, PAGE_SIZE).getContent();
        List<GroupAjaxDto> dtos = new ArrayList<>();
        for (Group group : foundGroups) {
            dtos.add(groupAjaxConverter.convertGroup(group));
        }
        return dtos;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/countGroups")
    @ResponseBody
    public long getCountGroups(@RequestParam("value") String searchValue) {
        return groupService.getFoundGroups(searchValue, 0, 1).getTotalElements();
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(false));
    }
}
