package com.getjavajob.training.web1801.orlovskiiv.controller;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.configuration.security.AfterLoginAuthenticationSuccessHandler;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.AccountDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.AccountConverter;
import com.getjavajob.training.web1801.orlovskiiv.dto.transfer.New;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import com.getjavajob.training.web1801.orlovskiiv.validator.AccountDtoValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;


@Controller
public class WelcomeController {
    private static final String TRUE_VALUE = "true";
    private static final String FALSE_VALUE = "false";
    private static final String ACCOUNT_MODEL_NAME = "account";
    private static final String REDIRECT_ACCOUNT = "redirect:/account/";
    private static final String REDIRECT_LOGIN = "redirect:/login";
    private static final String LOGIN_VIEW_NAME = "welcome/login";
    private static final String REGISTRATION_VIEW_NAME = "welcome/registration";
    private static final Logger logger = LoggerFactory.getLogger(WelcomeController.class);

    private AccountService accountService;
    private SessionAccount sessionAccount;
    private AccountConverter accountConverter;
    private AccountDtoValidator accountDtoValidator;
    private AfterLoginAuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    public WelcomeController(AccountService accountService,
                             SessionAccount sessionAccount,
                             AccountConverter accountConverter,
                             AccountDtoValidator accountDtoValidator,
                             AfterLoginAuthenticationSuccessHandler authenticationSuccessHandler) {
        this.accountService = accountService;
        this.sessionAccount = sessionAccount;
        this.accountConverter = accountConverter;
        this.accountDtoValidator = accountDtoValidator;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/", "/login"})
    public ModelAndView getLogin() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String view = !(auth instanceof AnonymousAuthenticationToken) ? REDIRECT_ACCOUNT + sessionAccount.getId()
                : LOGIN_VIEW_NAME;
        return new ModelAndView(view);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    public ModelAndView getRegistration() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return new ModelAndView(REDIRECT_ACCOUNT + sessionAccount.getId());
        } else {
            return new ModelAndView(REGISTRATION_VIEW_NAME, ACCOUNT_MODEL_NAME, new AccountDto());
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/registration")
    public ModelAndView postRegistration(@Validated(New.class) @ModelAttribute(ACCOUNT_MODEL_NAME) AccountDto account,
                                         BindingResult bindingResult, HttpServletRequest request) {
        accountDtoValidator.validate(account, bindingResult);
        if (bindingResult.getAllErrors().isEmpty()) {
            Account createdAccount =
                    accountService.createUpdateAccount(accountConverter.mapTargetToSource(account, new Account()));
            try {
                request.login(account.getEmail(), account.getPassword());
                authenticationSuccessHandler.setSessionAttributes(createdAccount);
                return new ModelAndView(REDIRECT_ACCOUNT + createdAccount.getId());
            } catch (ServletException e) {
                logger.error("Error login after registration: ", e);
                return new ModelAndView(REDIRECT_LOGIN);
            }
        } else {
            return new ModelAndView(REGISTRATION_VIEW_NAME, ACCOUNT_MODEL_NAME, account);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/registration/checkEmail")
    @ResponseBody
    public String checkEmail(@RequestParam String email) {
        return !accountService.emailExists(email) ? TRUE_VALUE : FALSE_VALUE;
    }
}
