package com.getjavajob.training.web1801.orlovskiiv.interceptor;

import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import com.getjavajob.training.web1801.orlovskiiv.support.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class AdminInterceptor extends HandlerInterceptorAdapter {
    private static final String PATH_VARIABLE_NAME = "accountId";

    @Autowired
    private SessionAccount sessionAccount;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        Map<String, String> pathVariables =
                (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        int accountForwardId = parseInt(pathVariables.get(PATH_VARIABLE_NAME));
        if (sessionAccount.getId() == accountForwardId || sessionAccount.isAdmin()) {
            return true;
        } else {
            throw new UnauthorizedException();
        }
    }
}
