package com.getjavajob.training.web1801.orlovskiiv.configuration.web;

import org.springframework.web.multipart.MultipartFile;

import java.beans.PropertyEditorSupport;
import java.io.IOException;

public class ByteArrayEditor extends PropertyEditorSupport {
    @Override
    public void setValue(Object value) {
        if (value instanceof MultipartFile) {
            MultipartFile multipartFile = (MultipartFile) value;
            try {
                super.setValue(getData(multipartFile.getBytes()));
            } catch (IOException ex) {
                throw new IllegalArgumentException("Cannot read contents of multipart file", ex);
            }
        }
    }

    private byte[] getData(byte[] data) {
        return data.length != 0 ? data : null;
    }
}
