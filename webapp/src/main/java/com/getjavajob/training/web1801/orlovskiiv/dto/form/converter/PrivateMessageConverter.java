package com.getjavajob.training.web1801.orlovskiiv.dto.form.converter;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.PrivateMessageDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.support.SupportMessageConverter;
import com.getjavajob.training.web1801.orlovskiiv.message.PrivateMessage;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class PrivateMessageConverter implements Mapper<PrivateMessageDto, PrivateMessage> {
    private static final String ERROR_MESSAGE = "BeanUtils exception ";
    private static final Logger logger = LoggerFactory.getLogger(PrivateMessageConverter.class);

    private SupportMessageConverter supportMessageConverter;

    @Autowired
    public PrivateMessageConverter(SupportMessageConverter supportMessageConverter) {
        this.supportMessageConverter = supportMessageConverter;
    }

    @Override
    public PrivateMessage mapTargetToSource(PrivateMessageDto dto, PrivateMessage privateMessage) {
        return supportMessageConverter.convertDtoToMessage(dto, privateMessage);
    }

    @Override
    public PrivateMessageDto mapSourceToTarget(PrivateMessage privateMessage) {
        PrivateMessageDto dto = new PrivateMessageDto();
        try {
            BeanUtils.copyProperties(dto, privateMessage);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn(ERROR_MESSAGE, e);
        }
        return dto;
    }
}
