package com.getjavajob.training.web1801.orlovskiiv.dto.form;

public abstract class MessageDto {
    private String text;
    private byte[] photo;
    private String deletePhoto;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getDeletePhoto() {
        return deletePhoto;
    }

    public void setDeletePhoto(String deletePhoto) {
        this.deletePhoto = deletePhoto;
    }
}
