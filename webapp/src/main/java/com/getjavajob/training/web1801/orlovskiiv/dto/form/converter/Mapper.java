package com.getjavajob.training.web1801.orlovskiiv.dto.form.converter;

public interface Mapper<S, T> {

    T mapTargetToSource(S source, T target);

    S mapSourceToTarget(T target);
}
