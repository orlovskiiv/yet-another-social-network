package com.getjavajob.training.web1801.orlovskiiv.dto.form;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Arrays;

public class GroupDto {
    private static final String NOT_BLANK_MESSAGE = "Поле не должно быть пустым";
    private static final String MAX_LENGTH_MESSAGE = "Не более 20 символов";
    private static final String DESCRIPTION_LENGTH_MESSAGE = "Не более 255 символов";
    private static final String PHOTO_SIZE_MESSAGE = "Максимальный размер 500кб";

    private Integer id;

    @NotBlank(message = NOT_BLANK_MESSAGE)
    @Size(max = 20, message = MAX_LENGTH_MESSAGE)
    private String name;
    @Size(max = 255, message = DESCRIPTION_LENGTH_MESSAGE)
    private String description;
    @Size(max = 512_000, message = PHOTO_SIZE_MESSAGE)
    private byte[] photo;
    private String deletePhoto;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getDeletePhoto() {
        return deletePhoto;
    }

    public void setDeletePhoto(String deletePhoto) {
        this.deletePhoto = deletePhoto;
    }

    @Override
    public String toString() {
        return "GroupDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", photo=" + Arrays.toString(photo) +
                ", deletePhoto='" + deletePhoto + '\'' +
                '}';
    }
}
