package com.getjavajob.training.web1801.orlovskiiv.controller;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.AccountDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.AccountConverter;
import com.getjavajob.training.web1801.orlovskiiv.validator.AccountDtoValidator;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class XmlController {
    private static final String ACCOUNT_MODEL = "account";
    private static final String ACCOUNT_UPDATE_VIEW_NAME = "account/accountUpdate";
    private static final String XML_ERROR_VIEW_NAME = "error/errorXml";
    private static final String REDIRECT_ACCOUNT = "redirect:/account/";
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private AccountService accountService;
    private AccountConverter accountConverter;
    private AccountDtoValidator accountDtoValidator;
    private Validator validator;

    @Autowired
    public XmlController(AccountService accountService,
                         AccountConverter accountConverter,
                         AccountDtoValidator accountDtoValidator,
                         Validator validator) {
        this.accountService = accountService;
        this.accountConverter = accountConverter;
        this.accountDtoValidator = accountDtoValidator;
        this.validator = validator;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/account/{accountId}/account",
            produces = "text/xml;charset=UTF-8")
    @ResponseBody
    public String getAccountXml(@PathVariable int accountId) {
        Account accountForUpdate = accountService.getFullAccountById(accountId);
        AccountDto dto = accountConverter.mapSourceToTarget(accountForUpdate);
        XStream xstream = new XStream();
        xstream.processAnnotations(AccountDto.class);
        return xstream.toXML(dto);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/account/{accountId}/xml")
    public ModelAndView postAccountXml(@PathVariable int accountId,
                                       @RequestParam byte[] xml, ModelAndView modelAndView) {
        XStream xstream = new XStream(new DomDriver());
        xstream.processAnnotations(AccountDto.class);

        if (xml == null) {
            modelAndView.setViewName(XML_ERROR_VIEW_NAME);
            return modelAndView;
        } else {
            AccountDto dto = (AccountDto) xstream.fromXML(new String(xml));
            dto.setId(accountId);
            DataBinder binder = new DataBinder(dto);
            binder.setValidator(validator);
            binder.validate();

            BindingResult bindingResult = binder.getBindingResult();
            accountDtoValidator.validate(dto, bindingResult);

            if (bindingResult.getAllErrors().isEmpty()) {
                Account oldAccount = accountService.getFullAccountById(accountId);
                Account updatedAccount = accountConverter.mapTargetToSource(dto, oldAccount);
                accountService.createUpdateAccount(updatedAccount);
                logger.info("Account updated {}", updatedAccount);
                modelAndView.setViewName(REDIRECT_ACCOUNT + accountId);
            } else {
                modelAndView.addObject(ACCOUNT_MODEL, dto);
                modelAndView.addObject(BindingResult.MODEL_KEY_PREFIX + ACCOUNT_MODEL, binder.getBindingResult());
                modelAndView.setViewName(ACCOUNT_UPDATE_VIEW_NAME);
            }
            return modelAndView;
        }
    }
}
