package com.getjavajob.training.web1801.orlovskiiv.controller.message;

import com.getjavajob.training.web1801.orlovskiiv.controller.support.GroupSupport;
import com.getjavajob.training.web1801.orlovskiiv.controller.support.MessageSupport;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.GroupMessageDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.GroupMessageConverter;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupService;
import com.getjavajob.training.web1801.orlovskiiv.message.GroupMessage;
import com.getjavajob.training.web1801.orlovskiiv.message.GroupMessageService;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import com.getjavajob.training.web1801.orlovskiiv.support.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

import static com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus.MODER;

@Controller
public class GroupMessageController {
    private static final String MESSAGE_MODEL = "message";
    private static final String GROUP_TARGET = "updateGroupMessage";
    private static final String REDIRECT_GROUP = "redirect:/group/";
    private static final String GROUP_MESSAGE_MODEL = "groupMessage";

    private GroupService groupService;
    private GroupMessageService groupMessageService;
    private SessionAccount sessionAccount;
    private GroupMessageConverter groupMessageConverter;
    private MessageSupport messageSupport;
    private GroupSupport groupSupport;

    @Autowired
    public GroupMessageController(GroupService groupService,
                                  GroupMessageService groupMessageService,
                                  SessionAccount sessionAccount,
                                  GroupMessageConverter groupMessageConverter,
                                  MessageSupport messageSupport,
                                  GroupSupport groupSupport) {
        this.groupService = groupService;
        this.groupMessageService = groupMessageService;
        this.sessionAccount = sessionAccount;
        this.groupMessageConverter = groupMessageConverter;
        this.messageSupport = messageSupport;
        this.groupSupport = groupSupport;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/group/{groupId}")
    public ModelAndView postGroupMessage(@PathVariable int groupId,
                                         @Valid @ModelAttribute(GROUP_MESSAGE_MODEL) GroupMessageDto groupMessage,
                                         BindingResult bindingResult, ModelAndView modelAndView) {
        if (bindingResult.getAllErrors().isEmpty()) {
            GroupMessage newMessage = groupMessageConverter.mapTargetToSource(groupMessage, new GroupMessage());
            newMessage.setIdAuthor(sessionAccount.getId());
            newMessage.setIdTarget(groupId);
            groupMessageService.createUpdateMessage(newMessage);
            modelAndView.setViewName(REDIRECT_GROUP + groupId);
        } else {
            modelAndView.addObject(GROUP_MESSAGE_MODEL, groupMessage);
            groupSupport.displayingGroupContent(groupId, modelAndView);
        }
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/deleteGroupMessage/{messageId}")
    public String deleteGroupMessage(@PathVariable int messageId) throws UnauthorizedException {
        GroupMessage message = groupMessageService.getMessageById(messageId);
        boolean isAdmin = sessionAccount.isAdmin();
        boolean isAuthor = message.getIdAuthor().equals(sessionAccount.getId());
        GroupMemberStatus sessionAccountStatus = groupService.getMemberStatus(sessionAccount.getId(),
                message.getIdTarget());
        boolean isModerator = sessionAccountStatus == MODER;

        if (isAuthor || isModerator || isAdmin) {
            groupMessageService.deleteMessageById(messageId);
            return REDIRECT_GROUP + message.getIdTarget();
        } else {
            throw new UnauthorizedException();
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/updateGroupMessage/{messageId}")
    public ModelAndView getUpdateGroupMessage(@PathVariable int messageId, ModelAndView modelAndView)
            throws UnauthorizedException {
        GroupMessage oldMessage = groupMessageService.getMessageById(messageId);
        boolean isAdmin = sessionAccount.isAdmin();
        boolean isAuthor = oldMessage.getIdAuthor().equals(sessionAccount.getId());
        GroupMemberStatus sessionAccountStatus = groupService.getMemberStatus(sessionAccount.getId(),
                oldMessage.getIdTarget());
        boolean isModerator = sessionAccountStatus == MODER;

        if (isAuthor || isModerator || isAdmin) {
            GroupMessageDto groupMessageDto = groupMessageConverter.mapSourceToTarget(oldMessage);
            messageSupport.setUpdateMessageAttributes(groupMessageDto, modelAndView, oldMessage, GROUP_TARGET);
            return modelAndView;
        } else {
            throw new UnauthorizedException();
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/updateGroupMessage/{messageId}")
    public ModelAndView postUpdateGroupMessage(@PathVariable int messageId,
                                               @Valid @ModelAttribute(MESSAGE_MODEL) GroupMessageDto messageDto,
                                               BindingResult bindingResult, ModelAndView modelAndView)
            throws UnauthorizedException {
        GroupMessage originalMessage = groupMessageService.getMessageById(messageId);
        boolean isAdmin = sessionAccount.isAdmin();
        boolean isAuthor = originalMessage.getIdAuthor().equals(sessionAccount.getId());
        GroupMemberStatus sessionAccountStatus = groupService.getMemberStatus(sessionAccount.getId(),
                originalMessage.getIdTarget());
        boolean isModerator = sessionAccountStatus == MODER;
        if (isAuthor || isModerator || isAdmin) {
            if (bindingResult.getAllErrors().isEmpty()) {
                GroupMessage updatedMessage = groupMessageConverter.mapTargetToSource(messageDto, originalMessage);
                groupMessageService.createUpdateMessage(updatedMessage);
                modelAndView.setViewName(REDIRECT_GROUP + originalMessage.getIdTarget());
            } else {
                messageSupport.setUpdateMessageAttributes(messageDto, modelAndView, originalMessage, GROUP_TARGET);
            }
        } else {
            throw new UnauthorizedException();
        }
        return modelAndView;
    }
}
