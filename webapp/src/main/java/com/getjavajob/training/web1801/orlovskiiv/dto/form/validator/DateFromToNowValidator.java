package com.getjavajob.training.web1801.orlovskiiv.dto.form.validator;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.constraints.DateFromToNow;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDate.now;
import static java.time.LocalDate.parse;

public class DateFromToNowValidator implements ConstraintValidator<DateFromToNow, LocalDate> {
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private LocalDate dateFrom;

    @Override
    public void initialize(DateFromToNow constraintAnnotation) {
        dateFrom = parse(constraintAnnotation.from(), DateTimeFormatter.ofPattern(DATE_FORMAT));
    }

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context) {
        LocalDate today = now();
        if (value != null) {
            return (value.isAfter(dateFrom) || value.isEqual(dateFrom)) &&
                    (value.isBefore(today) || value.isEqual(today));
        }
        return true;
    }
}
