package com.getjavajob.training.web1801.orlovskiiv.controller.support;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.MessageDto;
import com.getjavajob.training.web1801.orlovskiiv.message.Message;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

@Component
public class MessageSupport {
    private static final String UPDATE_MESSAGE_VIEW = "message/updateMessage";
    private static final String MESSAGE_MODEL = "message";
    private static final String TARGET_ATTR = "target";
    private static final String MESSAGE_ID_ATTR = "messageId";

    public void setUpdateMessageAttributes(MessageDto messageDto, ModelAndView modelAndView, Message originalMessage,
                                           String target) {
        modelAndView.addObject(MESSAGE_MODEL, messageDto);
        modelAndView.addObject(TARGET_ATTR, target);
        modelAndView.addObject(MESSAGE_ID_ATTR, originalMessage.getId());
        modelAndView.setViewName(UPDATE_MESSAGE_VIEW);
    }
}
