package com.getjavajob.training.web1801.orlovskiiv.controller.message;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.controller.support.MessageSupport;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.PrivateMessageDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.PrivateMessageConverter;
import com.getjavajob.training.web1801.orlovskiiv.dto.websocket.PrivateMessageJsonDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.websocket.converter.PrivateMessageJsonConverter;
import com.getjavajob.training.web1801.orlovskiiv.message.PrivateMessage;
import com.getjavajob.training.web1801.orlovskiiv.message.PrivateMessageService;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import com.getjavajob.training.web1801.orlovskiiv.support.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Controller
public class PrivateMessageController {
    private static final String MESSAGES_WITH_ACCOUNT_ATTR = "messagesWithChatAccount";
    private static final String MESSAGES_WITH_AUTHOR_ATTR = "messagesWithAuthor";
    private static final String CHAT_OWNER_FIRST_NAME = "chatOwnerFirstName";
    private static final String CHAT_OWNER_LAST_NAME = "chatOwnerLastName";
    private static final String INTERLOCUTOR_FIRST_NAME = "interlocutorFirstName";
    private static final String INTERLOCUTOR_LAST_NAME = "interlocutorLastName";
    private static final String ID_ATTR = "id";
    private static final String ID2_ATTR = "id2";
    private static final String DATE_PATTERN = "dd.MM.yyyy в HH:mm";
    private static final String PRIVATE_MESSAGES_VIEW_NAME = "message/privateMessages";
    private static final String CHAT_VIEW_NAME = "message/chat";
    private static final String MESSAGE_MODEL = "message";
    private static final String PRIVATE_TARGET = "updatePrivateMessage";
    private static final String REDIRECT_ACCOUNT = "redirect:/account/";
    private static final String REDIRECT_MESSAGES = "/messages/";
    private static final String REDIRECT_CHAT = "/messages/";

    private AccountService accountService;
    private PrivateMessageService privateMessageService;
    private SessionAccount sessionAccount;
    private PrivateMessageConverter privateMessageConverter;
    private PrivateMessageJsonConverter privateMessageJsonConverter;
    private MessageSupport messageSupport;

    @Autowired
    public PrivateMessageController(AccountService accountService,
                                    PrivateMessageService privateMessageService,
                                    SessionAccount sessionAccount,
                                    PrivateMessageConverter privateMessageConverter,
                                    PrivateMessageJsonConverter privateMessageJsonConverter,
                                    MessageSupport messageSupport) {
        this.accountService = accountService;
        this.privateMessageService = privateMessageService;
        this.sessionAccount = sessionAccount;
        this.privateMessageConverter = privateMessageConverter;
        this.privateMessageJsonConverter = privateMessageJsonConverter;
        this.messageSupport = messageSupport;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/account/{accountId}/messages")
    public String getPrivateMessages(@PathVariable int accountId, Model model) {
        Map<PrivateMessage, Account> messagesWithChatAccount = privateMessageService.getLastMessages(accountId);
        model.addAttribute(MESSAGES_WITH_ACCOUNT_ATTR, messagesWithChatAccount);
        return PRIVATE_MESSAGES_VIEW_NAME;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/account/{accountId}/messages/{interlocutorId}")
    public ModelAndView getChatMessages(@PathVariable int accountId,
                                        @PathVariable int interlocutorId,
                                        ModelAndView modelAndView) {
        displayingContent(accountId, interlocutorId, modelAndView);
        modelAndView.setViewName(CHAT_VIEW_NAME);
        return modelAndView;
    }

    private void displayingContent(int accountId, int interlocutorId, ModelAndView modelAndView) {
        modelAndView.addObject(ID_ATTR, accountId);
        modelAndView.addObject(ID2_ATTR, interlocutorId);
        Map<PrivateMessage, Account> messagesWithAuthor =
                privateMessageService.getChatMessages(accountId, interlocutorId);
        modelAndView.addObject(MESSAGES_WITH_AUTHOR_ATTR, messagesWithAuthor);
        Account chatOwner = accountService.getAccountById(accountId);
        modelAndView.addObject(CHAT_OWNER_FIRST_NAME, chatOwner.getFirstName());
        modelAndView.addObject(CHAT_OWNER_LAST_NAME, chatOwner.getLastName());
        Account interlocutor = accountService.getAccountById(interlocutorId);
        modelAndView.addObject(INTERLOCUTOR_FIRST_NAME, interlocutor.getFirstName());
        modelAndView.addObject(INTERLOCUTOR_LAST_NAME, interlocutor.getLastName());
    }

    @MessageMapping("/room/{idAuthor}/{idTarget}")
    @SendTo("/topic/{idAuthor}/{idTarget}")
    public PrivateMessageJsonDto send(PrivateMessageJsonDto dto) {
        PrivateMessage createdMessage = privateMessageService
                .createUpdateMessage(privateMessageJsonConverter.convertPrivateMessageJsonDto(dto));
        dto.setId(createdMessage.getId().toString());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        LocalDateTime dateTime = LocalDateTime.now();
        dto.setDateOfCreation(dateTime.format(formatter));
        return dto;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/deletePrivateMessage/{messageId}")
    public String deletePrivateMessage(@PathVariable int messageId) throws UnauthorizedException {
        PrivateMessage message = privateMessageService.getMessageById(messageId);
        boolean isAuthor = message.getIdAuthor().equals(sessionAccount.getId());
        boolean isTarget = message.getIdTarget().equals(sessionAccount.getId());
        if (isAuthor || isTarget) {
            return getRedirectAndDelete(message, isAuthor);
        } else {
            throw new UnauthorizedException();
        }
    }

    private String getRedirectAndDelete(PrivateMessage message, boolean isAuthor) {
        int redirectId = isAuthor ? message.getIdTarget() : message.getIdAuthor();
        privateMessageService.deleteMessageFromChat(message.getId(), isAuthor);
        return REDIRECT_ACCOUNT + sessionAccount.getId() + REDIRECT_CHAT + redirectId;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/deletePrivateMessageFull/{messageId}")
    public String deletePrivateMessageFull(@PathVariable int messageId) throws UnauthorizedException {
        PrivateMessage message = privateMessageService.getMessageById(messageId);
        boolean isAuthor = message.getIdAuthor().equals(sessionAccount.getId());
        if (isAuthor) {
            privateMessageService.deleteMessageById(messageId);
            return REDIRECT_ACCOUNT + sessionAccount.getId() + REDIRECT_CHAT + message.getIdTarget();
        } else {
            throw new UnauthorizedException();
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/updatePrivateMessage/{messageId}")
    public ModelAndView getUpdatePrivateMessage(@PathVariable int messageId, ModelAndView modelAndView)
            throws UnauthorizedException {
        PrivateMessage oldMessage = privateMessageService.getMessageById(messageId);
        boolean isAuthor = oldMessage.getIdAuthor().equals(sessionAccount.getId());
        if (isAuthor) {
            PrivateMessageDto privateMessageDto = privateMessageConverter.mapSourceToTarget(oldMessage);
            messageSupport.setUpdateMessageAttributes(privateMessageDto, modelAndView, oldMessage, PRIVATE_TARGET);
            return modelAndView;
        } else {
            throw new UnauthorizedException();
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/updatePrivateMessage/{messageId}")
    public ModelAndView postUpdatePrivateMessage(@PathVariable int messageId,
                                                 @Valid @ModelAttribute(MESSAGE_MODEL) PrivateMessageDto messageDto,
                                                 BindingResult bindingResult, ModelAndView modelAndView)
            throws UnauthorizedException {
        PrivateMessage originalMessage = privateMessageService.getMessageById(messageId);
        boolean isAuthor = originalMessage.getIdAuthor().equals(sessionAccount.getId());
        if (isAuthor) {
            if (bindingResult.getAllErrors().isEmpty()) {
                PrivateMessage updatedMessage = privateMessageConverter.mapTargetToSource(messageDto, originalMessage);
                privateMessageService.createUpdateMessage(updatedMessage);
                modelAndView.setViewName(
                        REDIRECT_ACCOUNT + sessionAccount.getId() + REDIRECT_MESSAGES + originalMessage.getIdTarget());
            } else {
                messageSupport.setUpdateMessageAttributes(messageDto, modelAndView, originalMessage, PRIVATE_TARGET);
            }
        } else {
            throw new UnauthorizedException();
        }
        return modelAndView;
    }
}
