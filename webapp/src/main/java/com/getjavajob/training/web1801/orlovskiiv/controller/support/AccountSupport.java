package com.getjavajob.training.web1801.orlovskiiv.controller.support;


import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountRelationshipStatus;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.message.WallMessage;
import com.getjavajob.training.web1801.orlovskiiv.message.WallMessageService;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Component
public class AccountSupport {
    private static final String STATUS_ATTR = "status";
    private static final String ACCOUNT_MODEL = "account";
    private static final String MESSAGES_WITH_AUTHOR_ATTR = "messagesWithAuthor";
    private static final String ACCOUNT_VIEW_NAME = "account/account";

    private AccountService accountService;
    private SessionAccount sessionAccount;
    private WallMessageService wallMessageService;

    @Autowired
    public AccountSupport(AccountService accountService,
                          SessionAccount sessionAccount,
                          WallMessageService wallMessageService) {
        this.accountService = accountService;
        this.sessionAccount = sessionAccount;
        this.wallMessageService = wallMessageService;
    }

    public void displayingAccountContent(int accountForwardId, ModelAndView modelAndView) {
        AccountRelationshipStatus status = accountService.getAccountStatus(sessionAccount.getId(), accountForwardId);
        modelAndView.addObject(STATUS_ATTR, status);
        Account account = accountService.getFullAccountById(accountForwardId);
        modelAndView.addObject(ACCOUNT_MODEL, account);
        Map<WallMessage, Account> messagesWithAuthor = wallMessageService.getUserMessagesWithAuthor(accountForwardId);
        modelAndView.addObject(MESSAGES_WITH_AUTHOR_ATTR, messagesWithAuthor);
        modelAndView.setViewName(ACCOUNT_VIEW_NAME);
    }
}
