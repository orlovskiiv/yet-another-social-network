package com.getjavajob.training.web1801.orlovskiiv.dto.form.constraints;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.validator.OneFieldNotBlankValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = OneFieldNotBlankValidator.class)
public @interface OneFieldNotBlank {
    String message() default "At least one field is not empty";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * Fields type: String, array
     *
     * @return array of field names
     */
    String[] fields();
}
