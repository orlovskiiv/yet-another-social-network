package com.getjavajob.training.web1801.orlovskiiv.controller.support;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.group.Group;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupService;
import com.getjavajob.training.web1801.orlovskiiv.message.GroupMessage;
import com.getjavajob.training.web1801.orlovskiiv.message.GroupMessageService;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Component
public class GroupSupport {
    private static final String STATUS_ATTR = "status";
    private static final String GROUP_ATTR = "group";
    private static final String CREATOR_ATTR = "creator";
    private static final String MESSAGES_WITH_AUTHOR_ATTR = "messagesWithAuthor";
    private static final String GROUP_VIEW_NAME = "group/group";

    private AccountService accountService;
    private GroupService groupService;
    private GroupMessageService groupMessageService;
    private SessionAccount sessionAccount;

    @Autowired
    public GroupSupport(AccountService accountService,
                        GroupService groupService,
                        GroupMessageService groupMessageService,
                        SessionAccount sessionAccount) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.groupMessageService = groupMessageService;
        this.sessionAccount = sessionAccount;
    }

    public void displayingGroupContent(int groupId, ModelAndView modelAndView) {
        Group group = groupService.getGroupById(groupId);
        Account creator = accountService.getAccountById(group.getCreatorId());
        int sessionId = sessionAccount.getId();
        GroupMemberStatus status = groupService.getMemberStatus(sessionId, group.getId());
        modelAndView.addObject(STATUS_ATTR, status);
        modelAndView.addObject(GROUP_ATTR, group);
        modelAndView.addObject(CREATOR_ATTR, creator);
        Map<GroupMessage, Account> messagesWithAuthor = groupMessageService.getGroupMessagesWithAuthor(groupId);
        modelAndView.addObject(MESSAGES_WITH_AUTHOR_ATTR, messagesWithAuthor);
        modelAndView.setViewName(GROUP_VIEW_NAME);
    }
}
