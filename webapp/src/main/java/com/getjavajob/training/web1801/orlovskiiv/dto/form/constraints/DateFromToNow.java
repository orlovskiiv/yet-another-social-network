package com.getjavajob.training.web1801.orlovskiiv.dto.form.constraints;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.validator.DateFromToNowValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = DateFromToNowValidator.class)
public @interface DateFromToNow {
    String message() default "Date does not match";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * String in format "yyyy-MM-dd"
     *
     * @return date from
     */
    String from();
}
