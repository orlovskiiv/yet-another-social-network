package com.getjavajob.training.web1801.orlovskiiv.configuration.security;

import com.getjavajob.training.web1801.orlovskiiv.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    private static final int TEN_YEARS_COOKIE_LIFETIME = 60 * 60 * 24 * 365 * 10;

    private UserDetailsServiceImpl userDetailsService;
    private AfterLoginAuthenticationSuccessHandler authenticationSuccessHandler;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public WebSecurityConfiguration(UserDetailsServiceImpl userDetailsService,
                                    AfterLoginAuthenticationSuccessHandler authenticationSuccessHandler,
                                    PasswordEncoder passwordEncoder) {
        this.userDetailsService = userDetailsService;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/registration", "/registration/**", "/resources/**", "/webjars/**")
                .permitAll()
                .anyRequest().authenticated()
                .and();
        http
                .formLogin()
                .loginPage("/login")
                .failureUrl("/login?error")
                .successHandler(authenticationSuccessHandler)
                .permitAll();
        http
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .and();
        http
                .rememberMe().key("remember-me").tokenValiditySeconds(TEN_YEARS_COOKIE_LIFETIME)
                .authenticationSuccessHandler(authenticationSuccessHandler);
    }
}
