package com.getjavajob.training.web1801.orlovskiiv.configuration.security;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRole.ADMIN;

@Component
public class AfterLoginAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private static final String REDIRECT_ACCOUNT = "/account/";

    private AccountService accountService;
    private SessionAccount sessionAccount;

    @Autowired
    public AfterLoginAuthenticationSuccessHandler(AccountService accountService, SessionAccount sessionAccount) {
        this.accountService = accountService;
        this.sessionAccount = sessionAccount;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException {
        User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Account account = accountService.getAccountByEmail(authUser.getUsername());
        setSessionAttributes(account);
        httpServletResponse.sendRedirect(REDIRECT_ACCOUNT + account.getId());
    }

    public void setSessionAttributes(Account account) {
        sessionAccount.setId(account.getId());
        sessionAccount.setFirstName(account.getFirstName());
        sessionAccount.setLastName(account.getLastName());
        sessionAccount.setAdmin(account.getRole() == ADMIN);
        sessionAccount.setDeleted(account.isDeleted());
    }
}
