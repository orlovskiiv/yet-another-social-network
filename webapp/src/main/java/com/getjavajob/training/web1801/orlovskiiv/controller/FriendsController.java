package com.getjavajob.training.web1801.orlovskiiv.controller;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountRelationshipStatus;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Set;

import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRelationshipStatus.*;

@Controller
@RequestMapping("/account/{accountId}")
public class FriendsController {
    private static final String ACCOUNT_ID_ATTR = "accountId";
    private static final String TAB_ATTR = "tab";
    private static final String TARGET_FRIENDS_VALUE = "friends";
    private static final String TARGET_FRIEND_REQUESTS_VALUE = "friendRequests";
    private static final String TARGET_SENT_REQUESTS_VALUE = "sentRequests";
    private static final String TARGET_SUBSCRIBERS_VALUE = "subscribers";
    private static final String ACCOUNTS_ATTR = "accounts";
    private static final String FRIENDS_VIEW_NAME = "account/friends";
    private static final String REDIRECT_ACCOUNT = "redirect:/account/";

    private AccountService accountService;
    private SessionAccount sessionAccount;

    @Autowired
    public FriendsController(AccountService accountService,
                             SessionAccount sessionAccount) {
        this.accountService = accountService;
        this.sessionAccount = sessionAccount;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/friends")
    public ModelAndView getFriends(@PathVariable int accountId, ModelAndView modelAndView) {
        Set<Account> friends = accountService.getFriends(accountId);
        setModelAttributes(accountId, modelAndView, friends, TARGET_FRIENDS_VALUE);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/friendRequests")
    public ModelAndView getFriendRequests(@PathVariable int accountId, ModelAndView modelAndView) {
        Set<Account> friendRequests = accountService.getFriendRequests(accountId);
        setModelAttributes(accountId, modelAndView, friendRequests, TARGET_FRIEND_REQUESTS_VALUE);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/sentRequests")
    public ModelAndView getSentRequests(@PathVariable int accountId, ModelAndView modelAndView) {
        Set<Account> sentRequests = accountService.getSentRequests(accountId);
        setModelAttributes(accountId, modelAndView, sentRequests, TARGET_SENT_REQUESTS_VALUE);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/subscribers")
    public ModelAndView getSubscribers(@PathVariable int accountId, ModelAndView modelAndView) {
        Set<Account> subscribers = accountService.getSubscribers(accountId);
        setModelAttributes(accountId, modelAndView, subscribers, TARGET_SUBSCRIBERS_VALUE);
        return modelAndView;
    }

    private void setModelAttributes(int accountId, ModelAndView modelAndView, Set<Account> accounts, String target) {
        modelAndView.addObject(ACCOUNT_ID_ATTR, accountId);
        modelAndView.addObject(TAB_ATTR, target);
        modelAndView.addObject(ACCOUNTS_ATTR, accounts);
        modelAndView.setViewName(FRIENDS_VIEW_NAME);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/acceptFriendRequest/{newFriendId}")
    public String acceptFriendRequest(@PathVariable int newFriendId) {
        int sessionId = sessionAccount.getId();
        AccountRelationshipStatus status = accountService.getAccountStatus(sessionId, newFriendId);
        if (status == FRIEND_REQUEST || status == SUBSCRIBER) {
            accountService.acceptFriendRequest(sessionId, newFriendId);
        }
        return REDIRECT_ACCOUNT + newFriendId;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/declineFriendRequest/{declinedId}")
    public String declineFriendRequest(@PathVariable int declinedId) {
        int sessionId = sessionAccount.getId();
        AccountRelationshipStatus status = accountService.getAccountStatus(sessionId, declinedId);
        if (status == FRIEND || status == FRIEND_REQUEST) {
            accountService.declineFriendRequest(sessionId, declinedId);
        }
        return REDIRECT_ACCOUNT + declinedId;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/sendFriendRequest/{sendId}")
    public String sendFriendRequest(@PathVariable int sendId) {
        int sessionId = sessionAccount.getId();
        AccountRelationshipStatus status = accountService.getAccountStatus(sessionId, sendId);
        if (status == NONE) {
            accountService.addFriend(sessionId, sendId);
        }
        return REDIRECT_ACCOUNT + sendId;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/withdrawFriendRequest/{withdrawId}")
    public String withdrawFriendRequest(@PathVariable int withdrawId) {
        int sessionId = sessionAccount.getId();
        AccountRelationshipStatus status = accountService.getAccountStatus(sessionId, withdrawId);
        if (status == SENT_REQUEST) {
            accountService.withdrawFriendRequest(sessionId, withdrawId);
        }
        return REDIRECT_ACCOUNT + withdrawId;

    }
}
