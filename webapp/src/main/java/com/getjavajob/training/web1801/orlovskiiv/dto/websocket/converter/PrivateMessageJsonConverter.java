package com.getjavajob.training.web1801.orlovskiiv.dto.websocket.converter;

import com.getjavajob.training.web1801.orlovskiiv.dto.websocket.PrivateMessageJsonDto;
import com.getjavajob.training.web1801.orlovskiiv.message.PrivateMessage;
import org.springframework.stereotype.Component;

import static java.lang.Integer.parseInt;

@Component
public class PrivateMessageJsonConverter {
    public PrivateMessage convertPrivateMessageJsonDto(PrivateMessageJsonDto dto) {
        PrivateMessage message = new PrivateMessage();
        message.setIdAuthor(parseInt(dto.getIdAuthor()));
        message.setIdTarget(parseInt(dto.getIdTarget()));
        message.setText(dto.getText());
        return message;
    }
}
