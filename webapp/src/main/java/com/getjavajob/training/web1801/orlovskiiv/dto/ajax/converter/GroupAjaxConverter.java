package com.getjavajob.training.web1801.orlovskiiv.dto.ajax.converter;

import com.getjavajob.training.web1801.orlovskiiv.dto.ajax.GroupAjaxDto;
import com.getjavajob.training.web1801.orlovskiiv.group.Group;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class GroupAjaxConverter {
    private static final Logger logger = LoggerFactory.getLogger(GroupAjaxConverter.class);

    public GroupAjaxDto convertGroup(Group group) {
        GroupAjaxDto dto = new GroupAjaxDto();
        try {
            BeanUtils.copyProperties(dto, group);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn("BeanUtils exception ", e);
        }
        return dto;
    }
}
