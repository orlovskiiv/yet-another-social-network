package com.getjavajob.training.web1801.orlovskiiv.interceptor;

import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupService;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import com.getjavajob.training.web1801.orlovskiiv.support.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus.MODER;
import static java.lang.Integer.parseInt;

public class GroupModeratorInterceptor extends HandlerInterceptorAdapter {
    private static final String PATH_VARIABLE_GROUP_ID = "groupId";

    @Autowired
    private GroupService groupService;
    @Autowired
    private SessionAccount sessionAccount;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        Map<String, String> pathVariables =
                (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        int groupId = parseInt(pathVariables.get(PATH_VARIABLE_GROUP_ID));
        int sessionId = sessionAccount.getId();
        GroupMemberStatus sessionAccountStatus = groupService.getMemberStatus(sessionId, groupId);
        if (sessionAccountStatus == MODER) {
            return true;
        } else {
            throw new UnauthorizedException();
        }
    }
}
