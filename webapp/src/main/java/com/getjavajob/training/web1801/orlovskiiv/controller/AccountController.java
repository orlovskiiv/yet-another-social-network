package com.getjavajob.training.web1801.orlovskiiv.controller;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.controller.support.AccountSupport;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.AccountDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.AccountConverter;
import com.getjavajob.training.web1801.orlovskiiv.dto.transfer.Exist;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import com.getjavajob.training.web1801.orlovskiiv.validator.AccountDtoValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class AccountController {
    private static final String STATUS_ATTR = "status";
    private static final String ACCOUNT_MODEL = "account";
    private static final String ID_ATTR = "id";
    private static final String TRUE_VALUE = "true";
    private static final String FALSE_VALUE = "false";
    private static final String ACCOUNT_DELETED_VIEW_NAME = "account/accountDeleteStatus";
    private static final String ACCOUNT_UPDATE_VIEW_NAME = "account/accountUpdate";
    private static final String REDIRECT_ACCOUNT = "redirect:/account/";
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private AccountService accountService;
    private SessionAccount sessionAccount;
    private AccountConverter accountConverter;
    private AccountDtoValidator accountDtoValidator;
    private PasswordEncoder passwordEncoder;
    private AccountSupport accountSupport;

    @Autowired
    public AccountController(AccountService accountService,
                             SessionAccount sessionAccount,
                             AccountConverter accountConverter,
                             AccountDtoValidator accountDtoValidator,
                             PasswordEncoder passwordEncoder,
                             AccountSupport accountSupport) {
        this.accountService = accountService;
        this.sessionAccount = sessionAccount;
        this.accountConverter = accountConverter;
        this.accountDtoValidator = accountDtoValidator;
        this.passwordEncoder = passwordEncoder;
        this.accountSupport = accountSupport;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/account/{accountId}")
    public ModelAndView getAccount(@PathVariable int accountId,
                                   ModelAndView modelAndView) {
        accountSupport.displayingAccountContent(accountId, modelAndView);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/account/{accountId}/update")
    public ModelAndView getUpdateAccount(@PathVariable int accountId,
                                         ModelAndView modelAndView) {
        Account accountForUpdate = accountService.getFullAccountById(accountId);
        AccountDto dto = accountConverter.mapSourceToTarget(accountForUpdate);
        modelAndView.addObject(ACCOUNT_MODEL, dto);
        modelAndView.setViewName(ACCOUNT_UPDATE_VIEW_NAME);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/account/{accountId}/update")
    public ModelAndView postUpdateAccount(@PathVariable int accountId,
                                          @Validated(Exist.class) @ModelAttribute(ACCOUNT_MODEL) AccountDto account,
                                          BindingResult bindingResult,
                                          ModelAndView modelAndView) {
        account.setId(accountId);
        accountDtoValidator.validate(account, bindingResult);
        if (bindingResult.getAllErrors().isEmpty()) {
            Account oldAccount = accountService.getFullAccountById(accountId);
            Account updatedAccount = accountConverter.mapTargetToSource(account, oldAccount);
            accountService.createUpdateAccount(updatedAccount);
            logger.info("Account updated {}", updatedAccount);
            modelAndView.setViewName(REDIRECT_ACCOUNT + accountId);
        } else {
            modelAndView.addObject(ACCOUNT_MODEL, account);
            modelAndView.setViewName(ACCOUNT_UPDATE_VIEW_NAME);
        }
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/account/{accountId}/update/deleteAccount")
    public String deleteAccount(@PathVariable int accountId, Model model) {
        boolean deleted = accountService.deleteAccountById(accountId);
        model.addAttribute(STATUS_ATTR, deleted);
        if (deleted) {
            sessionAccount.setDeleted(accountId == sessionAccount.getId());
        } else {
            model.addAttribute(ID_ATTR, accountId);
        }
        return ACCOUNT_DELETED_VIEW_NAME;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/account/{accountId}/update/recoverAccount")
    public String recoverAccount(@PathVariable int accountId) {
        accountService.recoverAccountById(accountId);
        if (accountId == sessionAccount.getId()) {
            sessionAccount.setDeleted(false);
        }
        return REDIRECT_ACCOUNT + accountId;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/account/{accountId}/update/checkOldPassword")
    @ResponseBody
    public String checkOldPassword(@PathVariable int accountId,
                                   @RequestParam("oldPswd") String oldPassword) {
        Account account = accountService.getAccountById(accountId);
        return passwordEncoder.matches(oldPassword, account.getPassword()) ? TRUE_VALUE : FALSE_VALUE;
    }
}
