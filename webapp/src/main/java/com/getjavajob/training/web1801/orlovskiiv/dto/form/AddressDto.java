package com.getjavajob.training.web1801.orlovskiiv.dto.form;

import com.getjavajob.training.web1801.orlovskiiv.account.address.AddressType;
import com.getjavajob.training.web1801.orlovskiiv.dto.transfer.Exist;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.validation.constraints.Size;

@XStreamAlias("address")
public class AddressDto {
    private static final String MAX_LENGTH_MESSAGE = "Не более 20 символов";

    @XStreamAlias("type")
    private AddressType type;
    @XStreamAlias("country")
    @Size(max = 20, message = MAX_LENGTH_MESSAGE, groups = Exist.class)
    private String country;
    @XStreamAlias("region")
    @Size(max = 20, message = MAX_LENGTH_MESSAGE, groups = Exist.class)
    private String region;
    @XStreamAlias("city")
    @Size(max = 20, message = MAX_LENGTH_MESSAGE, groups = Exist.class)
    private String city;
    @XStreamAlias("street")
    @Size(max = 20, message = MAX_LENGTH_MESSAGE, groups = Exist.class)
    private String street;
    @XStreamAlias("house")
    @Size(max = 20, message = MAX_LENGTH_MESSAGE, groups = Exist.class)
    private String house;

    public AddressDto() {
    }

    AddressDto(AddressType type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public AddressType getType() {
        return type;
    }

    public void setType(AddressType type) {
        this.type = type;
    }

    public boolean isEmpty() {
        return country == null && region == null && city == null && street == null && house == null;
    }

    @Override
    public String toString() {
        return "AddressDto{" +
                "type=" + type +
                ", country='" + country + '\'' +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", house='" + house + '\'' +
                '}';
    }
}
