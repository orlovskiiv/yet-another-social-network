package com.getjavajob.training.web1801.orlovskiiv.dto.form.converter;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.GroupDto;
import com.getjavajob.training.web1801.orlovskiiv.group.Group;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class GroupConverter implements Mapper<GroupDto, Group> {
    private static final String DELETE_PHOTO_ATTR = "deletePhoto";
    private static final String ERROR_MESSAGE = "BeanUtils exception ";
    private static final Logger logger = LoggerFactory.getLogger(GroupConverter.class);

    @Override
    public Group mapTargetToSource(GroupDto dto, Group group) {
        try {
            byte[] oldPhoto = group.getPhoto();
            BeanUtils.copyProperties(group, dto);
            boolean photoIsNull = dto.getPhoto() == null;
            String deletePhoto = dto.getDeletePhoto();
            boolean isDeletePhoto = deletePhoto != null && deletePhoto.equals(DELETE_PHOTO_ATTR);
            if (isDeletePhoto || photoIsNull) {
                group.setPhoto(getPhoto(oldPhoto, isDeletePhoto));
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn(ERROR_MESSAGE, e);
        }
        return group;
    }

    private byte[] getPhoto(byte[] oldPhoto, boolean isDeletePhoto) {

        return isDeletePhoto ? null : oldPhoto;
    }

    @Override
    public GroupDto mapSourceToTarget(Group group) {
        GroupDto dto = new GroupDto();
        try {
            BeanUtils.copyProperties(dto, group);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn(ERROR_MESSAGE, e);
        }
        return dto;
    }
}
