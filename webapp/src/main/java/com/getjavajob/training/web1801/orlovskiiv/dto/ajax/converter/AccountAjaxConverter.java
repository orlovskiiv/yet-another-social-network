package com.getjavajob.training.web1801.orlovskiiv.dto.ajax.converter;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.dto.ajax.AccountAjaxDto;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class AccountAjaxConverter {
    private static final Logger logger = LoggerFactory.getLogger(AccountAjaxConverter.class);

    public AccountAjaxDto convertAccount(Account account) {
        AccountAjaxDto dto = new AccountAjaxDto();
        try {
            BeanUtils.copyProperties(dto, account);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn("BeanUtils exception ", e);
        }
        return dto;
    }
}
