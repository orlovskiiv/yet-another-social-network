package com.getjavajob.training.web1801.orlovskiiv.dto.form.validator;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.AccountDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.constraints.FieldsMatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;

public class FieldsMatchValidator implements ConstraintValidator<FieldsMatch, AccountDto> {
    private static final Logger logger = LoggerFactory.getLogger(FieldsMatchValidator.class);

    private String firstFieldName;
    private String secondFieldName;

    @Override
    public void initialize(FieldsMatch constraintAnnotation) {
        firstFieldName = constraintAnnotation.first();
        secondFieldName = constraintAnnotation.second();
    }

    @Override
    public boolean isValid(AccountDto value, ConstraintValidatorContext context) {
        try {
            Field firstField = value.getClass().getDeclaredField(firstFieldName);
            firstField.setAccessible(true);
            if (!firstField.getType().isAssignableFrom(String.class)) {
                logger.warn("Field \" {} \" not String", firstFieldName);
                return false;
            }
            String firstValue = (String) firstField.get(value);

            Field secondField = value.getClass().getDeclaredField(secondFieldName);
            secondField.setAccessible(true);
            if (!secondField.getType().isAssignableFrom(String.class)) {
                logger.warn("Field \" {} \" not String", secondFieldName);
                return false;
            }
            String secondValue = (String) secondField.get(value);

            if (firstValue != null && secondValue != null) {
                return firstValue.equals(secondValue);
            } else return firstValue == null && secondValue == null;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            logger.warn("FieldsMatchValidator: ", e);
            return false;
        }
    }
}
