package com.getjavajob.training.web1801.orlovskiiv.dto.form.validator;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.MessageDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.constraints.OneFieldNotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;

public class OneFieldNotBlankValidator implements ConstraintValidator<OneFieldNotBlank, MessageDto> {
    private static final Logger logger = LoggerFactory.getLogger(OneFieldNotBlankValidator.class);

    private String[] fields;

    @Override
    public void initialize(OneFieldNotBlank constraintAnnotation) {
        fields = constraintAnnotation.fields();
    }

    @Override
    public boolean isValid(MessageDto value, ConstraintValidatorContext context) {
        boolean result = false;
        try {
            for (String fieldName : fields) {
                result = result || isEmptyField(value, fieldName);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            logger.warn("OneFieldNotBlankValidator: ", e);
        }
        return result;
    }

    private boolean isEmptyField(MessageDto value, String fieldName)
            throws NoSuchFieldException, IllegalAccessException {
        Field field = value.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        Object fieldValue = field.get(value);

        if (fieldValue == null) {
            return false;
        } else if (fieldValue instanceof String) {
            String sValue = (String) fieldValue;
            return !sValue.isEmpty();
        } else if (fieldValue instanceof byte[]) {
            byte[] byteValue = (byte[]) field.get(value);
            return byteValue.length != 0;
        }
        return true;
    }
}
