package com.getjavajob.training.web1801.orlovskiiv.configuration.web;

import com.getjavajob.training.web1801.orlovskiiv.interceptor.AdminInterceptor;
import com.getjavajob.training.web1801.orlovskiiv.interceptor.GroupModeratorInterceptor;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.List;

import static org.springframework.web.context.WebApplicationContext.SCOPE_SESSION;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.getjavajob.training.web1801.orlovskiiv")
public class WebAppConfiguration implements WebMvcConfigurer {
    @Value("${spring.mvc.view.prefix}")
    private String jspPrefix;
    @Value("${spring.mvc.view.suffix}")
    private String jspSuffix;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry resourceHandlerRegistry) {
        resourceHandlerRegistry.addResourceHandler("/resources/**")
                .addResourceLocations("classpath:/resources/");
        resourceHandlerRegistry.addResourceHandler("/webjars/**").addResourceLocations("/webjars/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(adminInterceptor()).addPathPatterns("/account/*/update", "/account/*/update/**",
                "/account/*/update/checkOldPassword", "/account/*/update/checkOldPassword/*", "/account/*/groups",
                "/account/*/groups/*", "/account/*/messages", "/account/*/messages/*", "/account/*/friends",
                "/account/*/friends/*", "/account/*/friendRequests", "/account/*/friendRequests/*",
                "/account/*/sentRequests", "/account/*/sentRequests/*", "/account/*/subscribers",
                "/account/*/subscribers/*");
        registry.addInterceptor(groupModeratorInterceptor()).addPathPatterns("/group/*/update", "/group/*/update/**",
                "/group/*/deleteGroupMember/*", "/group/*/changeMemberStatus/**");
    }

    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
        exceptionResolvers.add(new CustomHandlerExceptionResolver());
    }

    @Bean
    public ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setExposedContextBeanNames("sessionAccount");
        bean.setPrefix(jspPrefix);
        bean.setSuffix(jspSuffix);
        return bean;
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        return new CommonsMultipartResolver();
    }

    @Bean(name = "sessionAccount")
    @Scope(value = SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public SessionAccount sessionAccount() {
        return new SessionAccount();
    }

    @Bean
    public AdminInterceptor adminInterceptor() {
        return new AdminInterceptor();
    }

    @Bean
    public GroupModeratorInterceptor groupModeratorInterceptor() {
        return new GroupModeratorInterceptor();
    }

    @Bean(name = "validator")
    public LocalValidatorFactoryBean localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
