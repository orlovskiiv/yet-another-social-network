package com.getjavajob.training.web1801.orlovskiiv.validator;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.GroupDto;
import com.getjavajob.training.web1801.orlovskiiv.group.Group;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class GroupDtoValidator implements Validator {
    private static final String GROUP_NAME_FIELD_NAME = "name";
    private static final String GROUP_NAME_ERROR = "groupName.exist";
    private static final String GROUP_NAME_MESSAGE = "Группа с таким именем уже существует";

    private GroupService groupService;

    @Autowired
    public GroupDtoValidator(GroupService groupService) {
        this.groupService = groupService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return GroupDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        GroupDto dto = (GroupDto) target;
        if (dto.getId() == null) {
            checkingGroupNameExistence(dto, errors);
        } else {
            Group oldGroup = groupService.getGroupById(dto.getId());
            if (!oldGroup.getName().equals(dto.getName())) {
                checkingGroupNameExistence(dto, errors);
            }
        }
    }

    private void checkingGroupNameExistence(GroupDto dto, Errors errors) {
        boolean groupNameExist = groupService.groupNameExists(dto.getName());
        if (groupNameExist) {
            errors.rejectValue(GROUP_NAME_FIELD_NAME, GROUP_NAME_ERROR, GROUP_NAME_MESSAGE);
        }
    }
}
