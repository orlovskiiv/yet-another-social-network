package com.getjavajob.training.web1801.orlovskiiv.dto.form;

import com.getjavajob.training.web1801.orlovskiiv.account.phone.PhoneType;
import com.getjavajob.training.web1801.orlovskiiv.dto.transfer.Exist;
import com.getjavajob.training.web1801.orlovskiiv.dto.transfer.New;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@XStreamAlias("phone")
public class PhoneDto {
    private static final String NOT_BLANK_MESSAGE = "Поле не должно быть пустым";
    private static final String ONLY_DIGITS_MESSAGE = "Только цифры";
    private static final String NUMBER_LENGTH_MESSAGE = "Длина номера 11 цифр";

    @XStreamAlias("number")
    @NotBlank(message = NOT_BLANK_MESSAGE, groups = {New.class, Exist.class})
    @Pattern(regexp = "[0-9]+", message = ONLY_DIGITS_MESSAGE, groups = {New.class, Exist.class})
    @Size(min = 11, max = 11, message = NUMBER_LENGTH_MESSAGE, groups = {New.class, Exist.class})
    private String number;
    @XStreamAlias("type")
    private PhoneType type;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PhoneDto{" +
                "number='" + number + '\'' +
                ", type=" + type +
                '}';
    }
}
