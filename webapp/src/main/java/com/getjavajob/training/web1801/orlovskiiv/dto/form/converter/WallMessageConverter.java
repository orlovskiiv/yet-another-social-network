package com.getjavajob.training.web1801.orlovskiiv.dto.form.converter;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.WallMessageDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.support.SupportMessageConverter;
import com.getjavajob.training.web1801.orlovskiiv.message.WallMessage;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class WallMessageConverter implements Mapper<WallMessageDto, WallMessage> {
    private static final String ERROR_MESSAGE = "BeanUtils exception ";
    private static final Logger logger = LoggerFactory.getLogger(WallMessageConverter.class);

    private SupportMessageConverter supportMessageConverter;

    @Autowired
    public WallMessageConverter(SupportMessageConverter supportMessageConverter) {
        this.supportMessageConverter = supportMessageConverter;
    }

    @Override
    public WallMessage mapTargetToSource(WallMessageDto dto, WallMessage wallMessage) {
        return supportMessageConverter.convertDtoToMessage(dto, wallMessage);
    }

    @Override
    public WallMessageDto mapSourceToTarget(WallMessage wallMessage) {
        WallMessageDto dto = new WallMessageDto();
        try {
            BeanUtils.copyProperties(dto, wallMessage);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn(ERROR_MESSAGE, e);
        }
        return dto;
    }
}
