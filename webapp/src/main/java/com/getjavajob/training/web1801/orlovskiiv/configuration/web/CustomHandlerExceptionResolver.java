package com.getjavajob.training.web1801.orlovskiiv.configuration.web;

import com.getjavajob.training.web1801.orlovskiiv.support.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomHandlerExceptionResolver extends DefaultHandlerExceptionResolver {
    private static final String NOT_FOUND_VIEW = "error/error404";
    private static final String UNAUTHORIZED_VIEW = "error/error401";
    private static final String INTERNAL_SERVER_VIEW = "error/error500";
    private static final Logger customLogger = LoggerFactory.getLogger(CustomHandlerExceptionResolver.class);

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response,
                                              Object handler, Exception ex) {
        if (ex instanceof NoHandlerFoundException) {
            return new ModelAndView(NOT_FOUND_VIEW);
        } else if (ex instanceof UnauthorizedException || ex instanceof AccessDeniedException) {
            return new ModelAndView(UNAUTHORIZED_VIEW);
        } else {
            customLogger.error("NoHandlerFoundException", ex);
            return new ModelAndView(INTERNAL_SERVER_VIEW);
        }


    }
}
