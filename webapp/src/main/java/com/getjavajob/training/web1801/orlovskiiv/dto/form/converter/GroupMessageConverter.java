package com.getjavajob.training.web1801.orlovskiiv.dto.form.converter;

import com.getjavajob.training.web1801.orlovskiiv.dto.form.GroupMessageDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.support.SupportMessageConverter;
import com.getjavajob.training.web1801.orlovskiiv.message.GroupMessage;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

@Component
public class GroupMessageConverter implements Mapper<GroupMessageDto, GroupMessage> {
    private static final String ERROR_MESSAGE = "BeanUtils exception ";
    private static final Logger logger = LoggerFactory.getLogger(GroupMessageConverter.class);

    private SupportMessageConverter supportMessageConverter;

    @Autowired
    public GroupMessageConverter(SupportMessageConverter supportMessageConverter) {
        this.supportMessageConverter = supportMessageConverter;
    }

    @Override
    public GroupMessage mapTargetToSource(GroupMessageDto dto, GroupMessage groupMessage) {
        return supportMessageConverter.convertDtoToMessage(dto, groupMessage);
    }

    @Override
    public GroupMessageDto mapSourceToTarget(GroupMessage groupMessage) {
        GroupMessageDto dto = new GroupMessageDto();
        try {
            BeanUtils.copyProperties(dto, groupMessage);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.warn(ERROR_MESSAGE, e);
        }
        return dto;
    }
}
