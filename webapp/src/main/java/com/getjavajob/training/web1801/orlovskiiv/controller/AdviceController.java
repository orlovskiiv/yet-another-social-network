package com.getjavajob.training.web1801.orlovskiiv.controller;

import com.getjavajob.training.web1801.orlovskiiv.configuration.web.ByteArrayEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@ControllerAdvice
public class AdviceController {
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDate.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                if (!text.isEmpty()) {
                    LocalDate date = LocalDate.parse(text, DateTimeFormatter.ofPattern(DATE_FORMAT));
                    this.setValue(date);
                }
            }
        });
        binder.registerCustomEditor(byte[].class, new ByteArrayEditor());
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }
}
