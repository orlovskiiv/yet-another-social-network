package com.getjavajob.training.web1801.orlovskiiv.dto.form;

import com.getjavajob.training.web1801.orlovskiiv.account.address.AddressType;
import com.getjavajob.training.web1801.orlovskiiv.account.phone.PhoneType;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.constraints.DateFromToNow;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.constraints.FieldsMatch;
import com.getjavajob.training.web1801.orlovskiiv.dto.transfer.Exist;
import com.getjavajob.training.web1801.orlovskiiv.dto.transfer.New;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.getjavajob.training.web1801.orlovskiiv.account.address.AddressType.HOME;
import static com.getjavajob.training.web1801.orlovskiiv.account.phone.PhoneType.PERSONAL;

@XStreamAlias("account")
@FieldsMatch(first = "password", second = "passwordCopy", message = "Пароли не совпадают",
        groups = {New.class, Exist.class})
public class AccountDto {
    private static final String DATE_PATTERN = "уууу-MM-dd";
    private static final String MIN_BIRTH_DATE = "1900-01-01";
    private static final String NOT_BLANK_MESSAGE = "Поле не должно быть пустым";
    private static final String MAX_LENGTH_MESSAGE = "Не более 20 символов";
    private static final String INVALID_EMAIL_FORMAT = "Неверный формат эл. почты";
    private static final String SIZE_PSWD_MESSAGE = "От 6 до 20 символов";
    private static final String DATE_NOT_MATCH_MESSAGE = "От 1.01.1900 до сегодняшнего дня";
    private static final String PHOTO_SIZE_MESSAGE = "Максимальный размер 500кб";
    private static final String ICQ_LENGTH_MESSAGE = "От 5 до 9 цифр";
    private static final String ONLY_DIGITS_MESSAGE = "Только цифры";
    private static final String SKYPE_LENGTH_MESSAGE = "Не более 32 символов";
    private static final String ADDITIONAL_INFO_MESSAGE = "Не более 255 символов";

    @XStreamOmitField
    private Integer id;

    @XStreamAlias("firstName")
    @NotBlank(message = NOT_BLANK_MESSAGE, groups = {New.class, Exist.class})
    @Size(max = 20, message = MAX_LENGTH_MESSAGE, groups = {New.class, Exist.class})
    private String firstName;

    @XStreamAlias("lastName")
    @NotBlank(message = NOT_BLANK_MESSAGE, groups = {New.class, Exist.class})
    @Size(max = 20, message = MAX_LENGTH_MESSAGE, groups = {New.class, Exist.class})
    private String lastName;

    @XStreamAlias("middleName")
    @Size(max = 20, message = MAX_LENGTH_MESSAGE, groups = {New.class, Exist.class})
    private String middleName;

    @XStreamAlias("dateOfBirth")
    @DateTimeFormat(pattern = DATE_PATTERN)
    @DateFromToNow(from = MIN_BIRTH_DATE, message = DATE_NOT_MATCH_MESSAGE, groups = {New.class, Exist.class})
    private LocalDate dateOfBirth;

    @XStreamAlias("email")
    @NotBlank(message = NOT_BLANK_MESSAGE, groups = {New.class, Exist.class})
    @Email(message = INVALID_EMAIL_FORMAT, groups = {New.class, Exist.class})
    private String email;

    @XStreamOmitField
    @NotBlank(message = NOT_BLANK_MESSAGE, groups = New.class)
    @Size(min = 6, max = 20, message = SIZE_PSWD_MESSAGE, groups = {New.class, Exist.class})
    private String password;

    @XStreamOmitField
    @NotBlank(message = NOT_BLANK_MESSAGE, groups = New.class)
    @Size(min = 6, max = 20, message = SIZE_PSWD_MESSAGE, groups = {New.class, Exist.class})
    private String passwordCopy;

    @XStreamAlias("personalPhones")
    @Valid
    private List<PhoneDto> personalPhones = new ArrayList<PhoneDto>() {
        @Override
        public PhoneDto set(int index, PhoneDto element) {
            element.setType(PERSONAL);
            return super.set(index, element);
        }

        @Override
        public boolean add(PhoneDto phoneDto) {
            phoneDto.setType(PERSONAL);
            return super.add(phoneDto);
        }

        @Override
        public void add(int index, PhoneDto element) {
            element.setType(PERSONAL);
            super.add(index, element);
        }
    };

    @XStreamAlias("workingPhones")
    @Valid
    private List<PhoneDto> workingPhones = new ArrayList<PhoneDto>() {
        @Override
        public PhoneDto set(int index, PhoneDto element) {
            element.setType(PhoneType.WORKING);
            return super.set(index, element);
        }

        @Override
        public boolean add(PhoneDto phoneDto) {
            phoneDto.setType(PhoneType.WORKING);
            return super.add(phoneDto);
        }

        @Override
        public void add(int index, PhoneDto element) {
            element.setType(PhoneType.WORKING);
            super.add(index, element);
        }
    };

    @XStreamAlias("homeAddress")
    @Valid
    private AddressDto homeAddress = new AddressDto(HOME);

    @XStreamAlias("workingAddress")
    @Valid
    private AddressDto workingAddress = new AddressDto(AddressType.WORKING);

    @XStreamAlias("icq")
    @Size(min = 5, max = 9, message = ICQ_LENGTH_MESSAGE, groups = {New.class, Exist.class})
    @Pattern(regexp = "[0-9]+", message = ONLY_DIGITS_MESSAGE, groups = {New.class, Exist.class})
    private String icq;

    @XStreamAlias("skype")
    @Size(max = 32, message = SKYPE_LENGTH_MESSAGE, groups = {New.class, Exist.class})
    private String skype;

    @XStreamAlias("additionalInformation")
    @Size(max = 255, message = ADDITIONAL_INFO_MESSAGE, groups = {New.class, Exist.class})
    private String additionalInformation;

    @XStreamOmitField
    @Size(max = 512_000, message = PHOTO_SIZE_MESSAGE, groups = {New.class, Exist.class})
    private byte[] photo;

    @XStreamOmitField
    @Size(min = 6, max = 20, message = SIZE_PSWD_MESSAGE, groups = {New.class, Exist.class})
    private String oldPassword;

    @XStreamOmitField
    private String deletePhoto;

    @XStreamOmitField
    private String admin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordCopy() {
        return passwordCopy;
    }

    public void setPasswordCopy(String passwordCopy) {
        this.passwordCopy = passwordCopy;
    }

    public List<PhoneDto> getPersonalPhones() {
        return personalPhones;
    }

    public void setPersonalPhones(List<PhoneDto> personalPhones) {
        this.personalPhones = personalPhones;
    }

    public List<PhoneDto> getWorkingPhones() {
        return workingPhones;
    }

    public void setWorkingPhones(List<PhoneDto> workingPhones) {
        this.workingPhones = workingPhones;
    }

    public AddressDto getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(AddressDto homeAddress) {
        this.homeAddress = homeAddress;
    }

    public AddressDto getWorkingAddress() {
        return workingAddress;
    }

    public void setWorkingAddress(AddressDto workingAddress) {
        this.workingAddress = workingAddress;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getDeletePhoto() {
        return deletePhoto;
    }

    public void setDeletePhoto(String deletePhoto) {
        this.deletePhoto = deletePhoto;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    @Override
    public String toString() {
        return "AccountDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", passwordCopy='" + passwordCopy + '\'' +
                ", personalPhones=" + personalPhones +
                ", workingPhones=" + workingPhones +
                ", homeAddress=" + homeAddress +
                ", workingAddress=" + workingAddress +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", additionalInformation='" + additionalInformation + '\'' +
                ", photo=" + Arrays.toString(photo) +
                ", oldPassword='" + oldPassword + '\'' +
                ", deletePhoto='" + deletePhoto + '\'' +
                ", admin='" + admin + '\'' +
                '}';
    }
}
