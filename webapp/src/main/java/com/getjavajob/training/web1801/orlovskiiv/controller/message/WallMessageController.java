package com.getjavajob.training.web1801.orlovskiiv.controller.message;

import com.getjavajob.training.web1801.orlovskiiv.controller.support.AccountSupport;
import com.getjavajob.training.web1801.orlovskiiv.controller.support.MessageSupport;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.WallMessageDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.WallMessageConverter;
import com.getjavajob.training.web1801.orlovskiiv.message.WallMessage;
import com.getjavajob.training.web1801.orlovskiiv.message.WallMessageService;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import com.getjavajob.training.web1801.orlovskiiv.support.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class WallMessageController {
    private static final String WALL_MESSAGE_MODEL = "wallMessage";
    private static final String WALL_TARGET = "updateWallMessage";
    private static final String MESSAGE_MODEL = "message";
    private static final String REDIRECT_ACCOUNT = "redirect:/account/";

    private SessionAccount sessionAccount;
    private WallMessageService wallMessageService;
    private WallMessageConverter wallMessageConverter;
    private AccountSupport accountSupport;
    private MessageSupport messageSupport;

    @Autowired
    public WallMessageController(SessionAccount sessionAccount,
                                 WallMessageService wallMessageService,
                                 WallMessageConverter wallMessageConverter,
                                 AccountSupport accountSupport,
                                 MessageSupport messageSupport) {
        this.sessionAccount = sessionAccount;
        this.wallMessageService = wallMessageService;
        this.wallMessageConverter = wallMessageConverter;
        this.accountSupport = accountSupport;
        this.messageSupport = messageSupport;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/account/{accountId}")
    public ModelAndView postAccountWallMessage(@PathVariable int accountId,
                                               @Valid @ModelAttribute(WALL_MESSAGE_MODEL) WallMessageDto wallMessage,
                                               BindingResult bindingResult,
                                               ModelAndView modelAndView) {
        if (bindingResult.getAllErrors().isEmpty()) {
            WallMessage newMessage = wallMessageConverter.mapTargetToSource(wallMessage, new WallMessage());
            newMessage.setIdAuthor(sessionAccount.getId());
            newMessage.setIdTarget(accountId);
            wallMessageService.createUpdateMessage(newMessage);
            modelAndView.setViewName(REDIRECT_ACCOUNT + accountId);
        } else {
            modelAndView.addObject(WALL_MESSAGE_MODEL, wallMessage);
            accountSupport.displayingAccountContent(accountId, modelAndView);
        }
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/deleteWallMessage/{messageId}")
    public String deleteWallMessage(@PathVariable int messageId) throws UnauthorizedException {
        WallMessage message = wallMessageService.getMessageById(messageId);
        boolean isAdmin = sessionAccount.isAdmin();
        boolean isAuthor = message.getIdAuthor().equals(sessionAccount.getId());
        boolean isTarget = message.getIdTarget().equals(sessionAccount.getId());

        if (isAuthor || isTarget || isAdmin) {
            wallMessageService.deleteMessageById(messageId);
            return REDIRECT_ACCOUNT + message.getIdTarget();
        } else {
            throw new UnauthorizedException();
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/updateWallMessage/{messageId}")
    public ModelAndView getUpdateWallMessage(@PathVariable int messageId, ModelAndView modelAndView)
            throws UnauthorizedException {
        WallMessage oldMessage = wallMessageService.getMessageById(messageId);
        boolean isAdmin = sessionAccount.isAdmin();
        boolean isAuthor = oldMessage.getIdAuthor().equals(sessionAccount.getId());

        if (isAuthor || isAdmin) {
            WallMessageDto wallMessageDto = wallMessageConverter.mapSourceToTarget(oldMessage);
            messageSupport.setUpdateMessageAttributes(wallMessageDto, modelAndView, oldMessage, WALL_TARGET);
            return modelAndView;
        } else {
            throw new UnauthorizedException();
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/updateWallMessage/{messageId}")
    public ModelAndView postUpdateWallMessage(@PathVariable int messageId,
                                              @Valid @ModelAttribute(MESSAGE_MODEL) WallMessageDto messageDto,
                                              BindingResult bindingResult, ModelAndView modelAndView)
            throws UnauthorizedException {
        WallMessage originalMessage = wallMessageService.getMessageById(messageId);
        boolean isAdmin = sessionAccount.isAdmin();
        boolean isAuthor = originalMessage.getIdAuthor().equals(sessionAccount.getId());
        if (isAuthor || isAdmin) {
            if (bindingResult.getAllErrors().isEmpty()) {
                WallMessage updatedMessage = wallMessageConverter.mapTargetToSource(messageDto, originalMessage);
                wallMessageService.createUpdateMessage(updatedMessage);
                modelAndView.setViewName(REDIRECT_ACCOUNT + originalMessage.getIdTarget());
            } else {
                messageSupport.setUpdateMessageAttributes(messageDto, modelAndView, originalMessage, WALL_TARGET);
            }
            return modelAndView;
        } else {
            throw new UnauthorizedException();
        }
    }
}
