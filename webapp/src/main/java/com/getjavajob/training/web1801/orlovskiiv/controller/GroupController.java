package com.getjavajob.training.web1801.orlovskiiv.controller;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.controller.support.GroupSupport;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.GroupDto;
import com.getjavajob.training.web1801.orlovskiiv.dto.form.converter.GroupConverter;
import com.getjavajob.training.web1801.orlovskiiv.group.Group;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupService;
import com.getjavajob.training.web1801.orlovskiiv.support.SessionAccount;
import com.getjavajob.training.web1801.orlovskiiv.support.UnauthorizedException;
import com.getjavajob.training.web1801.orlovskiiv.validator.GroupDtoValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Set;

import static com.getjavajob.training.web1801.orlovskiiv.group.GroupMemberStatus.*;

@Controller
public class GroupController {
    private static final String MEMBER_GROUPS_ATTR = "memberGroups";
    private static final String PENDING_GROUPS_ATTR = "pendingGroups";
    private static final String STATUS_ATTR = "status";
    private static final String GROUP_ATTR = "group";
    private static final String GROUP_MODEL = "group";
    private static final String ID_ATTR = "id";
    private static final String PENDING_USERS_ATTR = "pendingUsers";
    private static final String MODERATORS_ATTR = "moderators";
    private static final String USERS_ATTR = "users";
    private static final String GROUP_LIST_VIEW_NAME = "group/groupList";
    private static final String GROUP_UPDATE_VIEW_NAME = "group/groupUpdate";
    private static final String GROUP_CREATE_VIEW_NAME = "group/groupCreate";
    private static final String GROUP_DELETE_VIEW_NAME = "group/groupDeleteStatus";
    private static final String GROUP_MEMBERS_USER_VIEW = "group/groupMembersUser";
    private static final String GROUP_MEMBERS_MODER_VIEW = "group/groupMembersModer";
    private static final String REDIRECT_GROUP = "redirect:/group/";
    private static final String REDIRECT_MEMBERS = "/members";
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    private AccountService accountService;
    private GroupService groupService;
    private SessionAccount sessionAccount;
    private GroupConverter groupConverter;
    private GroupDtoValidator groupDtoValidator;
    private GroupSupport groupSupport;

    public GroupController(AccountService accountService,
                           GroupService groupService,
                           SessionAccount sessionAccount,
                           GroupConverter groupConverter,
                           GroupDtoValidator groupDtoValidator,
                           GroupSupport groupSupport) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.sessionAccount = sessionAccount;
        this.groupConverter = groupConverter;
        this.groupDtoValidator = groupDtoValidator;
        this.groupSupport = groupSupport;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/account/{accountId}/groups")
    public ModelAndView getGroups(@PathVariable int accountId, ModelAndView modelAndView) {
        Set<Group> memberGroups = groupService.getGroupsByAccountIdAndStatus(accountId, MODER);
        memberGroups.addAll(groupService.getGroupsByAccountIdAndStatus(accountId, USER));
        Set<Group> pendingGroups = groupService.getGroupsByAccountIdAndStatus(accountId, PENDING);
        modelAndView.addObject(MEMBER_GROUPS_ATTR, memberGroups);
        modelAndView.addObject(PENDING_GROUPS_ATTR, pendingGroups);
        modelAndView.setViewName(GROUP_LIST_VIEW_NAME);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/group/{groupId}")
    public ModelAndView getGroup(@PathVariable int groupId, ModelAndView modelAndView) {
        groupSupport.displayingGroupContent(groupId, modelAndView);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/group/{groupId}/members")
    public ModelAndView getGroupMembers(@PathVariable int groupId, ModelAndView modelAndView)
            throws UnauthorizedException {
        int sessionId = sessionAccount.getId();
        GroupMemberStatus sessionAccountStatus = groupService.getMemberStatus(sessionId, groupId);
        setGroupMembersAttributes(groupId, modelAndView);
        switch (sessionAccountStatus) {
            case USER:
                modelAndView.setViewName(GROUP_MEMBERS_USER_VIEW);
                break;
            case MODER:
                Set<Account> pendingUsers = accountService.getAccountsInGroup(groupId, PENDING);
                modelAndView.addObject(PENDING_USERS_ATTR, pendingUsers);
                modelAndView.setViewName(GROUP_MEMBERS_MODER_VIEW);
                break;
            default:
                throw new UnauthorizedException();
        }
        return modelAndView;
    }

    private void setGroupMembersAttributes(int groupId, ModelAndView modelAndView) {
        Group group = groupService.getGroupById(groupId);
        modelAndView.addObject(GROUP_ATTR, group);
        Set<Account> moderators = accountService.getAccountsInGroup(groupId, MODER);
        Set<Account> users = accountService.getAccountsInGroup(groupId, USER);
        modelAndView.addObject(MODERATORS_ATTR, moderators);
        modelAndView.addObject(USERS_ATTR, users);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/createGroup")
    public ModelAndView getCreateGroup() {
        return new ModelAndView(GROUP_CREATE_VIEW_NAME, GROUP_MODEL, new Group());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/createGroup")
    public ModelAndView postCreateGroup(@Valid @ModelAttribute(GROUP_MODEL) GroupDto group,
                                        BindingResult bindingResult, ModelAndView modelAndView) {
        groupDtoValidator.validate(group, bindingResult);
        if (bindingResult.getAllErrors().isEmpty()) {
            Group newGroup = groupConverter.mapTargetToSource(group, new Group());
            newGroup.setCreatorId(sessionAccount.getId());
            newGroup = groupService.createGroup(newGroup);
            logger.info("Group created: {}", newGroup);
            modelAndView.setViewName(REDIRECT_GROUP + newGroup.getId());
        } else {
            modelAndView.addObject(GROUP_MODEL, group);
            modelAndView.setViewName(GROUP_CREATE_VIEW_NAME);
        }
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/group/{groupId}/update")
    public ModelAndView getUpdateGroup(@PathVariable int groupId, ModelAndView modelAndView) {
        Group groupForUpdate = groupService.getGroupById(groupId);
        GroupDto dto = groupConverter.mapSourceToTarget(groupForUpdate);
        modelAndView.addObject(GROUP_MODEL, dto);
        modelAndView.addObject(ID_ATTR, groupForUpdate.getId());
        modelAndView.setViewName(GROUP_UPDATE_VIEW_NAME);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/group/{groupId}/update")
    public ModelAndView postUpdateGroup(@PathVariable int groupId,
                                        @Valid @ModelAttribute(GROUP_MODEL) GroupDto group,
                                        BindingResult bindingResult, ModelAndView modelAndView) {
        group.setId(groupId);
        groupDtoValidator.validate(group, bindingResult);
        if (bindingResult.getAllErrors().isEmpty()) {
            Group oldGroup = groupService.getGroupById(groupId);
            Group updatedGroup = groupConverter.mapTargetToSource(group, oldGroup);
            groupService.updateGroup(updatedGroup);
            logger.info("Group updated: {}", updatedGroup);
            modelAndView.setViewName(REDIRECT_GROUP + groupId);
        } else {
            modelAndView.addObject(GROUP_MODEL, group);
            modelAndView.setViewName(GROUP_UPDATE_VIEW_NAME);
        }
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/group/{deleteGroupId}/update/deleteGroup")
    public ModelAndView deleteGroup(@PathVariable int deleteGroupId) {
        boolean deleted = groupService.deleteGroupById(deleteGroupId);
        ModelAndView modelAndView = new ModelAndView(GROUP_DELETE_VIEW_NAME, STATUS_ATTR, deleted);
        if (!deleted) {
            modelAndView.addObject(ID_ATTR, deleteGroupId);
        }
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/group/{recoverGroupId}/update/recoverGroup")
    public String recoverGroup(@PathVariable int recoverGroupId) {
        groupService.recoverGroupById(recoverGroupId);
        return REDIRECT_GROUP + recoverGroupId;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/group/{groupId}/deleteGroupMember/{accountId}")
    public String deleteGroupMember(@PathVariable int groupId,
                                    @PathVariable int accountId) {
        groupService.deleteGroupMemberById(accountId, groupId);
        return REDIRECT_GROUP + groupId + REDIRECT_MEMBERS;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/group/{groupId}/changeMemberStatus/{accountId}/{status}")
    public String changeMemberStatusToUser(@PathVariable int groupId,
                                           @PathVariable int accountId,
                                           @PathVariable String status) {
        GroupMemberStatus newStatus = GroupMemberStatus.valueOf(status.toUpperCase());
        groupService.createUpdateGroupMember(accountId, groupId, newStatus);
        return REDIRECT_GROUP + groupId + REDIRECT_MEMBERS;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/subscribe/group/{groupId}")
    public String subscribe(@PathVariable int groupId) {
        int accountId = sessionAccount.getId();
        GroupMemberStatus status = groupService.getMemberStatus(accountId, groupId);
        if (status == NONE) {
            groupService.createUpdateGroupMember(accountId, groupId, PENDING);
        }
        return REDIRECT_GROUP + groupId;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/unsubscribe/group/{groupId}")
    public String unsubscribe(@PathVariable int groupId) {
        int accountId = sessionAccount.getId();
        GroupMemberStatus status = groupService.getMemberStatus(accountId, groupId);
        if (status != NONE) {
            groupService.deleteGroupMemberById(accountId, groupId);
        }
        return REDIRECT_GROUP + groupId;
    }
}
