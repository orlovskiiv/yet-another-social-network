package com.getjavajob.training.web1801.orlovskiiv.dto.websocket;

public class PrivateMessageJsonDto {
    private String id;
    private String idAuthor;
    private String idTarget;
    private String text;
    private String dateOfCreation;

    public PrivateMessageJsonDto() {
    }

    public PrivateMessageJsonDto(String id, String idAuthor, String idTarget, String text, String dateOfCreation) {
        this.id = id;
        this.idAuthor = idAuthor;
        this.idTarget = idTarget;
        this.text = text;
        this.dateOfCreation = dateOfCreation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(String idAuthor) {
        this.idAuthor = idAuthor;
    }

    public String getIdTarget() {
        return idTarget;
    }

    public void setIdTarget(String idTarget) {
        this.idTarget = idTarget;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }
}
