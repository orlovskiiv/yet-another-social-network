package com.getjavajob.training.web1801.orlovskiiv.controller;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;
import com.getjavajob.training.web1801.orlovskiiv.account.AccountService;
import com.getjavajob.training.web1801.orlovskiiv.group.Group;
import com.getjavajob.training.web1801.orlovskiiv.group.GroupService;
import com.getjavajob.training.web1801.orlovskiiv.message.GroupMessageService;
import com.getjavajob.training.web1801.orlovskiiv.message.PrivateMessageService;
import com.getjavajob.training.web1801.orlovskiiv.message.WallMessageService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static java.lang.Thread.currentThread;

@Controller
@RequestMapping("/image")
public class ImageController {
    private static final String CONTENT_TYPE = "image/jpeg";
    private static final String DEFAULT_ACCOUNT_PHOTO = "defaultavatar/default-avatar.jpg";
    private static final String DELETED_ACCOUNT_PHOTO = "defaultavatar/profile-deleted.jpg";
    private static final String DEFAULT_GROUP_PHOTO = "defaultavatar/default-group-avatar.png";
    private static final String DELETED_GROUP_PHOTO = "defaultavatar/group-deleted.jpg";
    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

    private AccountService accountService;
    private GroupService groupService;
    private WallMessageService wallMessageService;
    private GroupMessageService groupMessageService;
    private PrivateMessageService privateMessageService;

    @Autowired
    public ImageController(AccountService accountService,
                           GroupService groupService,
                           WallMessageService wallMessageService,
                           GroupMessageService groupMessageService,
                           PrivateMessageService privateMessageService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.wallMessageService = wallMessageService;
        this.groupMessageService = groupMessageService;
        this.privateMessageService = privateMessageService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/account/{accountId}")
    public void getAccountPhoto(@PathVariable int accountId, HttpServletResponse response) {
        Account account = accountService.getAccountById(accountId);
        byte[] photoBytes = account.getPhoto();
        boolean deleted = account.isDeleted();
        if (deleted || photoBytes == null) {
            photoBytes = getDefaultAvatar(getPathToAccountAvatar(deleted));
        }
        writeOutputStream(response, photoBytes);
    }

    private String getPathToAccountAvatar(boolean deleted) {
        return deleted ? DELETED_ACCOUNT_PHOTO : DEFAULT_ACCOUNT_PHOTO;
    }

    private byte[] getDefaultAvatar(String defaultAvatar) {
        byte[] photoBytes = null;
        try (InputStream inputStream = currentThread().getContextClassLoader().getResourceAsStream(defaultAvatar)) {
            photoBytes = IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            logger.warn("IOException: ", e);
        }
        return photoBytes;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/group/{groupId}")
    public void getGroupPhoto(@PathVariable int groupId, HttpServletResponse response) {
        Group group = groupService.getGroupById(groupId);
        byte[] photoBytes = group.getPhoto();
        boolean deleted = group.isDeleted();
        if (deleted || photoBytes == null) {
            photoBytes = getDefaultAvatar(getPathToGroupAvatar(deleted));
        }
        writeOutputStream(response, photoBytes);
    }

    private String getPathToGroupAvatar(boolean deleted) {
        return deleted ? DELETED_GROUP_PHOTO : DEFAULT_GROUP_PHOTO;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/wallMessage/{wallMessageId}")
    public void getWallMessagePhoto(@PathVariable int wallMessageId, HttpServletResponse response) {
        byte[] photoBytes = wallMessageService.getMessageById(wallMessageId).getPhoto();
        writeOutputStream(response, photoBytes);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/groupMessage/{groupMessageId}")
    public void getGroupMessagePhoto(@PathVariable int groupMessageId, HttpServletResponse response) {
        byte[] photoBytes = groupMessageService.getMessageById(groupMessageId).getPhoto();
        writeOutputStream(response, photoBytes);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/privateMessage/{privateMessageId}")
    public void getPrivateMessagePhoto(@PathVariable int privateMessageId, HttpServletResponse response) {
        byte[] photoBytes = privateMessageService.getMessageById(privateMessageId).getPhoto();
        writeOutputStream(response, photoBytes);
    }

    private void writeOutputStream(HttpServletResponse response, byte[] photoBytes) {
        if (photoBytes != null) {
            response.setContentType(CONTENT_TYPE);
            try (OutputStream outputStream = response.getOutputStream()) {
                outputStream.write(photoBytes);
            } catch (IOException e) {
                logger.warn("IOException: ", e);
            }
        }
    }
}
