# Yet Another Social Network  

** Functionality: **  

+ registration  
+ authentication  
+ ajax search with pagination  
+ display profile  
+ edit profile  
+ upload and download avatar  
+ users export and import xml  
+ creating/updating groups  
+ wall/group messages  
+ private messages using websocket  
+ update/delete messages  
+ friendship functionality  
+ admin and user roles  

** Tools: **  
JDK 8, Spring 5, JPA 2 / Hibernate 4, XStream, jQuery 3, Twitter Bootstrap 4, JUnit 4, Mockito, Maven 4, Git / Bitbucket, Tomcat 8, MySQL, IntelliJIDEA 18.  

** Notes: **  
SQL ddl is located in the `database/mysql.sql`  

** Screenshots **  

![account](https://bitbucket.org/orlovskiiv/yet-another-social-network/raw/master/screenshots/account.png)  
![update](https://bitbucket.org/orlovskiiv/yet-another-social-network/raw/master/screenshots/update.png)  
![search](https://bitbucket.org/orlovskiiv/yet-another-social-network/raw/master/screenshots/search.png)  
![group](https://bitbucket.org/orlovskiiv/yet-another-social-network/raw/master/screenshots/group.png)  
![messaging](https://bitbucket.org/orlovskiiv/yet-another-social-network/raw/master/screenshots/messaging.png)  

http://new-soc-net.herokuapp.com  

Test user:  
login - test@test  
password - 123123  
_  
**Orlovskii Vladimir**  
Training getJavaJob  
http://www.getjavajob.com  

