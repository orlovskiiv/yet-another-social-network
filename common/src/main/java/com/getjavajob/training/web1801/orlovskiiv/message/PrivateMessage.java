package com.getjavajob.training.web1801.orlovskiiv.message;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "Private_Messages")
public class PrivateMessage extends Message {
    @Column(name = "Viewed")
    private boolean viewed;

    @Column(name = "Removed_by_Author")
    private boolean removedByAuthor;

    @Column(name = "Removed_by_Target")
    private boolean removedByTarget;

    public boolean isViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }

    public boolean isRemovedByAuthor() {
        return removedByAuthor;
    }

    public void setRemovedByAuthor(boolean removedByAuthor) {
        this.removedByAuthor = removedByAuthor;
    }

    public boolean isRemovedByTarget() {
        return removedByTarget;
    }

    public void setRemovedByTarget(boolean removedByTarget) {
        this.removedByTarget = removedByTarget;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PrivateMessage that = (PrivateMessage) o;
        return viewed == that.viewed &&
                removedByAuthor == that.removedByAuthor &&
                removedByTarget == that.removedByTarget;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), viewed, removedByAuthor, removedByTarget);
    }
}
