package com.getjavajob.training.web1801.orlovskiiv.message;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

@MappedSuperclass
public abstract class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false)
    private Integer id;

    @Column(name = "ID_Author")
    private Integer idAuthor;

    @Column(name = "ID_Target")
    private Integer idTarget;

    @Column(name = "Text")
    private String text;

    @Column(name = "Photo")
    private byte[] photo;

    @Column(name = "Date_of_Creation", insertable = false, updatable = false)
    private LocalDateTime dateOfCreation;

    @Column(name = "Edited")
    private boolean edited;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(Integer idAuthor) {
        this.idAuthor = idAuthor;
    }

    public Integer getIdTarget() {
        return idTarget;
    }

    public void setIdTarget(Integer idTarget) {
        this.idTarget = idTarget;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public LocalDateTime getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDateTime dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    @PreUpdate
    public void preUpdate() {
        edited = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return edited == message.edited &&
                Objects.equals(id, message.id) &&
                Objects.equals(idAuthor, message.idAuthor) &&
                Objects.equals(idTarget, message.idTarget) &&
                Objects.equals(text, message.text) &&
                Arrays.equals(photo, message.photo);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, idAuthor, idTarget, text, edited);
        result = 31 * result + Arrays.hashCode(photo);
        return result;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", idAuthor=" + idAuthor +
                ", idTarget=" + idTarget +
                ", text='" + text + '\'' +
                ", dateOfCreation=" + dateOfCreation +
                ", edited=" + edited +
                ", photo=" + (photo != null) +
                '}';
    }
}
