package com.getjavajob.training.web1801.orlovskiiv.account;

public enum AccountRelationshipStatus {
    FRIEND_REQUEST,
    SENT_REQUEST,
    SUBSCRIBER,
    FRIEND,
    INVALID_STATUS,
    NONE
}
