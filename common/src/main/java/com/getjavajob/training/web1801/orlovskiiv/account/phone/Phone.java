package com.getjavajob.training.web1801.orlovskiiv.account.phone;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Phones")
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Account_ID")
    private Account account;

    @Enumerated(EnumType.STRING)
    @Column(name = "Type")
    private PhoneType type;

    @Column(name = "Number")
    private String number;

    public Phone() {
    }

    public Phone(Account account, PhoneType type, String number) {
        this.account = account;
        this.type = type;
        this.number = number;
    }

    public Phone(Integer id, Account account, PhoneType type, String number) {
        this.id = id;
        this.account = account;
        this.type = type;
        this.number = number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return Objects.equals(id, phone.id) &&
                type == phone.type &&
                Objects.equals(number, phone.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, number);
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + id +
                ", type=" + type +
                ", number='" + number + '\'' +
                '}';
    }
}
