package com.getjavajob.training.web1801.orlovskiiv.group;

public enum GroupMemberStatus {
    PENDING,
    USER,
    MODER,
    BLOCKED,
    NONE
}
