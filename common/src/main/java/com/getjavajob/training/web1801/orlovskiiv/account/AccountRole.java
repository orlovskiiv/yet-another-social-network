package com.getjavajob.training.web1801.orlovskiiv.account;

public enum AccountRole {
    USER,
    ADMIN
}
