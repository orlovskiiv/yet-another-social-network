package com.getjavajob.training.web1801.orlovskiiv.account.address;

import com.getjavajob.training.web1801.orlovskiiv.account.Account;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Addresses")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Account_ID")
    private Account account;

    @Enumerated(EnumType.STRING)
    @Column(name = "Type")
    private AddressType type;

    @Column(name = "Country")
    private String country;

    @Column(name = "Region")
    private String region;

    @Column(name = "City")
    private String city;

    @Column(name = "Street")
    private String street;

    @Column(name = "House")
    private String house;

    public Address() {
    }

    public Address(Account account, AddressType type, String country, String region, String city, String street,
                   String house) {
        this.account = account;
        this.type = type;
        this.country = country;
        this.region = region;
        this.city = city;
        this.street = street;
        this.house = house;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public AddressType getType() {
        return type;
    }

    public void setType(AddressType type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(id, address.id) &&
                type == address.type &&
                Objects.equals(country, address.country) &&
                Objects.equals(region, address.region) &&
                Objects.equals(city, address.city) &&
                Objects.equals(street, address.street) &&
                Objects.equals(house, address.house);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, country, region, city, street, house);
    }

    @Override
    public String toString() {
        StringBuilder sbAddress = new StringBuilder();
        if (country != null) {
            sbAddress.append(country).append(" ");
        }
        if (region != null) {
            sbAddress.append(region).append(" ");
        }
        if (city != null) {
            sbAddress.append(city).append(" ");
        }
        if (street != null) {
            sbAddress.append(street).append(" ");
        }
        if (house != null) {
            sbAddress.append(house);
        }
        return sbAddress.toString();
    }
}
