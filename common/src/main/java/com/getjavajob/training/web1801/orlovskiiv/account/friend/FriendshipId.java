package com.getjavajob.training.web1801.orlovskiiv.account.friend;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class FriendshipId implements Serializable {
    @Column(name = "ID_min")
    private Integer minAccountId;

    @Column(name = "ID_max")
    private Integer maxAccountId;

    public FriendshipId() {
    }

    public FriendshipId(Integer minAccountId, Integer maxAccountId) {
        this.minAccountId = minAccountId;
        this.maxAccountId = maxAccountId;
    }

    public Integer getMinAccountId() {
        return minAccountId;
    }

    public void setMinAccountId(Integer minAccountId) {
        this.minAccountId = minAccountId;
    }

    public Integer getMaxAccountId() {
        return maxAccountId;
    }

    public void setMaxAccountId(Integer maxAccountId) {
        this.maxAccountId = maxAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendshipId that = (FriendshipId) o;
        return Objects.equals(minAccountId, that.minAccountId) &&
                Objects.equals(maxAccountId, that.maxAccountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(minAccountId, maxAccountId);
    }
}
