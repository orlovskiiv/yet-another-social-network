package com.getjavajob.training.web1801.orlovskiiv.message;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "Group_Messages")
public class GroupMessage extends Message {
    @Column(name = "Removed")
    private boolean removed;

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GroupMessage that = (GroupMessage) o;
        return removed == that.removed;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), removed);
    }
}
