package com.getjavajob.training.web1801.orlovskiiv.group;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Group_Members")
public class GroupMember {
    @EmbeddedId
    private GroupMemberId id;

    @Enumerated(EnumType.STRING)
    @Column(name = "Status")
    private GroupMemberStatus status;

    public GroupMember() {
    }

    public GroupMember(int accountId, int groupId, GroupMemberStatus status) {
        this.id = new GroupMemberId(accountId, groupId);
        this.status = status;
    }

    public GroupMemberId getId() {
        return id;
    }

    public void setId(GroupMemberId id) {
        this.id = id;
    }

    public GroupMemberStatus getStatus() {
        return status;
    }

    public void setStatus(GroupMemberStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupMember that = (GroupMember) o;
        return Objects.equals(id, that.id) &&
                status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status);
    }
}
