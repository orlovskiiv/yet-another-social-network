package com.getjavajob.training.web1801.orlovskiiv.account.phone;

public enum PhoneType {
    PERSONAL,
    WORKING
}
