package com.getjavajob.training.web1801.orlovskiiv.group;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class GroupMemberId implements Serializable {
    @Column(name = "ID_User")
    private Integer accountId;

    @Column(name = "ID_Group")
    private Integer groupId;

    public GroupMemberId() {
    }

    public GroupMemberId(Integer accountId, Integer groupId) {
        this.accountId = accountId;
        this.groupId = groupId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupMemberId that = (GroupMemberId) o;
        return Objects.equals(accountId, that.accountId) &&
                Objects.equals(groupId, that.groupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, groupId);
    }
}
