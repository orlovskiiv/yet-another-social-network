package com.getjavajob.training.web1801.orlovskiiv.account.friend;

public enum FriendStatus {
    PENDING,
    ACCEPTED,
    DECLINED,
    BLOCKED
}
