package com.getjavajob.training.web1801.orlovskiiv.account.friend;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Friends")
public class Friendship {
    @EmbeddedId
    private FriendshipId id;

    @Enumerated(EnumType.STRING)
    @Column(name = "Status")
    private FriendStatus status;

    @Column(name = "Action_User_ID")
    private Integer fromId;

    public Friendship() {
    }

    public Friendship(Integer minAccountId, Integer maxAccountId, FriendStatus status, Integer fromId) {
        this.id = new FriendshipId(minAccountId, maxAccountId);
        this.status = status;
        this.fromId = fromId;
    }

    public FriendshipId getId() {
        return id;
    }

    public void setId(FriendshipId id) {
        this.id = id;
    }

    public FriendStatus getStatus() {
        return status;
    }

    public void setStatus(FriendStatus status) {
        this.status = status;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friendship that = (Friendship) o;
        return Objects.equals(id, that.id) &&
                status == that.status &&
                Objects.equals(fromId, that.fromId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, status, fromId);
    }
}
