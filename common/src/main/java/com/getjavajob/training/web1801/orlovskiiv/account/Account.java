package com.getjavajob.training.web1801.orlovskiiv.account;

import com.getjavajob.training.web1801.orlovskiiv.account.address.Address;
import com.getjavajob.training.web1801.orlovskiiv.account.phone.Phone;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.getjavajob.training.web1801.orlovskiiv.account.AccountRole.USER;

@Entity
@Table(name = "Accounts")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false)
    private Integer id;

    @Column(name = "First_Name")
    private String firstName;

    @Column(name = "Last_Name")
    private String lastName;

    @Column(name = "Middle_Name")
    private String middleName;

    @Column(name = "Date_of_Birth")
    private LocalDate dateOfBirth;

    @Column(name = "Email")
    private String email;

    @Column(name = "Password")
    private String password;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "Type = \'PERSONAL\'")
    private Set<Phone> personalPhones = new HashSet<>();

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "Type = \'WORKING\'")
    private Set<Phone> workingPhones = new HashSet<>();

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "Type = \'HOME\'")
    private Set<Address> homeAddress = new HashSet<>();

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "Type = \'WORKING\'")
    private Set<Address> workingAddress = new HashSet<>();

    @Column(name = "ICQ")
    private String icq;

    @Column(name = "Skype")
    private String skype;

    @Column(name = "Additional_Information")
    private String additionalInformation;

    @Enumerated(EnumType.STRING)
    @Column(name = "Role")
    private AccountRole role = USER;

    @Column(name = "Photo")
    private byte[] photo;

    @Column(name = "Registration", insertable = false, updatable = false)
    private LocalDate registration;

    @Column(name = "Deleted")
    private boolean deleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Phone> getPersonalPhones() {
        return personalPhones;
    }

    public void setPersonalPhones(Set<Phone> personalPhones) {
        this.personalPhones.retainAll(personalPhones);
        this.personalPhones.addAll(personalPhones);
    }

    public Set<Phone> getWorkingPhones() {
        return workingPhones;
    }

    public void setWorkingPhones(Set<Phone> workingPhones) {
        this.workingPhones.retainAll(workingPhones);
        this.workingPhones.addAll(workingPhones);
    }

    public Set<Address> getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Set<Address> homeAddress) {
        this.homeAddress.retainAll(homeAddress);
        this.homeAddress.addAll(homeAddress);
    }

    public Set<Address> getWorkingAddress() {
        return workingAddress;
    }

    public void setWorkingAddress(Set<Address> workingAddress) {
        this.workingAddress.retainAll(workingAddress);
        this.workingAddress.addAll(workingAddress);
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public AccountRole getRole() {
        return role;
    }

    public void setRole(AccountRole role) {
        this.role = role;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public LocalDate getRegistration() {
        return registration;
    }

    public void setRegistration(LocalDate registration) {
        this.registration = registration;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return deleted == account.deleted &&
                Objects.equals(id, account.id) &&
                Objects.equals(firstName, account.firstName) &&
                Objects.equals(lastName, account.lastName) &&
                Objects.equals(middleName, account.middleName) &&
                Objects.equals(dateOfBirth, account.dateOfBirth) &&
                Objects.equals(email, account.email) &&
                Objects.equals(password, account.password) &&
                Objects.equals(personalPhones, account.personalPhones) &&
                Objects.equals(workingPhones, account.workingPhones) &&
                Objects.equals(homeAddress, account.homeAddress) &&
                Objects.equals(workingAddress, account.workingAddress) &&
                Objects.equals(icq, account.icq) &&
                Objects.equals(skype, account.skype) &&
                Objects.equals(additionalInformation, account.additionalInformation) &&
                role == account.role &&
                Arrays.equals(photo, account.photo) &&
                Objects.equals(registration, account.registration);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, firstName, lastName, middleName, dateOfBirth, email, password, personalPhones,
                workingPhones, homeAddress, workingAddress, icq, skype, additionalInformation, role, registration,
                deleted);
        result = 31 * result + Arrays.hashCode(photo);
        return result;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", personalPhones=" + personalPhones +
                ", workingPhones=" + workingPhones +
                ", homeAddress=" + homeAddress +
                ", workingAddress=" + workingAddress +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", additionalInformation='" + additionalInformation + '\'' +
                ", role=" + role +
                ", photo=" + (photo != null) +
                ", registration=" + registration +
                ", deleted=" + deleted +
                '}';
    }
}
